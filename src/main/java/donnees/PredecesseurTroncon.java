package donnees;

public class PredecesseurTroncon {
	private Station predecesseur;
	private TronconLigne tronconLigne;

	public PredecesseurTroncon(Station n, TronconLigne t) {
		this.predecesseur = n;
		this.tronconLigne = t;
	}

	public Station getPredecesseur() {
		return predecesseur;
	}

	public TronconLigne getTronconLigne() {
		return tronconLigne;
	}
}
