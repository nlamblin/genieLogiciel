package donnees;



public class Direction {

	private String nomStation;
	private String nomLigne;

	public Direction(String nomStation, String nomLigne) {
		super();
		this.nomStation = nomStation;
		this.nomLigne = nomLigne;
	}

	public String getNomStation() {
		return nomStation;
	}

	public String getNomLigne() {
		return nomLigne;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nomLigne == null) ? 0 : nomLigne.hashCode());
		result = prime * result + ((nomStation == null) ? 0 : nomStation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Direction other = (Direction) obj;
		if (nomLigne == null) {
			if (other.nomLigne != null)
				return false;
		} else if (!nomLigne.equals(other.nomLigne))
			return false;
		if (nomStation == null) {
			if (other.nomStation != null)
				return false;
		} else if (!nomStation.equals(other.nomStation))
			return false;
		return true;
	}

	public String toString() {
		return "Ligne " + this.getNomLigne() + " en direction de " + this.getNomStation();
	}

}
