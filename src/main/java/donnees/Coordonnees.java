package donnees;

public class Coordonnees {
	private double latitude;
	private double longitude;

	Coordonnees() {
		this.latitude = 0;
		this.longitude = 0;
	}

	public Coordonnees(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	double getLatitude() {
		return latitude;
	}

	void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	double getLongitude() {
		return longitude;
	}

	void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return this.getLatitude() + ", " + this.getLongitude();
	}

	public boolean equals(Object obj) {
		boolean similaires = false;
		if (obj != null && this.getClass() == obj.getClass()) {
			Coordonnees autre = (Coordonnees) obj;
			boolean identiques = this == obj;
			similaires = identiques || (this.latitude == autre.latitude && this.longitude == autre.longitude);
		}
		return similaires;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

}
