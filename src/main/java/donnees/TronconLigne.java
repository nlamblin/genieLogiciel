package donnees;

public class TronconLigne {

	private Station source;
	private Station destination;
	private boolean incident;
	private String ligne;

	public TronconLigne(Station source, Station destination, String ligne) {
		this(source, destination, false, ligne);
	}

	public TronconLigne(Station source, Station destination, boolean incident, String ligne) {
		this.source = source;
		this.destination = destination;
		this.incident = incident;
		this.ligne = ligne;
	}

	@Override
	public boolean equals(Object obj) {
		boolean similaires = false;
		if (obj != null && this.getClass() == obj.getClass()) {
			boolean identiques = this == obj;
			similaires = identiques || (this.source.equals(((TronconLigne) obj).source)
					&& this.destination.equals(((TronconLigne) obj).destination)
					&& this.ligne.equals(((TronconLigne) obj).ligne));
		}
		return similaires;
	}

	@Override
	public String toString() {
		return "TronconLigne entre" + source.getNom() + " et " + destination.getNom() + ", sur la ligne " + ligne;
	}

	public int hashCode() {
		return this.destination.hashCode() * 3 * this.source.hashCode() * ligne.hashCode();
	}

	public boolean isIncident() {
		return incident;
	}

	void setIncident(boolean incident) {
		this.incident = incident;
	}

	Station getSource() {
		return source;
	}

	Station getDestination() {
		return destination;

	}

	public String getLigne() {
		return ligne;
	}
}
