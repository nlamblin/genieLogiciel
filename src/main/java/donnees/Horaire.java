package donnees;

public class Horaire {

	private long heures;
	private long minutes;

	public Horaire(long heures, long minutes) {
		super();
		this.setHeures(heures);
		this.setMinutes(minutes);
	}

	public Horaire(long heureEnSecondes) {

		long heure = heureEnSecondes / 3600;
		long reste = heureEnSecondes - heure * 3600;
		long minute = reste / 60;
		this.setHeures(heure);
		this.setMinutes(minute);
	}

	@Override
	public String toString() {
		return getHeures() + "h" + getMinutes();
	}

	public long convertirEnSecondes() {
		return this.getHeures() * 3600 + this.getMinutes() * 60;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (getHeures() ^ (getHeures() >>> 32));
		result = prime * result + (int) (getMinutes() ^ (getMinutes() >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		boolean similaires = false;
		if (obj != null && this.getClass() == obj.getClass()) {
			boolean identiques = this == obj;
			Horaire autre = (Horaire) obj;
			similaires = identiques
					|| (this.getHeures() == autre.getHeures() && this.getMinutes() == autre.getMinutes());
		}
		return similaires;
	}

	public int compareTo(Horaire horaire) {
		int sortie;
		if (this.getHeures() < horaire.getHeures()) {
			sortie = -1;
		} else if (this.getHeures() > horaire.getHeures()) {
			sortie = 1;
		} else {
			if (this.getMinutes() < horaire.getMinutes()) {
				sortie = -1;
			} else if (this.getMinutes() > horaire.getMinutes()) {
				sortie = 1;
			} else {
				sortie = 0;
			}
		}
		return sortie;

	}

	public long getHeures() {
		return heures;
	}

	public void setHeures(long heures) {
		this.heures = heures;
	}

	public long getMinutes() {
		return minutes;
	}

	public void setMinutes(long minutes) {
		this.minutes = minutes;
	}

}
