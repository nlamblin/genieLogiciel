package donnees;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import calcul.ChangementLigne;
import exceptions.StationDejaExistante;
import exceptions.SuccesseurNoeudDejaExistant;
import interactions.SimpleHandler;

public class Serialisation extends TypeAdapter<Reseau> {

	public static final String CHEMIN_FICHIER_SAUVEGARDE = "src/main/resources/initialisation";
	private static Serialisation instance;
	private Gson gson;
	private static final Logger LOG = Logger.getLogger(Serialisation.class.getName());

	private Serialisation() {

	}

	public static Serialisation getInstance() {
		if (instance == null) {
			GsonBuilder builder = new GsonBuilder();
			instance = new Serialisation();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Reseau.class, instance);
			instance.gson = builder.create();
		}
		return instance;
	}

	public boolean serialize(Reseau reseau, String filename) {
		boolean sauvegarde = true;

		try (Writer out = new FileWriter(filename);) {
			out.write(gson.toJson(reseau));
		} catch (IOException e) {
			LOG.warning(
					"Impossible de sauvegarder ce réseau. \nVeuillez verifier l'existance du fichier de derniere sauvegarde.\nReitérer ensuite votre demande.");
			sauvegarde = false;
		}
		return sauvegarde;
	}

	public Reseau deserialize(String filename) {
		Reseau reseau = null;
		try {
			JsonReader json = new JsonReader(new BufferedReader(new FileReader(filename)));
			reseau = gson.fromJson(json, Reseau.class);
		} catch (IOException e) {
			LOG.warning(
					"Impossible de charger les données correspondantes. \nVeuillez verifier l'existance du fichier \npuis réitérer votre demande");
			System.exit(0);
		}
		return reseau;
	}

	@Override
	public Reseau read(final JsonReader in) throws IOException {
		Reseau reseau = new Reseau();

		in.beginObject();
		while (in.hasNext()) {
			in.nextName();
			String donnees = in.nextString();
			JsonObject json = new JsonParser().parse(donnees).getAsJsonObject();
			for (String cle : json.keySet()) {
				String contenu = json.get(cle).toString();
				switch (cle) {
					case "listeLignes":
						lireLignes(contenu, reseau);
						break;

					case "poidsTroncon":
						lireTroncons(contenu, reseau);
						break;

					case "nomStation":
						lireStations(contenu, reseau);
						break;

					case "tempsChangementLigneStation":
						lireChangements(contenu, reseau);
						break;

					default:
						throw new InputMismatchException("Erreur: Clé [" + cle + "] non supportée");
				}
			}
		}
		in.endObject();

		return reseau;
	}

	private void lireLignes(String contenu, Reseau reseau) {
		JsonArray lignes = new JsonParser().parse(contenu).getAsJsonArray();
		for (int i = 0; i < lignes.size(); i++)
			reseau.ajoutLigne(lignes.get(i).getAsString());
	}

	@SuppressWarnings("all")
	private void lireTroncons(String contenu, Reseau reseau) {
		JsonObject listeSources = new JsonParser().parse(contenu).getAsJsonObject();
		for (String index : listeSources.keySet()) {
			JsonObject donnees = listeSources.get(index).getAsJsonObject();
			Station destination = reseau.getStation(donnees.get("destination").getAsString());
			Station source = reseau.getStation(donnees.get("source").getAsString());
			boolean incident = donnees.get("incident").getAsBoolean();
			int valeur = donnees.get("valeur").getAsInt();
			String ligne = donnees.get("ligne").getAsString();
			TronconLigne troncon = new TronconLigne(source, destination, incident, ligne);
			try {
				reseau.ajoutTroncon(troncon, valeur);
			} catch (SuccesseurNoeudDejaExistant e) {
				LOG.warning(e.getMessage());
			}
		}
	}

	private void lireStations(String contenu, Reseau reseau) {
		JsonObject json = new JsonParser().parse(contenu).getAsJsonObject();
		for (String cle : json.keySet()) {
			JsonObject donnees = json.get(cle).getAsJsonObject();

			Station station = lireStation(donnees);

			try {
				reseau.ajoutStation(station);
			} catch (StationDejaExistante e) {
				LOG.severe(e.getMessage());
			}

			JsonObject listeLignes = donnees.get("listeHoraires").getAsJsonObject();

			for (String nomLigne : listeLignes.keySet()) {

				JsonObject ligne = listeLignes.get(nomLigne).getAsJsonObject();
				for (String nomDestination : ligne.keySet()) {
					JsonObject destination = ligne.get(nomDestination).getAsJsonObject();
					Horaire debut = gson.fromJson(destination.get("debut"), Horaire.class);
					Horaire fin = gson.fromJson(destination.get("fin"), Horaire.class);
					long frequence = destination.get("frequence").getAsLong();
					reseau.ajouterHoraires(station.getNom(), nomDestination, nomLigne, debut, fin, frequence);
				}
			}

		}

	}

	@SuppressWarnings("all")
	private Station lireStation(JsonObject donnees) {
		String nom = donnees.get("nom").getAsString();
		boolean incident = donnees.get("incident").getAsBoolean();
		int tempsArret = donnees.get("tempsArret").getAsInt();
		Coordonnees coords = lireCoordonnees(donnees.get("coordonnees").getAsJsonObject());
		return new Station(nom, incident, tempsArret, coords);
	}

	private Coordonnees lireCoordonnees(JsonObject coords) {
		double latitude = coords.get("latitude").getAsDouble();
		double longitude = coords.get("longitude").getAsDouble();
		return new Coordonnees(latitude, longitude);
	}

	@SuppressWarnings("all")
	private void lireChangements(String contenu, Reseau reseau) {
		JsonObject json = new JsonParser().parse(contenu).getAsJsonObject();
		for (String cle : json.keySet()) {
			JsonObject pair = json.get(cle).getAsJsonObject();
			String nom = pair.get("cle").getAsString();
			JsonObject valeur = pair.get("valeur").getAsJsonObject();
			for (int j = 1; j <= valeur.size(); j++) {
				JsonObject infosChangement = valeur.get(j + "").getAsJsonObject();
				ChangementLigne changement = gson.fromJson(infosChangement.get("cle"), ChangementLigne.class);
				int duree = infosChangement.get("valeur").getAsInt();
				reseau.ajouterChangementLigneStation(new Station(nom), changement.getNomLigneDepart(),
						changement.getNomLigneArrivee(), duree);
			}
		}
	}

	@Override
	public void write(final JsonWriter sortie, final Reseau reseau) throws IOException {
		sortie.beginObject();

		JsonParser parser = new JsonParser();
		JsonObject json = parser.parse(exportToJson(reseau).toString()).getAsJsonObject();

		String prettyJson = new GsonBuilder().setPrettyPrinting().create().toJson(json);

		sortie.name("reseau").jsonValue("\'" + prettyJson + "\'");
		sortie.endObject();
	}

	private JsonObject exportToJson(Reseau reseau) {
		JsonObject json = new JsonObject();
		json.add("listeLignes", listeLignes(reseau));
		json.add("nomStation", ajouterStations(reseau));
		json.add("poidsTroncon", ajouterPoidsTroncons(reseau));
		json.add("tempsChangementLigneStation", ajouterChangementsStation(reseau));

		return json;
	}

	private JsonElement listeLignes(Reseau reseau) {
		JsonArray array = new JsonArray();
		for (String ligne : reseau.getLignes())
			array.add(ligne);
		return array;
	}

	@SuppressWarnings("all")
	private JsonElement changementLigne(HashMap<ChangementLigne, Integer> valeurs) {
		JsonObject json = new JsonObject();
		Integer indexChangement = 1;
		Set<ChangementLigne> changements = valeurs.keySet();
		for (ChangementLigne changement : changements) {
			JsonObject element = new JsonObject();
			JsonElement cle = new JsonParser().parse(gson.toJson(changement));
			element.add("cle", cle);
			element.addProperty("valeur", valeurs.get(changement));
			json.add(indexChangement.toString(), element);
			indexChangement++;
		}
		return json;
	}

	@SuppressWarnings("all")
	private JsonObject ajouterChangementsStation(Reseau reseau) {
		JsonObject json = new JsonObject();
		Integer index = 1;
		HashMap<String, HashMap<ChangementLigne, Integer>> infos = reseau.getTempsChangementLigne();
		Set<String> nomsStations = infos.keySet();
		for (String nom : nomsStations) {
			JsonObject element = new JsonObject();
			element.addProperty("cle", nom);
			element.add("valeur", changementLigne(infos.get(nom)));
			json.add(index.toString(), element);
			index++;
		}
		return json;
	}

	@SuppressWarnings("all")
	private JsonObject ajouterPoidsTroncons(Reseau reseau) {
		JsonObject json = new JsonObject();
		Set<TronconLigne> troncons = reseau.getTronconsReseau();
		Integer index = 0;
		for (TronconLigne tron : troncons) {
			JsonObject donnees = new JsonObject();

			donnees.addProperty("destination", tron.getDestination().getNom());
			donnees.addProperty("source", tron.getSource().getNom());
			donnees.addProperty("incident", tron.isIncident());
			donnees.addProperty("valeur", reseau.getPoidsArc(tron));
			donnees.addProperty("ligne", tron.getLigne());

			json.add(index.toString(), donnees);
			index++;
		}
		return json;
	}

	@SuppressWarnings("all")
	private JsonObject ajouterStations(Reseau reseau) {
		JsonObject json = new JsonObject();
		Integer index = 1;
		List<Station> stations = reseau.getStationsReseau();
		for (Station station : stations) {
			JsonObject attributs = new JsonObject();
			attributs.addProperty("nom", station.getNom());
			attributs.addProperty("incident", station.isIncident());
			attributs.addProperty("tempsArret", station.getTempsArret());
			JsonObject coords = new JsonObject();
			coords.addProperty("latitude", station.getCoordonnees().getLatitude());
			coords.addProperty("longitude", station.getCoordonnees().getLongitude());
			attributs.add("coordonnees", coords);

			JsonObject listeHoraires = new JsonObject();
			JsonObject listeDirections = new JsonObject();
			Map<Direction, List<Horaire>> horairesStation = station.getListeHoraires();
			for (Entry<Direction, List<Horaire>> entree : horairesStation.entrySet()) {
				Direction direction = entree.getKey();
				listeDirections.add(direction.getNomStation(), ajouterHoraireDirection(entree.getValue()));
				String ligne = direction.getNomLigne();
				listeHoraires.add(ligne, listeDirections);
			}

			attributs.add("listeHoraires", listeHoraires);
			json.add(index.toString(), attributs);
			index++;
		}
		return json;
	}

	private JsonObject ajouterHoraireDirection(List<Horaire> horaires) {
		JsonObject json = new JsonObject();
		Horaire horaireDebut = horaires.get(0);
		Horaire horaireFin = horaires.get(horaires.size() - 1);
		Horaire horaireSuivant = horaires.get(1);

		long frequence = horaireSuivant.convertirEnSecondes() - horaireDebut.convertirEnSecondes();
		JsonParser parser = new JsonParser();

		json.add("debut", parser.parse(gson.toJson(horaireDebut)));
		json.add("fin", parser.parse(gson.toJson(horaireFin)));
		json.addProperty("frequence", frequence);

		return json;
	}

	public static void main(String[] args) {
		LOG.setUseParentHandlers(false);
		LOG.addHandler(new SimpleHandler());
		LOG.info("test");

	}

}
