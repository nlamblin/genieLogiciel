package donnees;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import calcul.ChangementLigne;
import exceptions.StationDejaExistante;
import exceptions.SuccesseurNoeudDejaExistant;

public class Reseau {

	private HashMap<TronconLigne, Integer> poidsTroncons; // Map permettant de recenser le poids des arcs
	private HashMap<String, ArrayList<String>> successeursNoeud; // Recense tous les succeseurs des noeuds
	private ArrayList<String> listeLignes;
	private HashMap<String, HashMap<ChangementLigne, Integer>> tempsChangementLigneStation;
	private static final Logger LOG = Logger.getLogger(Reseau.class.getName());

	private HashMap<String, Station> nomsStation;

	public Reseau() {
		this.poidsTroncons = new HashMap<>();
		this.successeursNoeud = new HashMap<>();
		this.listeLignes = new ArrayList<>();
		this.tempsChangementLigneStation = new HashMap<>();
		this.nomsStation = new HashMap<>();
	}

	/*
	 * Fonctions sur les lignes
	 */
	public boolean existeLigne(String nom) {
		return this.listeLignes.contains(nom);
	}

	public boolean ajoutLigne(String nom) {
		boolean ajout = true;
		if (existeLigne(nom)) {
			ajout = false;
		} else {
			this.listeLignes.add(nom);
		}
		return ajout;
	}

	public boolean supprimerLigne(String nom) {
		boolean supp = true;
		if (!existeLigne(nom)) {
			supp = false;
		} else {
			int index = this.listeLignes.indexOf(nom);
			this.listeLignes.remove(index);
			supprimerArcLigne(nom);
		}
		return supp;
	}

	/*
	 * Fonctions sur les incidents sur une ligne du réseau
	 */
	public boolean arcAvecIncident(TronconLigne a) {
		boolean incident = false;
		if (tronconExiste(a)) {
			TronconLigne arcReseau = getTronconReseau(a);
			incident = arcReseau.isIncident();
		}
		return incident;
	}

	public boolean ajoutIncidentLigne(TronconLigne a) {
		return modificationIncidentLigne(a, true);
	}

	public boolean suppressionIncidentLigne(TronconLigne a) {
		return modificationIncidentLigne(a, false);
	}

	boolean modificationIncidentLigne(TronconLigne arc, boolean changementEtat) {
		boolean modif = false;
		if (tronconExiste(arc)) {
			int poids = getPoidsArc(arc);
			supprimerTroncon(arc); // suppression de l'arc dans le graphe pour le modifier
			arc.setIncident(changementEtat);
			try {
				// ajout du nouvel arc
				ajoutTroncon(arc, poids);
			} catch (SuccesseurNoeudDejaExistant e) {
				LOG.severe(e.getMessage());
			}
			modif = true;
		}
		return modif;
	}

	/*
	 * Fonctions sur les incidents sur une station du réseau
	 */
	public boolean stationAvecIncident(Station noeud) {
		Station noeudReseau = getStationReseau(noeud);
		return noeudReseau.isIncident();
	}

	public boolean ajoutIncidentStation(Station noeud) {
		return modificationIncidentStation(noeud, true);
	}

	public boolean suppressionIncidentStation(Station noeud) {
		return modificationIncidentStation(noeud, false);
	}

	boolean modificationIncidentStation(Station noeudModifie, boolean changementEtat) {
		boolean modif = false;

		noeudModifie.setIncident(changementEtat);

		if (stationExiste(noeudModifie)) {
			this.nomsStation.put(noeudModifie.getNom(), noeudModifie);
			modif = true;
		}
		return modif;
	}

	/*
	 * Fonctions sur les noeuds dans le réseau
	 */

	public boolean stationExiste(Station station) {
		return this.nomsStation.containsValue(station);
	}

	public void ajoutStation(Station station) throws StationDejaExistante {
		if (!stationExiste(station)) {
			String nom = station.getNom();
			this.nomsStation.put(nom, station);
			this.successeursNoeud.put(nom, new ArrayList<String>());
			this.tempsChangementLigneStation.put(nom, new HashMap<ChangementLigne, Integer>());
		} else
			throw new StationDejaExistante();
	}

	public boolean supprimerStation(Station station) {
		boolean res = false;
		if (stationExiste(station)) {
			for (Station key : getStationsReseau()) {
				if (key != station) {
					supprimerUnSuccesseurNoeud(key, station);
				}
			}
			String nom = station.getNom();
			this.nomsStation.remove(nom);
			this.successeursNoeud.remove(nom);
			this.tempsChangementLigneStation.remove(nom);
			res = true;

		}
		return res;
	}

	Station getStationReseau(Station noeudRecherche) {
		if (stationExiste(noeudRecherche)) {
			return this.nomsStation.get(noeudRecherche.getNom());

		}
		return null;
	}

	/*
	 * Fonctions sur les arcs dans le réseau
	 */
	public boolean tronconExiste(TronconLigne troncon) {
		return this.poidsTroncons.containsKey(troncon);
	}

	public boolean ajoutTroncon(TronconLigne troncon, int poids) throws SuccesseurNoeudDejaExistant {
		boolean res = false;
		if (stationExiste(troncon.getSource()) && stationExiste(troncon.getDestination()) && !tronconExiste(troncon)) {
			this.poidsTroncons.put(troncon, poids);
			ajoutSuccesseurNoeud(troncon.getSource(), troncon.getDestination());
			res = true;
		}
		return res;
	}

	boolean supprimerTroncon(TronconLigne troncon) {
		boolean res = false;
		if (tronconExiste(troncon)) {
			this.poidsTroncons.remove(troncon);
			res = true;
		}
		return res;
	}

	// Permet de supprimer tous les liens de la station à la station b
	void supprimerArcEntreStations(Station a, Station b) {
		Set<TronconLigne> setTronconLigne = this.poidsTroncons.keySet(); // On recupère tous les arcs
		ArrayList<TronconLigne> aSupprimer = new ArrayList<>();
		for (TronconLigne troncon : setTronconLigne) {
			if (troncon.getSource().equals(a) && troncon.getDestination().equals(b)) {
				aSupprimer.add(troncon);
			}
		}
		for (TronconLigne troncon : aSupprimer) {
			supprimerTroncon(troncon);
		}
	}

	// Permet de supprimer tous les arcs d'une ligne
	void supprimerArcLigne(String ligne) {
		Set<TronconLigne> setTronconLigne = this.poidsTroncons.keySet(); // On recupère tous les arcs
		ArrayList<TronconLigne> aSupprimer = new ArrayList<>();
		for (TronconLigne troncon : setTronconLigne) {
			if (troncon.getLigne().equals(ligne)) {
				aSupprimer.add(troncon);
			}
		}
		for (TronconLigne troncon : aSupprimer) {
			supprimerTroncon(troncon);
		}
	}

	public int getPoidsArc(TronconLigne a) {
		int poids = -1;
		if (tronconExiste(a)) {
			poids = this.poidsTroncons.get(a);
		}
		return poids;
	}

	TronconLigne getTronconReseau(TronconLigne arcRecherche) {
		if (tronconExiste(arcRecherche)) {
			Set<TronconLigne> arcs = this.poidsTroncons.keySet();
			for (TronconLigne arc : arcs) {
				if (arc.equals(arcRecherche)) {
					return arc;
				}
			}
		}
		return null;
	}

	// Permet de récupérer la liste des arcs entre deux stations
	public List<TronconLigne> getArcEntreStation(Station a, Station b) {
		ArrayList<TronconLigne> liste = new ArrayList<>();
		Set<TronconLigne> map = this.poidsTroncons.keySet();
		for (TronconLigne troncon : map) {
			if (troncon.getSource().equals(a) && troncon.getDestination().equals(b)) {
				liste.add(troncon);
			}
		}
		return liste;
	}

	public Set<TronconLigne> getTronconsReseau() {
		return this.poidsTroncons.keySet();
	}

	/*
	 * Fonctions sur les successeurs
	 */
	void ajoutSuccesseurNoeud(Station source, Station successeur) throws SuccesseurNoeudDejaExistant {
		if (stationExiste(source) && stationExiste(successeur)) {
			ArrayList<String> liste = this.successeursNoeud.get(source.getNom());
			liste.add(successeur.getNom());
			this.successeursNoeud.put(source.getNom(), liste);
		} else
			throw new SuccesseurNoeudDejaExistant();
	}

	/*
	 * Fonction permettant de supprimer un successeur spécifique d'une station
	 */
	public boolean supprimerUnSuccesseurNoeud(Station noeud, Station successeur) {
		boolean res = false;
		if (stationExiste(noeud) && stationExiste(successeur)) {

			ArrayList<String> liste = this.successeursNoeud.get(noeud.getNom());
			int indice = liste.indexOf(successeur.getNom());

			if (indice != -1) { // On a bien supprimé le successeur
				liste.remove(indice);
				// On supprime tous les liens entre les deux stations
				supprimerArcEntreStations(noeud, successeur);
				supprimerArcEntreStations(successeur, noeud);
				res = true;
			}
			this.successeursNoeud.put(noeud.getNom(), liste);
		}
		return res;
	}

	public List<Station> getSuccesseursStation(Station station) {
		ArrayList<Station> stations = new ArrayList<>();
		for (String nomStation : successeursNoeud.get(station.getNom())) {
			stations.add(this.nomsStation.get(nomStation));
		}
		return stations;
	}

	public Map<String, List<Station>> getSuccesseursReseau() {
		List<Station> stations = getStationsReseau();
		HashMap<String, List<Station>> res = new HashMap<>();
		for (Station station : stations) {
			if (successeursNoeud.containsKey(station.getNom())) {
				res.put(station.getNom(), getSuccesseursStation(station));
			}
		}

		return res;
	}

	public List<Station> getStationsReseau() {
		Collection<Station> stations = nomsStation.values();
		return new ArrayList<>(stations);
	}

	public boolean ajouterChangementLigneStation(Station station, String ligneDepart, String ligneArrivee, int duree) {
		boolean ajout = true;
		if (stationExiste(station) && existeLigne(ligneDepart) && existeLigne(ligneArrivee)) {
			// On récupère les temps de changement de cette station
			HashMap<ChangementLigne, Integer> temporaire = getTempsChangementLigneStation(station);
			temporaire.put(new ChangementLigne(ligneDepart, ligneArrivee), duree);
			this.tempsChangementLigneStation.put(station.getNom(), temporaire);
		} else {
			ajout = false;
		}
		return ajout;
	}

	public boolean supprimerChangementLigneStation(Station station, String ligneDepart, String ligneArrivee) {
		boolean supp = false;
		if (stationExiste(station) && existeLigne(ligneDepart) && existeLigne(ligneArrivee)
				&& existanceChangementLigneStation(station, ligneDepart, ligneArrivee)) {
			HashMap<ChangementLigne, Integer> temporaire = getTempsChangementLigneStation(station);
			temporaire.remove(new ChangementLigne(ligneDepart, ligneArrivee));
			this.tempsChangementLigneStation.put(station.getNom(), temporaire);
			supp = true;
		}
		return supp;
	}

	public int getChangementLigneStation(Station station, String ligneDepart, String ligneArrivee) {
		int sortie = -1;
		if (existanceChangementLigneStation(station, ligneDepart, ligneArrivee)) {
			HashMap<ChangementLigne, Integer> temporaire = getTempsChangementLigneStation(station);
			sortie = temporaire.get(new ChangementLigne(ligneDepart, ligneArrivee));
		}
		return sortie;

	}

	public boolean existanceChangementLigneStation(Station station, String ligneDepart, String ligneArrivee) {
		boolean existe = false;
		if (stationExiste(station) && existeLigne(ligneDepart) && existeLigne(ligneArrivee)) {
			HashMap<ChangementLigne, Integer> temporaire = getTempsChangementLigneStation(station);
			existe = temporaire.containsKey(new ChangementLigne(ligneDepart, ligneArrivee));
		}
		return existe;
	}

	HashMap<ChangementLigne, Integer> getTempsChangementLigneStation(Station station) {
		return this.tempsChangementLigneStation.get(station.getNom());
	}

	HashMap<String, HashMap<ChangementLigne, Integer>> getTempsChangementLigne() {
		return this.tempsChangementLigneStation;
	}

	public int getDistance(Station noeud, Coordonnees coordonneesUtilisateur) {

		double latitudeNoeud = noeud.getCoordonnees().getLatitude();
		double longitudeNoeud = noeud.getCoordonnees().getLongitude();
		double latitudeUtilisateur = coordonneesUtilisateur.getLatitude();
		double longitudeUtilisateur = coordonneesUtilisateur.getLongitude();

		double latDistance = Math.toRadians(latitudeUtilisateur - latitudeNoeud);
		double longDistance = Math.toRadians(longitudeUtilisateur - longitudeNoeud);

		final int R = 6371; // rayon de la Terre
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
				+ Math.cos(Math.toRadians(latitudeNoeud)) * Math.cos(Math.toRadians(latitudeUtilisateur))
						* Math.sin(longDistance / 2) * Math.sin(longDistance / 2);

		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c * 1000; // convertir en mètres

		return (int) Math.round(distance);
	}

	public List<String> getLignes() {
		return this.listeLignes;
	}

	void setLignes(ArrayList<String> listeLignes) {
		this.listeLignes = listeLignes;
	}

	public Station getStation(String nom) {
		return this.nomsStation.get(nom);
	}

	@Override
	public boolean equals(Object obj) {
		boolean similaires = false;
		if (obj != null && this.getClass() == obj.getClass()) {
			boolean identiques = this == obj;
			Reseau autre = (Reseau) obj;
			similaires = identiques
					|| (this.listeLignes.equals(autre.listeLignes) && this.poidsTroncons.equals(autre.poidsTroncons)
							&& this.tempsChangementLigneStation.equals(autre.tempsChangementLigneStation)
							&& this.nomsStation.equals(autre.nomsStation));

			Iterator<String> it = successeursNoeud.keySet().iterator();
			while (similaires && it.hasNext()) {
				String nom = it.next();
				ArrayList<String> thisSuccesseurs;
				ArrayList<String> autreSuccesseurs;
				thisSuccesseurs = new ArrayList<>(successeursNoeud.get(nom));
				autreSuccesseurs = new ArrayList<>(autre.successeursNoeud.get(nom));
				Collections.sort(thisSuccesseurs);
				Collections.sort(autreSuccesseurs);
				similaires &= thisSuccesseurs.equals(autreSuccesseurs);
			}
		}
		return similaires;
	}

	// Fonctions sur les horaires

	void ajouterHoraires(Station station, Station stationDirection, String nomLigne, Horaire debut, Horaire fin,
			long frequenceSecondes) {
		ajouterHoraires(station.getNom(), stationDirection.getNom(), nomLigne, debut, fin, frequenceSecondes);
	}

	public void ajouterHoraires(String nomStation, String nomStationDirection, String nomLigne, Horaire debut,
			Horaire fin, long frequenceSecondes) {
		Station station = getStation(nomStation);
		station.ajouterHoraire(debut.convertirEnSecondes(), fin.convertirEnSecondes(), nomLigne, nomStationDirection,
				frequenceSecondes);
		this.nomsStation.put(nomStation, station);

	}

	public List<Horaire> recupererHoraire(String nomStation, String nomStationDirection, String nomLigne) {
		Station station = getStation(nomStation);
		return station.recupererHoraire(nomStationDirection, nomLigne);
	}

	public boolean supprimerHoraire(String nomStation, String nomStationDirection, String nomLigne) {
		Station station = getStation(nomStation);
		boolean supprimer = station.supprimerHoraire(nomStationDirection, nomLigne);
		this.nomsStation.put(station.getNom(), station);
		return supprimer;
	}

	public boolean existanceHoraire(String nomStation, String nomStationDirection, String nomLigne) {
		Station station = getStation(nomStation);
		return station.contientHoraireLigneDirection(nomStationDirection, nomLigne);
	}

	HashMap<String, ArrayList<String>> getSuccesseursNoeud() {
		return this.successeursNoeud;
	}

	public String affichageHoraire(String stationDepart, String stationDirection, String nomLigne) {
		String toString = "\n\n Voici les horaires de départ de cette station : \n";
		StringBuilder horaires = new StringBuilder();
		int compteurAffichage = 0;
		int limiteValeurLigne = 12;
		for (Horaire horaire : this.recupererHoraire(stationDepart, stationDirection, nomLigne)) {
			if (compteurAffichage > limiteValeurLigne) {
				horaires.append("\n" + horaire.toString());
				compteurAffichage = 0;
			} else {
				horaires.append(" | " + horaire.toString());
				compteurAffichage++;
			}
		}
		return toString + horaires.substring(2);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

}
