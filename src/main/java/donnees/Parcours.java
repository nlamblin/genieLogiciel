package donnees;


import java.util.ArrayList;
import java.util.List;

public class Parcours {
	private Reseau reseau;
	private List<Station> stationsParcourues = new ArrayList<>();
	private List<TronconLigne> tronconsUtilises = new ArrayList<>();
	private int distanceTotale;

	public Parcours(List<Station> stations, int distance, List<TronconLigne> troncons, Reseau reseau) {
		super();
		this.stationsParcourues = stations;
		this.distanceTotale = distance;
		this.tronconsUtilises = troncons;
		this.reseau = reseau;
	}

	@Override
	public String toString() {

		StringBuilder toString = new StringBuilder();
		toString.append("\nVoici le chemin trouvé : \n");
		toString.append("Depart à la station " + stationsParcourues.get(0).getNom() + "\n");
		toString.append("Prendre la ligne " + tronconsUtilises.get(0).getLigne() + " \n");

		for (int i = 1; i < stationsParcourues.size() - 1; i++) {
			toString.append("Station : " + stationsParcourues.get(i).getNom() + "\n");
			if (!(tronconsUtilises.get(i).getLigne()).equals(tronconsUtilises.get(i - 1).getLigne())) {
				toString.append(
						"Descendre à cette arrêt pour prendre la ligne :" + tronconsUtilises.get(i).getLigne() + "\n");
				toString.append("Puis : \n");
			}
		}
		toString.append("Puis continuer jusqu'à la station "
				+ stationsParcourues.get(stationsParcourues.size() - 1).getNom() + "\n");

		toString.append("Le temps de trajet de ce parcours est de : " + this.distanceTotale + " minutes");

		toString.append(reseau.affichageHoraire(stationsParcourues.get(0).getNom(), stationsParcourues.get(1).getNom(),
				tronconsUtilises.get(0).getLigne()));

		return toString.toString();
	}

	public List<Station> getStationsParcourues() {
		return stationsParcourues;
	}

	public int getDistanceTotale() {
		return distanceTotale;
	}

	public List<TronconLigne> getTronconsUtilises() {
		return tronconsUtilises;
	}

}
