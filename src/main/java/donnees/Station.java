package donnees;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Station {

	private String nom;
	private boolean incident;
	private int tempsArret;
	private Coordonnees coordonnees;
	private Map<Direction, List<Horaire>> listeHoraires;

	public Station(String nom) {
		this(nom, false, 1);
	}

	public Station(String nom, boolean incident) {
		this(nom, incident, 1);
	}

	public Station(String nom, boolean incident, int tempsArret) {
		this.nom = nom;
		this.incident = incident;
		this.tempsArret = tempsArret;
		this.listeHoraires = new HashMap<>();
	}

	public Station(String nom, boolean incident, int tempsArret, Coordonnees coordonnees) {
		this.nom = nom;
		this.incident = incident;
		this.tempsArret = tempsArret;
		this.coordonnees = coordonnees;
		this.listeHoraires = new HashMap<>();

	}

	@Override
	public boolean equals(Object obj) {
		boolean similaires = false;
		if (obj != null && this.getClass() == obj.getClass()) {
			boolean identiques = this == obj;
			similaires = identiques || this.nom.equals(((Station) obj).nom);
		}
		return similaires;
	}

	public int hashCode() {
		return this.nom.hashCode();
	}

	public String getNom() {
		return nom;
	}

	public boolean isIncident() {
		return this.incident;
	}

	void setIncident(boolean incident) {
		this.incident = incident;
	}

	public int getTempsArret() {
		return this.tempsArret;
	}

	void setTempsArret(int tempsArret) {
		this.tempsArret = tempsArret;
	}

	public String toString() {
		return "Station" + nom;
	}

	Coordonnees getCoordonnees() {
		return this.coordonnees;
	}

	void setCoordonnees(double latitude, double longitude) {
		this.coordonnees.setLatitude(latitude);
		this.coordonnees.setLongitude(longitude);
	}

	ArrayList<Horaire> genererHoraires(long debutHorairesSecondes, long finHorairessecondes, long frequenceSecondes) {
		ArrayList<Horaire> horaires;
		if (debutHorairesSecondes < finHorairessecondes) {
			horaires = genererHoraireJour(debutHorairesSecondes, finHorairessecondes, frequenceSecondes);
		} else {
			horaires = genererHoraireNuit(debutHorairesSecondes, finHorairessecondes, frequenceSecondes);
		}
		return horaires;
	}

	ArrayList<Horaire> genererHoraireJour(long debutHorairesSecondes, long finHorairessecondes,
			long frequenceSecondes) {
		long temporaire = debutHorairesSecondes;
		ArrayList<Horaire> horaires = new ArrayList<>();

		while (temporaire <= finHorairessecondes) {
			horaires.add(new Horaire(temporaire));
			temporaire += frequenceSecondes;
		}
		return horaires;
	}

	ArrayList<Horaire> genererHoraireNuit(long debutHorairesSecondes, long finHorairessecondes,
			long frequenceSecondes) {
		ArrayList<Horaire> horaires = new ArrayList<>();
		boolean changementHeure = false;
		long temporaire = debutHorairesSecondes;

		while (!changementHeure || temporaire <= finHorairessecondes) {
			if (temporaire >= 86400) {
				temporaire = temporaire - 86400;
				changementHeure = true;
			}
			horaires.add(new Horaire(temporaire));
			temporaire += frequenceSecondes;

		}
		return horaires;
	}

	public void ajouterHoraire(long debut, long fin, String nomLigne, String nomStationDirection,
			long frequenceSecondes) {

		this.listeHoraires.put(new Direction(nomStationDirection, nomLigne),
				genererHoraires(debut, fin, frequenceSecondes));
	}

	public List<Horaire> recupererHoraire(String nomLigne, String nomStationDirection) {
		ArrayList<Horaire> liste = new ArrayList<>();
		if (contientHoraireLigneDirection(nomLigne, nomStationDirection)) {
			liste.addAll(this.listeHoraires.get(new Direction(nomLigne, nomStationDirection)));
		}
		return liste;
	}

	public boolean contientHoraireLigneDirection(String nomStationDirection, String nomLigne) {
		return this.listeHoraires.containsKey(new Direction(nomStationDirection, nomLigne));
	}

	public boolean supprimerHoraire(String nomStationDirection, String nomLigne) {
		boolean supprimer = false;
		if (contientHoraireLigneDirection(nomStationDirection, nomLigne)) {
			this.listeHoraires.remove((new Direction(nomStationDirection, nomLigne)));
			supprimer = true;
		}
		return supprimer;
	}

	public Map<Direction, List<Horaire>> getListeHoraires() {
		return this.listeHoraires;
	}

}
