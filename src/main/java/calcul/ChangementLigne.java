package calcul;


public class ChangementLigne {

	private String nomLigneDepart;
	private String nomLigneArrivee;

	public ChangementLigne(String nomLigneDepart, String nomLigneArrivee) {
		super();
		this.nomLigneDepart = nomLigneDepart;
		this.nomLigneArrivee = nomLigneArrivee;
	}

	public String getNomLigneDepart() {
		return nomLigneDepart;
	}

	public String getNomLigneArrivee() {
		return nomLigneArrivee;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nomLigneArrivee == null) ? 0 : nomLigneArrivee.hashCode());
		result = prime * result + ((nomLigneDepart == null) ? 0 : nomLigneDepart.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChangementLigne other = (ChangementLigne) obj;
		if (nomLigneArrivee == null) {
			if (other.nomLigneArrivee != null)
				return false;
		} else if (!nomLigneArrivee.equals(other.nomLigneArrivee))
			return false;
		if (nomLigneDepart == null) {
			if (other.nomLigneDepart != null)
				return false;
		} else if (!nomLigneDepart.equals(other.nomLigneDepart))
			return false;
		return true;
	}

}
