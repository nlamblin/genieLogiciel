package calcul;

import donnees.PredecesseurTroncon;
import donnees.Reseau;
import donnees.Station;
import donnees.TronconLigne;

public class Dijkstra extends AlgorithmeChemin {

	public Dijkstra(Reseau graphe, Station source, Station destination) {
		super(graphe, source, destination, Integer.MAX_VALUE);

	}

	// Dans Dijkstra, distanceStationSource represente la distance en terme de temps
	// sur les arcs
	protected boolean miseAJourPredecesseurNoeud(Station noeudCourant, Station noeudSuivant,
			TronconLigne tronconUtilise) {

		boolean res = reseau.tronconExiste(tronconUtilise);
		if (res) {
			String ligneNoeudCourant = this.dernierLigneUtilisee.get(noeudCourant);
			int nouvelleDistance = this.distanceStationSource.get(noeudCourant) + reseau.getPoidsArc(tronconUtilise);

			if (!ligneNoeudCourant.equals(tronconUtilise.getLigne())) {
				// On prend en compte le temps de changement de ligne s'il y en as
				nouvelleDistance += this.reseau.getChangementLigneStation(noeudCourant, ligneNoeudCourant,
						tronconUtilise.getLigne());

			} else {
				nouvelleDistance += noeudCourant.getTempsArret();
			}
			if (this.distanceStationSource.get(noeudSuivant) > nouvelleDistance) {
				this.predecesseurStation.put(noeudSuivant, new PredecesseurTroncon(noeudCourant, tronconUtilise));
				this.distanceStationSource.put(noeudSuivant, nouvelleDistance);
				this.sommetsResteTraiter.add(noeudSuivant);
				this.dernierLigneUtilisee.put(noeudSuivant, tronconUtilise.getLigne());
			}
		}
		return res;
	}

}
