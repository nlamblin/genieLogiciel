package calcul;

import java.util.List;
import java.util.Map;

import donnees.PredecesseurTroncon;
import donnees.Reseau;
import donnees.Station;
import donnees.TronconLigne;

public class MoinsDeChangement extends AlgorithmeChemin {

	Map<String, List<Station>> map;

	public MoinsDeChangement(Reseau reseau, Station source, Station destination) {
		super(reseau, source, destination, 0);
		map = reseau.getSuccesseursReseau();
	}

	// Dans MoinsDeChangement, distanceStationSource represente la distance en terme
	// de nombre de changements

	protected boolean miseAJourPredecesseurNoeud(Station noeudCourant, Station noeudSuivant,
			TronconLigne tronconUtilise) {
		String ligneNoeudCourant;

		boolean res = reseau.tronconExiste(tronconUtilise);
		if (res) {

			ligneNoeudCourant = this.dernierLigneUtilisee.get(noeudCourant);
			PredecesseurTroncon ancienPredecesseurNoeudSuivant = this.predecesseurStation.get(noeudSuivant);

			if (ancienPredecesseurNoeudSuivant == null) { // 1ere fois que l'on arrive sur ce noeud
				// On créé les 1eres données de cette station
				this.dernierLigneUtilisee.put(noeudSuivant, tronconUtilise.getLigne());

				// On regarde s'il y a changement de ligne depuis le predecesseur
				int nbChangementTemporaire = this.distanceStationSource.get(noeudCourant);
				if (!ligneNoeudCourant.equals(tronconUtilise.getLigne())) {
					// ce n'est pas la meme ligne entre le noeud predecesseur et le troncon utilise
					nbChangementTemporaire += 1; // on a donc un changement au noeud precedent
				}
				this.distanceStationSource.put(noeudSuivant, nbChangementTemporaire);
				this.predecesseurStation.put(noeudSuivant, new PredecesseurTroncon(noeudCourant, tronconUtilise));
				this.sommetsResteTraiter.add(noeudSuivant);
			}

			else { // On est deja arrivé à ce noeud par un autre chemin
				int nbChangementTemporaire = this.distanceStationSource.get(noeudCourant);

				if (!ligneNoeudCourant.equals(tronconUtilise.getLigne())) {
					// ce n'est pas la meme ligne entre le noeud predecesseur et le troncon utilise
					nbChangementTemporaire += 1; // on a donc un changement au noeud precedent
				}
				if (nbChangementTemporaire < this.distanceStationSource.get(noeudSuivant)) {

					// On prend forcement ce chemin car moins de changement
					this.dernierLigneUtilisee.put(noeudSuivant, tronconUtilise.getLigne());
					// on sauvegarde la derniere ligne utilisée
					this.distanceStationSource.put(noeudSuivant, nbChangementTemporaire);
					// la station est à une distance de 0 changements
					this.predecesseurStation.put(noeudSuivant, new PredecesseurTroncon(noeudCourant, tronconUtilise));
					this.sommetsResteTraiter.add(noeudSuivant);
				}
			}

		}
		return res;
	}
}
