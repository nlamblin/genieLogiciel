package calcul;

import java.util.ArrayList;
import java.util.List;

import donnees.Parcours;
import donnees.Reseau;
import donnees.Station;
import donnees.TronconLigne;

public class Itineraire {

	private Reseau reseau;
	private Station source;
	private Station destination;
	private List<Station> sommetsIntermediaires = new ArrayList<>();
	private List<Parcours> listeParcours = new ArrayList<>();
	private Parcours parcours;

	public Itineraire(Reseau graphe, Station source, Station destination, List<Station> sommetsAParcourir) {
		super();
		this.reseau = graphe;
		this.source = source;
		this.destination = destination;
		this.sommetsIntermediaires = sommetsAParcourir;
		this.listeParcours = new ArrayList<>();
	}

	public void algo() {
		if (sommetsIntermediaires.isEmpty()) {
			this.algoSimple();
		} else {
			this.algoComplexe();
		}
	}

	void algoSimple() {
		Dijkstra dijkstra = new Dijkstra(reseau, source, destination);
		dijkstra.algo();
		this.parcours = dijkstra.plusCourtChemin();
	}

	void algoComplexe() {
		sommetsIntermediaires.add(0, this.source);
		sommetsIntermediaires.add(this.destination);
		Dijkstra dijkstra;

		for (int stationCourante = 0; (stationCourante < sommetsIntermediaires.size() - 1); stationCourante++) {
			this.source = sommetsIntermediaires.get(stationCourante);
			this.destination = sommetsIntermediaires.get(stationCourante + 1);
			dijkstra = new Dijkstra(this.reseau, this.source, this.destination);
			dijkstra.algo();
			this.listeParcours.add(dijkstra.plusCourtChemin()); // On récupère tous les parcours
		}
		getParcours(this.listeParcours);
	}

	void getParcours(List<Parcours> listeParcours) {
		ArrayList<Station> listeStation = new ArrayList<>();
		ArrayList<TronconLigne> tronconUtilise = new ArrayList<>();
		int index = 0;
		boolean cheminPossible = true;
		for (Parcours p : listeParcours) {
			if (p.getStationsParcourues() == null) {
				cheminPossible = false;
			} else {
				if (index != 0) {
					listeStation.remove(listeStation.size() - 1);
				}
				listeStation.addAll(p.getStationsParcourues());
				tronconUtilise.addAll(p.getTronconsUtilises());
				index++;

			}
		}

		if (cheminPossible) {
			this.parcours = new Parcours(listeStation, longueurParcours(listeStation, tronconUtilise), tronconUtilise,
					reseau);
		} else {
			this.parcours = new Parcours(null, 0, null, null);
		}

	}

	public Parcours plusCourtChemin() {
		return this.parcours;
	}

	int longueurParcours(ArrayList<Station> parcours, ArrayList<TronconLigne> tronconUtilise) {
		int longueur = this.reseau.getPoidsArc(tronconUtilise.get(0));
		for (int i = 1; i < parcours.size() - 1; i++) {

			if (!(tronconUtilise.get(i).getLigne()).equals(tronconUtilise.get(i - 1).getLigne())) {
				longueur += this.reseau.getChangementLigneStation(parcours.get(i), tronconUtilise.get(i - 1).getLigne(),
						tronconUtilise.get(i).getLigne());

			} else {
				longueur += parcours.get(i).getTempsArret();
			}
			longueur += this.reseau.getPoidsArc(tronconUtilise.get(i));

		}

		return longueur;

	}
}
