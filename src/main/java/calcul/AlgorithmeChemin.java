package calcul;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import donnees.Parcours;
import donnees.PredecesseurTroncon;
import donnees.Reseau;
import donnees.Station;
import donnees.TronconLigne;

public abstract class AlgorithmeChemin {

	protected Station source;
	protected Station destination;
	protected HashMap<Station, String> dernierLigneUtilisee; // pour comptabiliser si changement ou non
	protected HashMap<Station, Integer> distanceStationSource;
	protected HashMap<Station, PredecesseurTroncon> predecesseurStation;
	protected ArrayList<Station> sommetsDejaTraites;
	protected ArrayList<Station> sommetsResteTraiter;
	protected Reseau reseau;

	AlgorithmeChemin(Reseau reseau, Station source, Station destination, int distanceIntialeSommet) {
		this.reseau = reseau;
		this.source = source;
		this.destination = destination;
		this.distanceStationSource = new HashMap<>();
		this.predecesseurStation = new HashMap<>();
		this.sommetsDejaTraites = new ArrayList<>();
		this.sommetsResteTraiter = new ArrayList<>();
		this.dernierLigneUtilisee = new HashMap<>();

		Map<String, List<Station>> map = reseau.getSuccesseursReseau();
		for (String key : map.keySet()) {
			this.distanceStationSource.put(reseau.getStation(key), distanceIntialeSommet);
			this.predecesseurStation.put(reseau.getStation(key), null);
			this.dernierLigneUtilisee.put(reseau.getStation(key), null);
		}

		this.distanceStationSource.put(source, 0);
		this.predecesseurStation.put(source, null);
		this.sommetsResteTraiter.add(source);
	}

	public void algo() {
		Station noeudCourant = source;
		while (!this.sommetsResteTraiter.isEmpty()) { // tant que l'on a pas étudié tout le réseau
			boolean trouverProchainNoeud = false;
			while (!this.sommetsResteTraiter.isEmpty() && !trouverProchainNoeud) {
				noeudCourant = this.sommetsResteTraiter.get(0); // on récupère le 1er noeud
				this.sommetsResteTraiter.remove(0);
				if (!sommetsDejaTraites.contains(noeudCourant)) { // S'il n'a pas deja été traité
					trouverProchainNoeud = true;
				}
			}
			if (trouverProchainNoeud) {
				this.sommetsDejaTraites.add(noeudCourant); // On considère qu'on le traite
				for (Station noeudSuivant : this.reseau.getSuccesseursStation(noeudCourant)) {
					if (!this.reseau.stationAvecIncident(noeudSuivant)) {
						// Tous les successeurs où il n'y a pas d'incidents sur la ligne ou la station

						// pour tous les liens possibles entre ces deux stations
						List<TronconLigne> ligneEntreStation = this.reseau.getArcEntreStation(noeudCourant,
								noeudSuivant);
						for (TronconLigne troncon : ligneEntreStation) {
							if (!this.reseau.arcAvecIncident(troncon)) {
								// S'il on etudie la source, il faut modifier la ligne de depart de celle-ci
								if (noeudCourant.equals(source)) {
									// Si c'est le 1er noeud,on initialise avec ce premier arc
									this.dernierLigneUtilisee.put(noeudCourant, troncon.getLigne());
								}
								miseAJourPredecesseurNoeud(noeudCourant, noeudSuivant, troncon);
							}
						}
					}
				}
			}
		}

	}

	protected abstract boolean miseAJourPredecesseurNoeud(Station noeudCourant, Station noeudSuivant,
			TronconLigne tronconUtilise);

	public Parcours plusCourtChemin() {
		Station noeudCourant = this.destination;
		ArrayList<Station> parcours = new ArrayList<>();
		ArrayList<TronconLigne> tronconUtilise = new ArrayList<>();
		boolean cheminPossible = true;

		while (noeudCourant != this.source && cheminPossible) {
			parcours.add(0, noeudCourant);
			PredecesseurTroncon predecesseurNoeudCourant = this.predecesseurStation.get(noeudCourant);
			// Si l'on est bloqué dès la destination
			if (predecesseurNoeudCourant == null) {
				cheminPossible = false;
			} else {
				tronconUtilise.add(0, predecesseurNoeudCourant.getTronconLigne());
				noeudCourant = predecesseurNoeudCourant.getPredecesseur();
			}
			if (noeudCourant == null) {
				cheminPossible = false;
			}
		}
		Parcours resutlat;
		if (cheminPossible) {
			parcours.add(0, noeudCourant);
			resutlat = new Parcours(parcours, longueurParcours(parcours, tronconUtilise), tronconUtilise, reseau);
		} else {
			resutlat = new Parcours(null, 0, null, null);
		}

		return resutlat;
	}

	int longueurParcours(ArrayList<Station> parcours, ArrayList<TronconLigne> tronconUtilise) {
		int longueur = this.reseau.getPoidsArc(tronconUtilise.get(0));
		for (int i = 1; i < parcours.size() - 1; i++) {

			if (!(tronconUtilise.get(i).getLigne()).equals(tronconUtilise.get(i - 1).getLigne())) {
				longueur += this.reseau.getChangementLigneStation(parcours.get(i), tronconUtilise.get(i - 1).getLigne(),
						tronconUtilise.get(i).getLigne());

			} else {
				longueur += parcours.get(i).getTempsArret();
			}
			longueur += this.reseau.getPoidsArc(tronconUtilise.get(i));

		}

		return longueur;

	}
}
