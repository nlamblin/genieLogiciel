package interactions;

import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import donnees.Serialisation;

public class SimpleLogger {

	private final Logger logger = Logger.getLogger(Serialisation.class.getName());
	private static SimpleLogger instance;

	public static SimpleLogger getLogger() {
		if (instance == null) {
			instance = new SimpleLogger();
		}
		return instance;
	}

	private SimpleLogger() {
		logger.setUseParentHandlers(false);
		SimpleHandler handler = new SimpleHandler();
		handler.setFormatter(new SimpleFormatter() {
			@Override
			public synchronized String format(LogRecord lr) {
				return lr.getMessage();
			}
		});
		logger.addHandler(handler);
	}

	public void log(String message) {
		logger.info(message);
	}

	public void logln(String message) {
		log(message + "\n");
	}

	public void logln(int valeur) {
		log(valeur + "");
	}

}
