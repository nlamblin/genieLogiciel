package interactions;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import donnees.Coordonnees;
import donnees.Direction;
import donnees.Reseau;
import donnees.Station;

public class InteractionsUtilisateur extends Interaction {

	private static final SimpleLogger LOG_CONSOLE = SimpleLogger.getLogger();

	private Coordonnees coordonneesUtilisateur;

	public InteractionsUtilisateur(Reseau graphe) {
		super(graphe);
	}

	private int choixSource() {
		boolean verifChoixSource = false;
		int choix = 0;
		StringBuilder menu = new StringBuilder();
		while (!verifChoixSource) {
			menu.append(">> Choississez votre option de point de départ :\n");
			menu.append("1 - Selectionner une station\n");
			menu.append("2 - Trouver la station la plus proche\n");
			LOG_CONSOLE.log(menu.toString());
			choix = recupererSaisieInt();
			verifChoixSource = verifChoixSource(choix);
		}
		return choix;
	}

	private boolean verifChoixSource(int choix) {
		boolean verif = true;
		if ((choix != 1) && (choix != 2)) {
			verif = false;
		}
		return verif;
	}

	private boolean verifChoixAlgo(int choix) {
		boolean verif = true;
		if (choix < 1 || choix > 5) {
			verif = false;
		}
		return verif;
	}

	public List<Station> recupererListeStationDestination(Station source) {
		List<Station> stationsSansSource = recupererListeStationSansIncident();
		stationsSansSource.remove(stationsSansSource.indexOf(source));
		return stationsSansSource;
	}

	public List<Station> recupererListeStationsIntermediaires(Station source, Station destination,
			List<Station> stationsIntermediaires) {
		List<Station> stations = new ArrayList<>();
		for (Station station : recupererListeStationSansIncident()) {
			if (!station.equals(source) && !station.equals(destination) && !stationsIntermediaires.contains(station)) {
				stations.add(station);
			}
		}
		stations.sort(Comparator.comparing(Station::getNom)); // tri par ordre alphabétique
		return stations;
	}

	public Station recupererSource() {
		int choix = choixSource();
		Station noeud = null;
		switch (choix) {
			case 1:
				noeud = recupererStationDansListe(recupererListeStationSansIncident());
				break;
			case 2:
				double latitude = recupererLatitude();
				double longitude = recupererLongitude();
				ajouterCoordonnees(latitude, longitude);
				noeud = recupererSourcePosition();
				break;
			default:
				break;
		}
		return noeud;
	}

	public void ajouterCoordonnees(double latitude, double longitude) {
		this.coordonneesUtilisateur = new Coordonnees(latitude, longitude);
	}

	public Station recupererSourcePosition() {
		Station noeud = new Station("Introuvable");
		int distanceMin = Integer.MAX_VALUE;

		for (Station station : this.recupererListeStationSansIncident()) {
			int distance = this.reseau.getDistance(station, this.coordonneesUtilisateur);

			if (distance < distanceMin) {
				noeud = station;
				distanceMin = distance;
			}
		}

		String unite = "mètre(s)";
		if (distanceMin >= 1000) {
			distanceMin = distanceMin / 1000;
			unite = "kilomètre(s)";
		}

		LOG_CONSOLE.logln("La station la plus proche est " + noeud.getNom() + ", elle se trouve à " + distanceMin + " "
				+ unite + " de votre position.");
		return noeud;
	}

	public Station recupererDestination(Station source) {
		List<Station> stations = recupererListeStationDestination(source);
		stations = triAlphabetiqueListe(stations);
		afficherListeStation(stations);
		LOG_CONSOLE.logln(">> Veuillez saisir le numero de la station souhaitée");
		LOG_CONSOLE.logln(
				">> Si une station n'est pas disponible dans la liste, c'est qu'elle n'est pas accessible en raison d'un incident.");
		LOG_CONSOLE.logln("ou qu'il s'agit de la station de départ de votre parcours.");
		int numero = recupererNumero();
		boolean verificationNumeroStation = verifNumeroStation(numero, stations);

		while (!verificationNumeroStation) {
			numero = recupererNumero();
			verificationNumeroStation = verifNumeroStation(numero, stations);
		}
		return recupererListeStationDestination(source).get(numero - 1);
	}

	public List<Station> recupererSommetIntermediaireItineraire(Station source, Station destination) {
		LOG_CONSOLE.logln("> Pour exécuter l'itineraire, vous devez choisir des sommets intermédiaires");
		boolean encoreStationsIntermediaires = true;
		List<Station> stationsIntermediaires = new ArrayList<>();

		List<Station> stationsDisponibles = recupererListeStationsIntermediaires(source, destination,
				stationsIntermediaires);
		while (encoreStationsIntermediaires) {
			LOG_CONSOLE.logln("> Saisissez le numéro de la station par lequelle vous voulez passer :");
			afficherListeStation(stationsDisponibles);
			int numero = recupererNumero();
			boolean valide = verifNumeroStation(numero, stationsDisponibles);
			if (valide) {
				stationsIntermediaires.add(stationsDisponibles.get(numero - 1));
				stationsDisponibles.remove(numero - 1);
			}
			encoreStationsIntermediaires = !stationsDisponibles.isEmpty() && demandeContinuer();
		}
		return stationsIntermediaires;
	}

	public int choixAlgoUtilisateur() {
		int choix = 0;
		boolean verifChoixAlgoUtilisateur = false;
		while (!verifChoixAlgoUtilisateur) {
			LOG_CONSOLE.logln("Cette valeur n'est pas valide. Quel algorithme souhaitez-vous utiliser ?");
			LOG_CONSOLE.logln("1- Plus court chemin entre deux sommets\n"
					+ "2- Plus court chemin mais en passant par des sommets intérmédiaire \n"
					+ "3- Chemin avec le moins de changements \n" + "4- Horaires à une station donnée \n"
					+ "5- Afficher stations et troncons avec incidents");
			choix = recupererSaisieInt();
			verifChoixAlgoUtilisateur = verifChoixAlgo(choix);
		}
		return choix;
	}

	private double recupererLatitude() {
		LOG_CONSOLE.logln(">> Veuillez saisir votre latitude (Proche de Paris : entre 48.7507694 et 48.9925018)");
		double latitude = recupererSaisieDouble();
		boolean verification = verifLatitude(latitude);
		while (!verification) {
			LOG_CONSOLE.logln(
					">> Veuillez saisir une latitude proche de la région parisienne (entre 48.7507694 et 48.9925018)");
			latitude = recupererSaisieDouble();
			verification = verifLatitude(latitude);
		}
		return latitude;
	}

	private boolean verifLatitude(double latitude) {
		boolean valide = false;
		// bornes utilisées pour déterminer si la latitude est correcte ou non
		if (latitude > 48.7507694 && latitude < 48.9925018) {
			valide = true;
		}
		return valide;
	}

	private double recupererLongitude() {
		LOG_CONSOLE.logln(">> Veuillez saisir votre longitude (Proche de Paris : entre 2.2167685 et 2.5339018)");
		double longitude = recupererSaisieDouble();
		boolean verification = verifLongitude(longitude);
		while (!verification) {
			LOG_CONSOLE.logln(
					">> Veuillez saisir une longitude proche de la région parisienne (entre 2.2167685 et 2.5339018)");
			longitude = recupererSaisieDouble();
			verification = verifLongitude(longitude);
		}
		return longitude;
	}

	private boolean verifLongitude(double longitude) {
		boolean valide = false;
		// bornes utilisées pour déterminer si la longitude est correcte ou non
		if (longitude > 2.2167685 && longitude < 2.5339018) {
			valide = true;
		}
		return valide;
	}

	public Direction recupererLigne(Station stationSouhaitee) {
		ArrayList<Direction> directions = new ArrayList<>(stationSouhaitee.getListeHoraires().keySet());

		afficherListeDirection(directions);

		LOG_CONSOLE.logln(">> Veuillez saisir le numero correspondant à la direction et la ligne souhaitée");
		int numero = recupererNumero();
		boolean verificationNumeroDirection = verifDirection(numero, directions);

		while (!verificationNumeroDirection) {
			numero = recupererNumero();
			verificationNumeroDirection = verifDirection(numero, directions);
		}

		return directions.get(numero - 1);
	}

	private boolean verifDirection(int numero, ArrayList<Direction> directions) {
		boolean valide = true;

		if (numero <= 0 || numero >= directions.size() + 1) {
			valide = false;
			LOG_CONSOLE.logln("Veuillez saisir un numéro de direction valide.");
		}

		return valide;
	}

	private void afficherListeDirection(ArrayList<Direction> directions) {
		int index = 1;
		for (Direction direction : directions) {
			LOG_CONSOLE.logln(index + " - " + direction.toString());
			index++;
		}
	}

}
