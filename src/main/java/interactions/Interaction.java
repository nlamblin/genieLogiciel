package interactions;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import donnees.Reseau;
import donnees.Station;
import donnees.TronconLigne;

public class Interaction {
	protected Reseau reseau;
	private static final String SEPARATEUR = "-----------------------------------------------";
	private static final SimpleLogger LOG_CONSOLE = SimpleLogger.getLogger();

	public Interaction(Reseau graphe) {
		this.reseau = graphe;
	}

	public int recupererSaisieInt() {
		Scanner sc = new Scanner(System.in);
		int res = 0;
		boolean valide = false;
		while (!valide) {

			try {
				res = sc.nextInt();
				valide = true;
			} catch (InputMismatchException e) {
				LOG_CONSOLE.logln("La valeur saisie n'est pas un entier");
				sc.nextLine(); // /!\ Permet de vider le buffer pour ne pas boucler
				valide = false;
			}

		}
		return res;
	}

	public String recupererSaisieString() {
		Scanner sc = new Scanner(System.in);
		return sc.nextLine();
	}

	protected double recupererSaisieDouble() {
		Scanner sc = new Scanner(System.in);
		double res = 0;
		boolean valide = false;
		while (!valide) {
			try {
				res = sc.nextDouble();
				valide = true;
			} catch (InputMismatchException e) {
				LOG_CONSOLE.logln("La valeur saisie n'est pas un double. Veuillez retenter.");
				sc.nextLine(); // /!\ Permet de vider le buffer pour ne pas boucler
				valide = false;
			}
		}
		return res;
	}

	protected boolean demandeContinuer() {
		LOG_CONSOLE.logln("> Souhaitez vous continuer ? Rentrez 'oui' si oui, autre chose sinon");
		String choix = recupererSaisieString();
		boolean continuer = true;
		if (!choix.equals("oui")) {
			continuer = false;
		}
		return continuer;
	}

	/*
	 * Fonctions sur les stations
	 */

	protected List<Station> triAlphabetiqueListe(List<Station> liste) {
		ArrayList<Station> res = new ArrayList<>(liste);
		res.sort(Comparator.comparing(Station::getNom));
		return res;
	}

	public void afficherListeStation(List<Station> stations) {
		if (stations.isEmpty()) {
			LOG_CONSOLE.logln("Aucune station ne correspond à la demande");
		} else {
			int index = 1;
			for (Station station : triAlphabetiqueListe(stations)) {
				LOG_CONSOLE.logln(index + " - " + station.getNom());
				index++;
			}
		}
	}

	protected int recupererNumero() {
		return recupererSaisieInt();
	}

	public List<Station> recupererListeToutesStations() {
		List<Station> stations = new ArrayList<>(this.reseau.getStationsReseau());
		return triAlphabetiqueListe(stations);
	}

	public List<Station> recupererListeStationAvecIncident() {
		List<Station> stations = recupererListeToutesStations();
		List<Station> stationsIncident = new ArrayList<>();
		for (Station station : stations) {
			if (station.isIncident()) {
				stationsIncident.add(station);
			}
		}
		return stationsIncident;
	}

	public List<Station> recupererListeStationSansIncident() {
		List<Station> stations = recupererListeToutesStations();
		List<Station> stationsIncident = new ArrayList<>();
		for (Station station : stations) {
			if (!station.isIncident()) {
				stationsIncident.add(station);
			}
		}
		return stationsIncident;
	}

	public boolean verifNumeroStation(int numero, List<Station> stations) {
		boolean res;
		if (numero > 0 && numero < stations.size() + 1) {
			res = true;
		} else {
			res = false;
			LOG_CONSOLE.logln("Veuillez saisir un numero de station valide.");
		}

		return res;
	}

	public Station recupererStationDansListe(List<Station> listeStation) {
		listeStation = triAlphabetiqueListe(listeStation);
		afficherListeStation(listeStation);
		LOG_CONSOLE.logln(">> Veuillez saisir le numero de la station souhaitée");
		LOG_CONSOLE.logln(
				">> Si une station n'est pas disponible dans la liste, c'est qu'elle n'est pas accessible en raison d'un incident.");
		int numero = recupererNumero();
		boolean verificationNumeroStation = verifNumeroStation(numero, listeStation);
		while (!verificationNumeroStation) {
			numero = recupererNumero();
			verificationNumeroStation = verifNumeroStation(numero, listeStation);
		}
		Station noeud = listeStation.get(numero - 1);
		LOG_CONSOLE.logln("Voici la station selectionnée : " + noeud.getNom());
		return noeud;
	}

	public void affichageDelimiteur() {
		LOG_CONSOLE.logln(SEPARATEUR);
	}

	/*
	 * Fonctions sur les troncons
	 */
	public List<TronconLigne> recupererListeTousTroncons() {
		List<TronconLigne> troncons = new ArrayList<>();
		troncons.addAll(this.reseau.getTronconsReseau());
		return troncons;
	}

	public List<TronconLigne> recupererListeTronconsAvecIncident() {
		List<TronconLigne> troncons = recupererListeTousTroncons();
		List<TronconLigne> tronconsAvecIncident = new ArrayList<>();
		for (TronconLigne t : troncons) {
			if (t.isIncident()) {
				tronconsAvecIncident.add(t);
			}
		}
		return tronconsAvecIncident;
	}

	protected List<TronconLigne> recupererListeTronconsSansIncident() {
		List<TronconLigne> troncons = recupererListeTousTroncons();
		List<TronconLigne> tronconsAvecIncident = new ArrayList<>();
		for (TronconLigne t : troncons) {
			if (!t.isIncident()) {
				tronconsAvecIncident.add(t);
			}
		}
		return tronconsAvecIncident;
	}

	public void afficherListeTroncons(List<TronconLigne> troncons) {
		if (troncons.isEmpty()) {
			LOG_CONSOLE.logln("Aucun troncon ne correspond à la demande");
		} else {
			int index = 1;
			for (TronconLigne troncon : troncons) {
				LOG_CONSOLE.logln(index + " - " + troncon);
				index++;
			}
		}
	}

	boolean verifNumeroTroncon(int numero, List<TronconLigne> troncons) {
		boolean res;
		if (numero > 0 && numero < troncons.size() + 1) {
			res = true;
		} else {
			res = false;
			LOG_CONSOLE.logln("Veuillez saisir un numero de station valide.");
		}

		return res;
	}

	protected TronconLigne recupererTroncon(List<TronconLigne> troncons) {
		afficherListeTroncons(troncons);
		LOG_CONSOLE.logln(">> Veuillez saisir le numero du troncon souhaité");
		int numero = recupererNumero();
		boolean verificationNumero = verifNumeroTroncon(numero, troncons);
		while (!verificationNumero) {
			numero = recupererNumero();
			verificationNumero = verifNumeroTroncon(numero, troncons);
		}
		TronconLigne t = troncons.get(numero - 1);
		LOG_CONSOLE.logln("Voici le troncon selectionné : " + t);
		return t;
	}

	public void optionAfficherIncidents() {
		LOG_CONSOLE.logln(">> Nous allons afficher les stations et les troncons avec incidents !\n");
		LOG_CONSOLE.logln(SEPARATEUR + "STATIONS\n" + SEPARATEUR);
		List<Station> stationsIncidents = recupererListeStationAvecIncident();
		afficherListeStation(stationsIncidents);
		LOG_CONSOLE.logln(SEPARATEUR + "TRONCONS\n" + SEPARATEUR);
		List<TronconLigne> tronconsIncidents = recupererListeTronconsAvecIncident();
		afficherListeTroncons(tronconsIncidents);
	}

}
