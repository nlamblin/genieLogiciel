package interactions;

import java.util.List;

import donnees.Reseau;
import donnees.Station;
import donnees.TronconLigne;

public class InteractionInformaticien extends Interaction {

	private static final SimpleLogger LOG_CONSOLE = SimpleLogger.getLogger();

	public InteractionInformaticien(Reseau reseau) {
		super(reseau);
	}

	public boolean ajoutIncidentStation() {
		boolean ajout = true;
		List<Station> liste = recupererListeStationSansIncident();
		if (liste.isEmpty()) {
			LOG_CONSOLE.logln("Aucun ajout possible. Il n'y a aucune station sans incident");
		} else {
			LOG_CONSOLE.logln("Sur quelle station souhaitez vous ajouter un incident ?");
			Station station = recupererStationDansListe(liste);
			ajout = this.reseau.ajoutIncidentStation(station);
			if (ajout) {
				LOG_CONSOLE.logln("L'incident a bien été ajouté");
			} else {
				LOG_CONSOLE.logln("L'incident n'a pas pu être ajouté");
			}
		}
		return ajout;
	}

	public boolean suppressionIncidentStation() {
		boolean suppr = true;
		List<Station> liste = recupererListeStationAvecIncident();
		if (liste.isEmpty()) {
			LOG_CONSOLE.logln("Aucune suppression possible. Il n'y a aucune station avec incident");
		} else {
			LOG_CONSOLE.logln("Sur quelle station souhaitez vous retirer un incident ?");
			Station station = recupererStationDansListe(liste);
			suppr = this.reseau.suppressionIncidentStation(station);
			if (suppr) {
				LOG_CONSOLE.logln("L'incident a bien été supprimé");
			} else {
				LOG_CONSOLE.logln("L'incident n'a pas pu être supprimé");
			}
		}
		return suppr;
	}

	public boolean ajoutIncidentTroncon() {
		boolean ajout = true;
		List<TronconLigne> liste = recupererListeTronconsSansIncident();
		if (liste.isEmpty()) {
			LOG_CONSOLE.logln("Aucun ajout possible. Il n'y a aucun tronçon sans incident");
		} else {
			LOG_CONSOLE.logln("Sur quel troncon souhaitez vous ajouter un incident ?");
			TronconLigne troncon = recupererTroncon(liste);
			ajout = this.reseau.ajoutIncidentLigne(troncon);
			if (ajout) {
				LOG_CONSOLE.logln("L'incident a bien été ajouté");
			} else {
				LOG_CONSOLE.logln("L'incident n'a pas pu être ajouté");
			}
		}
		return ajout;
	}

	public boolean suppressionIncidentTroncon() {
		boolean suppr = true;
		List<TronconLigne> liste = recupererListeTronconsAvecIncident();
		if (liste.isEmpty()) {
			LOG_CONSOLE.logln("Aucune suppression possible. Il n'y a aucun tronçon avec incident");
		} else {
			LOG_CONSOLE.logln("Sur quel troncon souhaitez vous supprimer un incident ?");
			TronconLigne troncon = recupererTroncon(liste);
			suppr = this.reseau.suppressionIncidentLigne(troncon);
			if (suppr) {
				LOG_CONSOLE.logln("L'incident a bien été supprimé");
			} else {
				LOG_CONSOLE.logln("L'incident n'a pas pu être supprimé");
			}
		}
		return suppr;
	}

}
