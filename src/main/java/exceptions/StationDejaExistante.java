package exceptions;


public class StationDejaExistante extends Exception {

	public StationDejaExistante() {
		super("Station déjà ajoutée à ce Réseau");
	}

}
