package exceptions;


public class SuccesseurNoeudDejaExistant extends Exception {

	public SuccesseurNoeudDejaExistant() {
		super("Successeur déjà ajouté à cette Station");
	}

}
