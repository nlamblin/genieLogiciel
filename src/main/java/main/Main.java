package main;

import donnees.Reseau;
import donnees.Serialisation;
import interactions.Interaction;
import interactions.InteractionsUtilisateur;
import interactions.SimpleLogger;

public class Main {

	private static final String DERNIERE_SAUVEGARDE = "src/main/resources/reseaux/derniereSauvegarde";
	private static final String CHEMIN_SAUVEGARDE = "src/main/resources/reseaux/";
	private static final String[] FICHIERS = { "uneLigneSansIncident", "uneLigneAvecIncident", "multiLigneSansIncident",
			"multiLigneAvecIncident", "derniereSauvegarde" };
	private static int choixGraphe;
	private static Reseau reseau;
	static Interaction interfaceUtilisation;
	private static final Serialisation serial = Serialisation.getInstance();
	private static final SimpleLogger LOG_CONSOLE = SimpleLogger.getLogger();

	public static void main(String[] args) {
		interfaceUtilisation = new InteractionsUtilisateur(reseau);
		boolean go = true;
		while (go) {
			choixGraphe = choixGraphe();
			reseau = creationGraphe();
			if (choixGraphe == 0) {
				go = false;
			} else {
				go = programmePrincipal();
			}
		}
	}

	public static boolean programmePrincipal() {
		boolean go = true;
		int modeOuverture = choixOuvertureProgramme();
		if (modeOuverture == 0) {
			go = false;
		} else if (modeOuverture == 1) {
			// ouverture en mode informaticien
			MainInformaticien main = new MainInformaticien(reseau);
			reseau = main.programmeInformaticien();
			demandeSauvegarde();
		} else {
			// ouveture en mode utilisateur
			MainUtilisateur main = new MainUtilisateur(reseau);
			main.programmeUtilisateur();
		}
		return go;
	}

	private static Reseau creationGraphe() {
		Reseau reseau = null;
		interfaceUtilisation.affichageDelimiteur();
		if (choixGraphe < 6 && choixGraphe > 0) {
			reseau = serial.deserialize(CHEMIN_SAUVEGARDE + FICHIERS[choixGraphe - 1]);
			interfaceUtilisation.affichageDelimiteur();
		}
		return reseau;
	}

	private static boolean sauvegardeGraphe() {
		return serial.serialize(reseau, DERNIERE_SAUVEGARDE);
	}

	private static int choixGraphe() {
		boolean valide = false;
		int choix = 0;
		while (!valide) {
			interfaceUtilisation.affichageDelimiteur();
			StringBuilder menu = new StringBuilder();
			menu.append("Quel graphe souhaitez-vous utiliser ?\n");

			menu.append("1-Réseau à une ligne sans incident\n");
			menu.append("2-Réseau à une ligne avec incidents\n");
			menu.append("3-Réseau multi-ligne sans incident\n");
			menu.append("4-Réseau multi-ligne avec incident\n");
			menu.append("5-Derniere sauvegarde de reseau\n");
			menu.append("0-Quitter le programme\n");
			LOG_CONSOLE.log(menu.toString());

			choix = interfaceUtilisation.recupererSaisieInt();
			if (choix >= 0 && choix < 6) {
				valide = true;
			} else {
				LOG_CONSOLE.logln("Réponse incorrecte.");
			}
		}
		return choix;
	}

	protected static int choixOuvertureProgramme() {
		boolean valide = false;
		int choix = 0;
		StringBuilder menu = new StringBuilder();
		menu.append("Comment souhaitez-vous ouvrir le programme ?\n");
		menu.append("1-Mode informaticien\n");
		menu.append("2-Mode utilisateur\n");
		menu.append("0-Quitter le programme\n");
		while (!valide) {
			LOG_CONSOLE.log(menu.toString());
			choix = interfaceUtilisation.recupererSaisieInt();
			if (choix >= 0 && choix < 3) {
				valide = true;
			}
		}
		return choix;
	}

	private static boolean choixSauvegardeGraphe() {
		boolean valide = false;
		String choix = "x";

		while (!valide) {
			interfaceUtilisation.affichageDelimiteur();
			LOG_CONSOLE.logln(
					"Souhaitez vous garder une sauvegarde de ce réseau ? \nCette sauvegarde entrainera l'écrasement des anciennes sauvegardes effectuées.\n");
			LOG_CONSOLE.logln("Saissisez 'oui' ou 'non'.");
			choix = interfaceUtilisation.recupererSaisieString();
			if (!choix.equals("oui") && !choix.equals("non")) {
				valide = false;
				LOG_CONSOLE.logln("Réponse incorrecte.");
			} else {
				valide = true;
			}
		}
		boolean sortie;
		if (choix.equals("oui")) {
			sortie = true;
		} else {
			sortie = false;
		}
		return sortie;
	}

	private static void demandeSauvegarde() {
		boolean sauvegardeReussie = false;

		while (!sauvegardeReussie) {
			boolean sauvegarde = choixSauvegardeGraphe();
			if (sauvegarde) {
				sauvegardeReussie = sauvegardeGraphe();
			} else {
				sauvegardeReussie = true;
			}
		}
	}

}
