package main;

import java.util.List;

import donnees.Reseau;
import donnees.Station;
import donnees.TronconLigne;
import interactions.InteractionInformaticien;
import interactions.SimpleLogger;

public class MainInformaticien {

	InteractionInformaticien ie;
	Reseau reseau;
	private static final String SEPARATEUR = "-----------------------------------------------";
	private static final SimpleLogger LOG_CONSOLE = SimpleLogger.getLogger();

	public MainInformaticien(Reseau graphe) {
		reseau = graphe;
		ie = new InteractionInformaticien(graphe);
	}

	Reseau programmeInformaticien() {
		int choix = choixOption();
		executionOptions(choix);
		return reseau;
	}

	int choixOption() {
		int choix = 0;
		boolean verifChoix = false;
		StringBuilder menu = new StringBuilder();
		while (!verifChoix) {
			menu.append(">> Que souhaitez vous faire :\n");
			menu.append("1 - Ajouter un incident sur une station\n");
			menu.append("2 - Supprimer un incident sur une station\n");
			menu.append("3 - Ajouter un incident sur un tronçon\n");
			menu.append("4 - Supprimer un incident sur un tronçon\n");
			menu.append("5 - Afficher les stations et les tronçons avec incidents\n");
			LOG_CONSOLE.log(menu.toString());
			choix = ie.recupererSaisieInt();
			verifChoix = verifChoixOption(choix);
		}
		return choix;
	}

	public boolean verifChoixOption(int choix) {
		boolean verifChoix = false;
		if (choix > 0 && choix < 6) {
			verifChoix = true;
		} else {
			LOG_CONSOLE.logln("Réponse invalide");
		}
		return verifChoix;
	}

	void optionAfficherIncidents() {
		LOG_CONSOLE.logln(SEPARATEUR + "\n" + "STATIONS\n" + SEPARATEUR);
		List<Station> stationsIncidents = ie.recupererListeStationAvecIncident();
		ie.afficherListeStation(stationsIncidents);
		LOG_CONSOLE.logln(SEPARATEUR + "\n" + "TRONCONS\n" + SEPARATEUR);
		List<TronconLigne> tronconsIncidents = ie.recupererListeTronconsAvecIncident();
		ie.afficherListeTroncons(tronconsIncidents);
	}

	boolean executionOptions(int option) {
		boolean effectue = true;
		switch (option) {
			case 1:
				effectue = ie.ajoutIncidentStation();
				break;
			case 2:
				effectue = ie.suppressionIncidentStation();
				break;
			case 3:
				effectue = ie.ajoutIncidentTroncon();
				break;
			case 4:
				effectue = ie.suppressionIncidentTroncon();
				break;
			case 5:
				optionAfficherIncidents();
				break;
			default:
				break;
		}
		return effectue;
	}
}
