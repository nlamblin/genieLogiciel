package main;

import java.util.List;

import calcul.Dijkstra;
import calcul.Itineraire;
import calcul.MoinsDeChangement;
import donnees.Direction;
import donnees.Parcours;
import donnees.Reseau;
import donnees.Station;
import interactions.InteractionsUtilisateur;
import interactions.SimpleLogger;

public class MainUtilisateur {
	InteractionsUtilisateur ie;
	Reseau reseau;
	private static final String MESSAGE_CALCUL_IMPOSSIBLE = ">> En raison d'un excès d'incident sur le réseau, cette option n'est pas disponible";
	private static final SimpleLogger LOG_CONSOLE = SimpleLogger.getLogger();

	public MainUtilisateur(Reseau graphe) {
		reseau = graphe;
		ie = new InteractionsUtilisateur(graphe);
	}

	void programmeUtilisateur() {
		int choix = ie.choixAlgoUtilisateur();
		ie.affichageDelimiteur();
		switch (choix) {
			case 1:
				optionPlusCourtChemin();
				break;
			case 2:
				optionItineraire();
				break;
			case 3:
				optionMoinsDeChangement();
				break;
			case 4:
				optionHoraires();
				break;
			case 5:
				ie.optionAfficherIncidents();
				break;
			default:
				break;

		}
		ie.affichageDelimiteur();
		LOG_CONSOLE.logln("Votre demande est terminée.");

	}

	void optionPlusCourtChemin() {
		if (autoriserParcours()) {
			LOG_CONSOLE.logln(
					">> Nous allons donc rechercher le plus court chemin entre un point de départ et une arrivée ! ");
			mainPlusCourtChemin();
		} else {
			LOG_CONSOLE.logln(MESSAGE_CALCUL_IMPOSSIBLE);
			LOG_CONSOLE.logln(
					">> En effet, vous ne pourrez pas choisir de point de départ et d'arrivée pour votre parcours");
		}
	}

	void optionItineraire() {
		if (autoriserItineraire()) {
			LOG_CONSOLE.logln(
					">> Nous allons donc rechercher le plus court chemin entre un point de départ et une arrivée \n"
							+ " en passant par des points intermédiaires ! ");
			mainItineraire();
		} else {
			LOG_CONSOLE.logln(MESSAGE_CALCUL_IMPOSSIBLE);
			LOG_CONSOLE.logln(
					">> En effet, vous ne pourrez pas choisir de point de départ et d'arrivée pour votre parcours, ou encore de points intermédiaires.");
		}
	}

	void optionMoinsDeChangement() {
		if (autoriserParcours()) {
			LOG_CONSOLE.logln(
					">> Nous allons donc chercher le chemin avec le moins de changement entre un point de départ et une arrivée !\n");
			mainMoinsDeChangement();
		} else {
			LOG_CONSOLE.logln(MESSAGE_CALCUL_IMPOSSIBLE);
			LOG_CONSOLE.logln(
					">> En effet, vous ne pourrez pas choisir de point de départ et d'arrivée pour votre parcours");
		}
	}

	void optionHoraires() {
		LOG_CONSOLE.logln(">> Nous allons chercher les horaires pour station et une ligne donnée !\n");
		horairesStationsLigne();
	}

	void mainPlusCourtChemin() {
		ie.affichageDelimiteur();
		LOG_CONSOLE.logln(">> Quel est le point de départ de votre parcours ?");
		Station source = ie.recupererSource();
		ie.affichageDelimiteur();
		LOG_CONSOLE.logln(">> Quel est le point d'arrivé de votre parcours ?");
		Station destination = ie.recupererDestination(source);
		Dijkstra dijkstra = new Dijkstra(reseau, source, destination);
		dijkstra.algo();
		Parcours parcours = dijkstra.plusCourtChemin();
		affichageParcours(parcours, source, destination);
	}

	void mainMoinsDeChangement() {
		ie.affichageDelimiteur();
		LOG_CONSOLE.logln(">> Quel est le point de départ de votre parcours avec le moins de changement ?");
		Station source = ie.recupererSource();
		ie.affichageDelimiteur();
		LOG_CONSOLE.logln(">> Quel est le point de départ de votre parcours avec le moins de changement ?");
		Station destination = ie.recupererDestination(source);
		MoinsDeChangement moinsDeChangement = new MoinsDeChangement(reseau, source, destination);
		moinsDeChangement.algo();
		Parcours parcours = moinsDeChangement.plusCourtChemin();
		affichageParcours(parcours, source, destination);
	}

	public boolean autoriserParcours() {
		boolean autoriser = true;
		if (ie.recupererListeStationSansIncident().size() < 2) {
			autoriser = false;
		}
		return autoriser;
	}

	void mainItineraire() {
		ie.affichageDelimiteur();
		LOG_CONSOLE.logln(">> Quel est le point de départ de votre itinéaire ?");
		Station source = ie.recupererSource();
		ie.affichageDelimiteur();
		LOG_CONSOLE.logln(">> Quel est le point d'arrivée de votre itinéaire ?");
		Station destination = ie.recupererDestination(source);
		List<Station> intermediaire = ie.recupererSommetIntermediaireItineraire(source, destination);
		Itineraire itineraire = new Itineraire(reseau, source, destination, intermediaire);
		itineraire.algo();
		Parcours parcours = itineraire.plusCourtChemin();
		affichageParcours(parcours, source, destination);
	}

	public boolean autoriserItineraire() {
		boolean autoriser = true;
		if (ie.recupererListeStationSansIncident().size() < 3) {
			autoriser = false;
		}
		return autoriser;
	}

	void horairesStationsLigne() {
		ie.affichageDelimiteur();
		LOG_CONSOLE.logln(">> Quel est la station dont vous souhaitez connaître les horaires ?");
		Station stationSouhaitee = ie.recupererStationDansListe(ie.recupererListeToutesStations());
		Direction direction = ie.recupererLigne(stationSouhaitee);
		LOG_CONSOLE.logln(
				reseau.affichageHoraire(stationSouhaitee.getNom(), direction.getNomStation(), direction.getNomLigne()));
	}

	void affichageParcours(Parcours parcours, Station source, Station destination) {
		if (parcours.getStationsParcourues() != null) {
			ie.affichageDelimiteur();
			LOG_CONSOLE.logln("Voici donc le chemin trouvé selon vos critères entre" + source.getNom() + " et "
					+ destination.getNom());
			LOG_CONSOLE.logln(parcours.toString());

		} else {
			ie.affichageDelimiteur();
			LOG_CONSOLE.logln("Votre demande n'a pas pu aboutir en raison d'incidents sur le réseau.");
		}
	}

}
