import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import donnees.Horaire;

class HoraireTest {

	@DisplayName("Comparaison inférieur")
	@Test
	void testInferieur() {
		assertTrue(new Horaire(20, 0).compareTo(new Horaire(20, 10)) < 0);
	}

	@DisplayName("Comparaison supérieur")
	@Test
	void testSuperieur() {
		assertTrue(new Horaire(20, 10).compareTo(new Horaire(20, 0)) > 0);
	}

	@DisplayName("Comparaison egalité")
	@Test
	void testEgalite() {
		assertTrue(new Horaire(20, 12).compareTo(new Horaire(20, 12)) == 0);
	}

	@DisplayName("Conversion en seconde")
	@Test
	void test1() {
		Horaire A = new Horaire(9, 20);
		long enSeconde = A.convertirEnSecondes();
		assertEquals(enSeconde, 33600);
	}

	@DisplayName("Conversion en seconde : inégalité")
	@Test
	void test2() {
		Horaire A = new Horaire(9, 20);
		long enSeconde = A.convertirEnSecondes();
		assertNotEquals(enSeconde, 33601);
	}

	@DisplayName("Constructeur horaire seconde : conversion en heure")
	@Test
	void test3() {
		Horaire A = new Horaire(33600);
		long heures = 9;
		long minutes = 20;
		assertEquals(A.getHeures(), heures);
		assertEquals(A.getMinutes(), minutes);
	}

	@DisplayName("Constructeur horaire seconde : inégalité heure")
	@Test
	void test4() {
		Horaire A = new Horaire(33600);
		long heures = 8;
		long minutes = 20;
		assertNotEquals(A.getHeures(), heures);
		assertEquals(A.getMinutes(), minutes);
	}

	@DisplayName("Constructeur horaire seconde : inégalité minute")
	@Test
	void test5() {
		Horaire A = new Horaire(33600);
		long heures = 9;
		long minutes = 21;
		assertEquals(A.getHeures(), heures);
		assertNotEquals(A.getMinutes(), minutes);
	}

}
