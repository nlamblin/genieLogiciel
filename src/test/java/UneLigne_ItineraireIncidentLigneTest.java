import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import calcul.Itineraire;
import donnees.Parcours;
import donnees.Reseau;
import donnees.Station;
import donnees.TronconLigne;
import exceptions.StationDejaExistante;
import exceptions.SuccesseurNoeudDejaExistant;

public class UneLigne_ItineraireIncidentLigneTest {

	Reseau reseau;

	@Test
	@DisplayName("Incident 1 : Execution d'un itineraire sans intermediaire")
	void itineraireSimpleParcoursIncident1() {
		// De A à D sans passer par l'arc AC
		ajoutIncidentLigne1();

		Itineraire itineraire = new Itineraire(this.reseau, new Station("A"), new Station("D"),
				new ArrayList<Station>());
		itineraire.algo();

		ArrayList<Station> resultatPrevu = new ArrayList<Station>();
		resultatPrevu.addAll(Arrays.asList(new Station("A"), new Station("E"), new Station("F"), new Station("D")));

		Parcours resultatAlgo = itineraire.plusCourtChemin();
		assertEquals(resultatPrevu, resultatAlgo.getStationsParcourues());
		assertEquals(11, resultatAlgo.getDistanceTotale());
	}

	@Test
	@DisplayName("Incident 1 : Execution d'un itineraire avec intermediaire")
	void itineraireComplexeParcoursIncident1() {
		// De A à D, en passant par C, sans passer par l'arc AC
		ajoutIncidentLigne1();

		ArrayList<Station> liste = new ArrayList<Station>();
		liste.add(new Station("C"));
		Itineraire itineraire = new Itineraire(this.reseau, new Station("A"), new Station("D"), liste);
		itineraire.algo();

		ArrayList<Station> resultatPrevu = new ArrayList<Station>();
		resultatPrevu.addAll(Arrays.asList(new Station("A"), new Station("E"), new Station("F"), new Station("C"),
				new Station("F"), new Station("D")));

		Parcours resultatAlgo = itineraire.plusCourtChemin();
		assertEquals(resultatPrevu, resultatAlgo.getStationsParcourues());
		assertEquals(19, resultatAlgo.getDistanceTotale());
	}

	@Test
	@DisplayName("Incident 2 : Execution d'un itineraire sans intermediaire ")
	void itineraireSimpleParcoursIncident2() {
		// Itineraire de A à D, mais avec incident sur FD
		ajoutIncidentLigne2();

		Itineraire itineraire = new Itineraire(this.reseau, new Station("A"), new Station("D"),
				new ArrayList<Station>());
		itineraire.algo();

		ArrayList<Station> resultatPrevu = new ArrayList<Station>();
		resultatPrevu.addAll(Arrays.asList(new Station("A"), new Station("B"), new Station("D")));

		Parcours resultatAlgo = itineraire.plusCourtChemin();
		assertEquals(resultatPrevu, resultatAlgo.getStationsParcourues());
		assertEquals(13, resultatAlgo.getDistanceTotale());
	}

	@Test
	@DisplayName("Incident 2 : Execution d'un itineraire avec intermediaire ")
	void itineraireComplexeParcoursIncident2() {
		// Itineraire de A à D en passant par F mais avec incident sur FD
		ajoutIncidentLigne2();

		ArrayList<Station> liste = new ArrayList<Station>();
		liste.add(new Station("F"));
		Itineraire itineraire = new Itineraire(this.reseau, new Station("A"), new Station("D"), liste);
		itineraire.algo();

		ArrayList<Station> resultatPrevu = new ArrayList<Station>();
		resultatPrevu.addAll(Arrays.asList(new Station("A"), new Station("C"), new Station("F"), new Station("C"),
				new Station("A"), new Station("B"), new Station("D")));

		Parcours resultatAlgo = itineraire.plusCourtChemin();
		assertEquals(resultatPrevu, resultatAlgo.getStationsParcourues());
		assertEquals(31, resultatAlgo.getDistanceTotale());
	}

	/*
	 * Création des graphes
	 */

	@BeforeEach
	void creation() {
		this.reseau = new Reseau();

		reseau.ajoutLigne("1");

		try {
			reseau.ajoutStation(new Station("A"));
			reseau.ajoutStation(new Station("B"));
			reseau.ajoutStation(new Station("C"));
			reseau.ajoutStation(new Station("D"));
			reseau.ajoutStation(new Station("E"));
			reseau.ajoutStation(new Station("F"));
		} catch (StationDejaExistante e) {
			e.printStackTrace();
		}

		try {
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("B"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("C"), "1"), 4);
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("E"), "1"), 5);

			reseau.ajoutTroncon(new TronconLigne(new Station("B"), new Station("A"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("B"), new Station("D"), "1"), 6);

			reseau.ajoutTroncon(new TronconLigne(new Station("C"), new Station("A"), "1"), 4);
			reseau.ajoutTroncon(new TronconLigne(new Station("C"), new Station("F"), "1"), 3);

			reseau.ajoutTroncon(new TronconLigne(new Station("D"), new Station("B"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("D"), new Station("F"), "1"), 1);

			reseau.ajoutTroncon(new TronconLigne(new Station("E"), new Station("A"), "1"), 5);
			reseau.ajoutTroncon(new TronconLigne(new Station("E"), new Station("F"), "1"), 3);

			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("E"), "1"), 3);
			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("C"), "1"), 3);
			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("D"), "1"), 1);
		} catch (SuccesseurNoeudDejaExistant e) {
			e.printStackTrace();
		}

	}

	void ajoutIncidentLigne1() {
		this.reseau.ajoutIncidentLigne(new TronconLigne(new Station("A"), new Station("C"), "1"));
		this.reseau.ajoutIncidentLigne(new TronconLigne(new Station("C"), new Station("A"), "1"));
	}

	void ajoutIncidentLigne2() {
		this.reseau.ajoutIncidentLigne(new TronconLigne(new Station("F"), new Station("D"), "1"));
		this.reseau.ajoutIncidentLigne(new TronconLigne(new Station("D"), new Station("F"), "1"));
	}

}
