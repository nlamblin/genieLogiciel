import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import donnees.Direction;

public class DirectionTest {

	@DisplayName("Egalite de direction")
	@Test
	void test1() {
		Direction A = new Direction("A", "ligne1");
		Direction B = new Direction("A", "ligne1");
		assertEquals(A, B);
	}

	@DisplayName("Ingalite de direction")
	@Test
	void test2() {
		Direction A = new Direction("A", "ligne1");
		Direction B = new Direction("B", "ligne1");
		assertNotEquals(A, B);
	}

	@DisplayName("Ingalite de direction")
	@Test
	void test3() {
		Direction A = new Direction("A", "ligne1 ");
		Direction B = new Direction("A", "ligne1");
		assertNotEquals(A, B);
	}

}
