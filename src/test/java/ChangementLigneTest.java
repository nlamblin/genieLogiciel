import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import calcul.ChangementLigne;

class ChangementLigneTest {

	@DisplayName("Egalite de changement de ligne")
	@Test
	void test1() {
		assertEquals(new ChangementLigne("1", "2"), new ChangementLigne("1", "2"));
		assertEquals(new ChangementLigne("1", "2").hashCode(), new ChangementLigne("1", "2").hashCode());
	}

}
