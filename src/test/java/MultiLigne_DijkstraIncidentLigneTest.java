import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import calcul.Dijkstra;
import donnees.Parcours;
import donnees.Reseau;
import donnees.Station;
import donnees.TronconLigne;
import exceptions.StationDejaExistante;
import exceptions.SuccesseurNoeudDejaExistant;

class MultiLigne_DijkstraIncidentLigneTest {

	Reseau reseau;

	@DisplayName("IncidentLigne 1")
	@Test
	void test1() {
		// De A à D sans passer par AC
		ajoutIncidentLigne1();
		Dijkstra dijkstra = new Dijkstra(this.reseau, new Station("A"), new Station("D"));
		dijkstra.algo();

		ArrayList<Station> resultatPrevu = new ArrayList<Station>();
		resultatPrevu.addAll(Arrays.asList(new Station("A"), new Station("B"), new Station("D")));

		Parcours resultatAlgo = dijkstra.plusCourtChemin();
		assertEquals(resultatPrevu, resultatAlgo.getStationsParcourues());
		assertEquals(14, resultatAlgo.getDistanceTotale());

	}

	@DisplayName("IncidentLigne 2")
	@Test
	void test2() {
		// De A à D sans passer par FD
		ajoutIncidentLigne2();

		Dijkstra dijkstra = new Dijkstra(this.reseau, new Station("A"), new Station("D"));
		dijkstra.algo();

		ArrayList<Station> resultatPrevu = new ArrayList<Station>();
		resultatPrevu.addAll(Arrays.asList(new Station("A"), new Station("B"), new Station("D")));

		Parcours resultatAlgo = dijkstra.plusCourtChemin();
		assertEquals(resultatPrevu, resultatAlgo.getStationsParcourues());
		assertEquals(14, resultatAlgo.getDistanceTotale());

	}

	@BeforeEach
	void creation() {
		// Voir grapheIteration4 dans les ressources
		this.reseau = new Reseau();
		reseau.ajoutLigne("1");
		reseau.ajoutLigne("2");

		try {
			reseau.ajoutStation(new Station("A"));
			reseau.ajoutStation(new Station("B"));
			reseau.ajoutStation(new Station("C"));
			reseau.ajoutStation(new Station("D"));
			reseau.ajoutStation(new Station("E"));
			reseau.ajoutStation(new Station("F"));
		} catch (StationDejaExistante e) {
			e.printStackTrace();
		}

		try {
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("B"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("C"), "2"), 4);
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("E"), "1"), 5);

			reseau.ajoutTroncon(new TronconLigne(new Station("B"), new Station("A"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("B"), new Station("D"), "2"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("B"), new Station("D"), "1"), 7);

			reseau.ajoutTroncon(new TronconLigne(new Station("C"), new Station("A"), "2"), 4);
			reseau.ajoutTroncon(new TronconLigne(new Station("C"), new Station("F"), "2"), 3);

			reseau.ajoutTroncon(new TronconLigne(new Station("D"), new Station("B"), "1"), 7);
			reseau.ajoutTroncon(new TronconLigne(new Station("D"), new Station("B"), "2"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("D"), new Station("F"), "2"), 1);

			reseau.ajoutTroncon(new TronconLigne(new Station("E"), new Station("A"), "1"), 5);
			reseau.ajoutTroncon(new TronconLigne(new Station("E"), new Station("F"), "1"), 3);

			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("E"), "1"), 3);
			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("C"), "2"), 3);
			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("D"), "2"), 1);
		} catch (SuccesseurNoeudDejaExistant e) {
			e.printStackTrace();
		}

		reseau.ajouterChangementLigneStation(new Station("A"), "1", "2", 2);
		reseau.ajouterChangementLigneStation(new Station("A"), "2", "1", 2);

		reseau.ajouterChangementLigneStation(new Station("B"), "1", "2", 4);
		reseau.ajouterChangementLigneStation(new Station("B"), "2", "1", 4);

		reseau.ajouterChangementLigneStation(new Station("D"), "1", "2", 2);
		reseau.ajouterChangementLigneStation(new Station("D"), "2", "1", 3);

		reseau.ajouterChangementLigneStation(new Station("F"), "1", "2", 4);
		reseau.ajouterChangementLigneStation(new Station("F"), "2", "1", 4);

	}

	void ajoutIncidentLigne1() {
		// Voir graphe incidentLigne1 en annexe
		this.reseau.ajoutIncidentLigne(new TronconLigne(new Station("A"), new Station("C"), "2"));
		this.reseau.ajoutIncidentLigne(new TronconLigne(new Station("C"), new Station("A"), "2"));
	}

	void ajoutIncidentLigne2() {
		// Voir graphe incidentLigne2 en annexe
		this.reseau.ajoutIncidentLigne(new TronconLigne(new Station("F"), new Station("D"), "2"));
		this.reseau.ajoutIncidentLigne(new TronconLigne(new Station("D"), new Station("F"), "2"));
	}

}
