import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import donnees.Reseau;
import donnees.Station;
import donnees.TronconLigne;
import exceptions.StationDejaExistante;
import exceptions.SuccesseurNoeudDejaExistant;

class MultiLigne_ReseauTest {

	Reseau reseau;

	@DisplayName("Contient un arc")
	@Test
	void test1() {
		assertTrue(reseau.tronconExiste(new TronconLigne(new Station("B"), new Station("D"), "1")));
		assertTrue(reseau.tronconExiste(new TronconLigne(new Station("B"), new Station("D"), "2")));
	}

	@DisplayName("Ne contient pas un arc ")
	@Test
	void test2() {
		assertFalse(reseau.tronconExiste(new TronconLigne(new Station("A"), new Station("D"), "1")));
		assertFalse(reseau.tronconExiste(new TronconLigne(new Station("A"), new Station("C"), "1")));

	}

	@DisplayName("Verification suppresion d'un noeud - Suppression des arcs")
	@Test
	void test3() {
		this.reseau.supprimerStation(new Station("D"));
		assertFalse(this.reseau.tronconExiste(new TronconLigne(new Station("B"), new Station("D"), "1")));
		assertFalse(this.reseau.tronconExiste(new TronconLigne(new Station("B"), new Station("D"), "2")));
	}

	@DisplayName("Existance ligne")
	@Test
	void test4() {
		assertTrue(this.reseau.existeLigne("1"));
	}

	@DisplayName("Non existance ligne")
	@Test
	void test5() {
		assertFalse(this.reseau.existeLigne("3"));
	}

	@DisplayName("Ajout d'une ligne existante")
	@Test
	void test6() {
		assertFalse(this.reseau.ajoutLigne("1"));
	}

	@DisplayName("Ajout d'une ligne non existante")
	@Test
	void test7() {
		assertTrue(this.reseau.ajoutLigne("3"));
	}

	@DisplayName("Suppression d'une ligne existante")
	@Test
	void test8() {
		assertTrue(this.reseau.supprimerLigne("1"));
	}

	@DisplayName("Suppression d'une ligne non existante")
	@Test
	void test9() {
		assertFalse(this.reseau.supprimerLigne("3"));
	}

	@DisplayName("Suppression d'une ligne : plus d'arcs de cette ligne")
	@Test
	void test10() {
		this.reseau.supprimerLigne("1");
		assertFalse(this.reseau.tronconExiste(new TronconLigne(new Station("B"), new Station("D"), "1")));
	}

	@DisplayName("Ajout d'un changement de ligne - Possible")
	@Test
	void test11() {
		assertTrue(this.reseau.ajouterChangementLigneStation(new Station("F"), "1", "2", 3));
	}

	@DisplayName("Ajout d'un changement de ligne - Impossible")
	@Test
	void test12() {
		assertFalse(this.reseau.ajouterChangementLigneStation(new Station("F"), "1", "3", 3));
	}

	@DisplayName("Existance d'un changement de ligne ")
	@Test
	void test13() {
		assertTrue(this.reseau.existanceChangementLigneStation(new Station("F"), "1", "2"));
	}

	@DisplayName("Suppresion d'un changement de ligne existant")
	@Test
	void test14() {
		assertTrue(this.reseau.supprimerChangementLigneStation(new Station("F"), "1", "2"));
		assertFalse(this.reseau.existanceChangementLigneStation(new Station("F"), "1", "2"));
	}

	@DisplayName("Suppresion d'un changement de ligne inexistant")
	@Test
	void test15() {
		assertFalse(this.reseau.supprimerChangementLigneStation(new Station("C"), "1", "2"));
	}

	@BeforeEach
	void creation() {
		// Voir grapheIteration4 dans les ressources
		this.reseau = new Reseau();
		reseau.ajoutLigne("1");
		reseau.ajoutLigne("2");

		try {
			reseau.ajoutStation(new Station("A"));
			reseau.ajoutStation(new Station("B"));
			reseau.ajoutStation(new Station("C"));
			reseau.ajoutStation(new Station("D"));
			reseau.ajoutStation(new Station("E"));
			reseau.ajoutStation(new Station("F"));
		} catch (StationDejaExistante e) {
			e.printStackTrace();
		}

		try {
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("B"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("C"), "2"), 4);
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("E"), "1"), 5);

			reseau.ajoutTroncon(new TronconLigne(new Station("B"), new Station("A"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("B"), new Station("D"), "2"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("B"), new Station("D"), "1"), 7);

			reseau.ajoutTroncon(new TronconLigne(new Station("C"), new Station("A"), "2"), 4);
			reseau.ajoutTroncon(new TronconLigne(new Station("C"), new Station("F"), "2"), 3);

			reseau.ajoutTroncon(new TronconLigne(new Station("D"), new Station("B"), "1"), 7);
			reseau.ajoutTroncon(new TronconLigne(new Station("D"), new Station("B"), "2"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("D"), new Station("F"), "2"), 1);

			reseau.ajoutTroncon(new TronconLigne(new Station("E"), new Station("A"), "1"), 5);
			reseau.ajoutTroncon(new TronconLigne(new Station("E"), new Station("F"), "1"), 3);

			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("E"), "1"), 3);
			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("C"), "2"), 3);
			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("D"), "2"), 1);
		} catch (SuccesseurNoeudDejaExistant e) {
			e.printStackTrace();
		}

		reseau.ajouterChangementLigneStation(new Station("A"), "1", "2", 2);
		reseau.ajouterChangementLigneStation(new Station("A"), "2", "1", 2);

		reseau.ajouterChangementLigneStation(new Station("B"), "1", "2", 4);
		reseau.ajouterChangementLigneStation(new Station("B"), "2", "1", 4);

		reseau.ajouterChangementLigneStation(new Station("D"), "1", "2", 2);
		reseau.ajouterChangementLigneStation(new Station("D"), "2", "1", 3);

		reseau.ajouterChangementLigneStation(new Station("F"), "1", "2", 4);
		reseau.ajouterChangementLigneStation(new Station("F"), "2", "1", 4);

		reseau.ajouterChangementLigneStation(new Station("E"), "1", "2", 1);
		reseau.ajouterChangementLigneStation(new Station("E"), "2", "1", 1);
	}

}
