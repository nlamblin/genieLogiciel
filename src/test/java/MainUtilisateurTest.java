import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import donnees.Reseau;
import donnees.Station;
import donnees.TronconLigne;
import exceptions.StationDejaExistante;
import exceptions.SuccesseurNoeudDejaExistant;
import main.MainUtilisateur;

public class MainUtilisateurTest {
	Reseau reseau;
	MainUtilisateur mu;

	@DisplayName("Autoriser parcours plus court chemin sans incident")
	@Test
	void test1() {
		this.reseau.ajoutIncidentStation(new Station("A"));
		this.reseau.ajoutIncidentStation(new Station("B"));
		this.reseau.ajoutIncidentStation(new Station("C"));
		this.reseau.ajoutIncidentStation(new Station("D"));
		assertTrue(mu.autoriserParcours());
	}

	@DisplayName("Autoriser parcours plus court chemin avec incident")
	@Test
	void test2() {
		this.reseau.ajoutIncidentStation(new Station("A"));
		this.reseau.ajoutIncidentStation(new Station("B"));
		this.reseau.ajoutIncidentStation(new Station("C"));
		this.reseau.ajoutIncidentStation(new Station("D"));
		this.reseau.ajoutIncidentStation(new Station("E"));
		assertFalse(mu.autoriserParcours());
	}

	@DisplayName("Autoriser itinéraire sans incident")
	@Test
	void test3() {
		this.reseau.ajoutIncidentStation(new Station("A"));
		this.reseau.ajoutIncidentStation(new Station("B"));
		this.reseau.ajoutIncidentStation(new Station("C"));
		assertTrue(mu.autoriserItineraire());
	}

	@DisplayName("Autoriser itinéraire avec incident")
	@Test
	void test4() {
		this.reseau.ajoutIncidentStation(new Station("A"));
		this.reseau.ajoutIncidentStation(new Station("B"));
		this.reseau.ajoutIncidentStation(new Station("C"));
		this.reseau.ajoutIncidentStation(new Station("D"));
		assertFalse(mu.autoriserItineraire());
	}

	@BeforeEach
	void creation() {
		this.reseau = new Reseau();
		this.mu = new MainUtilisateur(reseau);

		reseau.ajoutLigne("1");

		try {
			reseau.ajoutStation(new Station("A"));
			reseau.ajoutStation(new Station("B"));
			reseau.ajoutStation(new Station("C"));
			reseau.ajoutStation(new Station("D"));
			reseau.ajoutStation(new Station("E"));
			reseau.ajoutStation(new Station("F"));
		} catch (StationDejaExistante e) {
			e.printStackTrace();
		}

		try {
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("B"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("C"), "1"), 4);
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("E"), "1"), 5);

			reseau.ajoutTroncon(new TronconLigne(new Station("B"), new Station("A"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("B"), new Station("D"), "1"), 6);

			reseau.ajoutTroncon(new TronconLigne(new Station("C"), new Station("A"), "1"), 4);
			reseau.ajoutTroncon(new TronconLigne(new Station("C"), new Station("F"), "1"), 3);

			reseau.ajoutTroncon(new TronconLigne(new Station("D"), new Station("B"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("D"), new Station("F"), "1"), 1);

			reseau.ajoutTroncon(new TronconLigne(new Station("E"), new Station("A"), "1"), 5);
			reseau.ajoutTroncon(new TronconLigne(new Station("E"), new Station("F"), "1"), 3);

			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("E"), "1"), 3);
			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("C"), "1"), 3);
			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("D"), "1"), 1);
		} catch (SuccesseurNoeudDejaExistant e) {
			e.printStackTrace();
		}

	}

}
