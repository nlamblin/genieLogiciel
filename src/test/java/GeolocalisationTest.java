import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import donnees.Coordonnees;
import donnees.Reseau;
import donnees.Station;
import exceptions.StationDejaExistante;
import interactions.InteractionsUtilisateur;

class GeolocalisationTest {

	private Coordonnees coordonneesUtilisateur;
	private Reseau graphe;

	@DisplayName("Test 1 de la distance")
	@Test
	void test1() {
		Station n = new Station("CHATELET", false, 1, new Coordonnees(48.858389963548795, 2.3474837299932694));

		try {
			this.graphe.ajoutStation(n);
		} catch (StationDejaExistante e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(this.graphe.getDistance(n, this.coordonneesUtilisateur), 433);
	}

	@DisplayName("Test 2 de la distance")
	@Test
	void test2() {
		Station n = new Station("VICTOR HUGO", false, 1, new Coordonnees(48.82801275135446, 2.2722000700485663));

		try {
			this.graphe.ajoutStation(n);
		} catch (StationDejaExistante e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(this.graphe.getDistance(n, this.coordonneesUtilisateur), 6156);
	}

	@DisplayName("Verification que la station avec incident n'est pas choisie")
	@Test
	void test3() {
		Station lesSablons = new Station("LES SABLONS", true, 1, new Coordonnees(48.8635862714, 2.46708540814));
		Station argentine = new Station("ARGENTINE", false, 1, new Coordonnees(48.875672486, 2.28944416448));
		Station charlesDeGaulleEtoile = new Station("CHARLES DE GAULLE - ETOILE", false, 1,
				new Coordonnees(48.8744527757, 2.2963258095));

		InteractionsUtilisateur ie = new InteractionsUtilisateur(this.graphe);
		ie.ajouterCoordonnees(48.8635862713, 2.46708540813);

		try {
			this.graphe.ajoutStation(lesSablons);
			this.graphe.ajoutStation(argentine);
			this.graphe.ajoutStation(charlesDeGaulleEtoile);
		} catch (StationDejaExistante e) {
			e.printStackTrace();
		}

		assertNotEquals(ie.recupererSourcePosition(), lesSablons);
	}

	@BeforeEach
	void before() {
		this.graphe = new Reseau();
		this.coordonneesUtilisateur = new Coordonnees(48.8592, 2.3417);
	}

}
