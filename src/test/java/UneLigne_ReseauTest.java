import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import donnees.Reseau;
import donnees.Station;
import donnees.TronconLigne;
import exceptions.StationDejaExistante;
import exceptions.SuccesseurNoeudDejaExistant;

class UneLigne_ReseauTest {

	Reseau reseau;

	@DisplayName("Contient un noeud")
	@Test
	void test() {
		assertTrue(reseau.stationExiste(new Station("A")));
	}

	@DisplayName("Ne contient pas un noeud ")
	@Test
	void test2() {
		assertFalse(reseau.stationExiste(new Station("Z")));
	}

	@DisplayName("Contient un arc")
	@Test
	void test3() {
		assertTrue(reseau.tronconExiste(new TronconLigne(new Station("A"), new Station("B"), "1")));
	}

	@DisplayName("Ne contient pas un arc ")
	@Test
	void test4() {
		assertFalse(reseau.tronconExiste(new TronconLigne(new Station("A"), new Station("D"), "1")));
	}

	@DisplayName("Verification ajout d'un noeud ")
	@Test
	void test5() {
		try {
			this.reseau.ajoutStation(new Station("X"));
		} catch (StationDejaExistante e) {
			e.printStackTrace();
		}
		assertTrue(this.reseau.stationExiste(new Station("X")));
	}

	@DisplayName("Verification ajout d'un noeud existant ")
	@Test
	void test5bis() {
		assertThrows(StationDejaExistante.class, () -> {
			this.reseau.ajoutStation(new Station("B"));
		});
	}

	@DisplayName("Verification suppresion d'un noeud - Noeud non present")
	@Test
	void test6() {
		this.reseau.supprimerStation(new Station("A"));
		assertFalse(this.reseau.stationExiste(new Station("A")));
	}

	@DisplayName("Verification successeur")
	@Test
	void test7() {
		List<Station> successeursDeB = this.reseau.getSuccesseursStation(new Station("B"));
		assertTrue(successeursDeB.contains(new Station("A")));
	}

	@DisplayName("Verification suppresion d'un successeur")
	@Test
	void test8() {
		this.reseau.supprimerUnSuccesseurNoeud(new Station("B"), new Station("A"));
		List<Station> successeursDeB = this.reseau.getSuccesseursStation(new Station("B"));
		assertFalse(successeursDeB.contains(new Station("A")));
	}

	@DisplayName("Verification suppresion d'un noeud - Plus pr�sent dans successeurs")
	@Test
	void test9() {
		this.reseau.supprimerStation(new Station("A"));
		List<Station> successeursDeB = this.reseau.getSuccesseursStation(new Station("B"));
		assertFalse(successeursDeB.contains(new Station("A")));
	}

	@DisplayName("Verification suppresion d'un noeud - Suppression des arcs")
	@Test
	void test10() {
		this.reseau.supprimerStation(new Station("A"));
		assertFalse(this.reseau.tronconExiste(new TronconLigne(new Station("A"), new Station("B"), "1")));
	}

	@DisplayName("Poids d'un arc existant")
	@Test
	void test11() {
		assertEquals(6, this.reseau.getPoidsArc(new TronconLigne(new Station("A"), new Station("B"), "1")));

	}

	@DisplayName("Poids d'un arc inexistant")
	@Test
	void test12() {
		assertEquals(-1, this.reseau.getPoidsArc(new TronconLigne(new Station("A"), new Station("D"), "1")));
	}

	@DisplayName("Ajout d'un incident sur un bout de ligne du réseau")
	@Test
	void test13() {
		this.reseau.ajoutIncidentLigne(new TronconLigne(new Station("A"), new Station("B"), "1"));
		assertTrue(this.reseau.arcAvecIncident(new TronconLigne(new Station("A"), new Station("B"), "1")));
		assertEquals(6, this.reseau.getPoidsArc(new TronconLigne(new Station("A"), new Station("B"), "1")));
	}

	@DisplayName("Suppression d'un incident sur un bout de ligne du réseau")
	@Test
	void test14() {
		this.reseau.suppressionIncidentLigne(new TronconLigne(new Station("A"), new Station("B"), "1"));
		assertFalse(this.reseau.arcAvecIncident(new TronconLigne(new Station("A"), new Station("B"), "1")));
		assertEquals(6, this.reseau.getPoidsArc(new TronconLigne(new Station("A"), new Station("B"), "1")));
	}

	@DisplayName("Ajout d'un incident sur une station du réseau")
	@Test
	void test15() {
		this.reseau.ajoutIncidentStation(new Station("A"));
		// assertTrue(this.graphe.stationAvecIncident(new Station("A")));
		List<Station> successeursDeB = this.reseau.getSuccesseursStation(new Station("B"));
		for (Station successeur : successeursDeB) {
			if (successeur.equals(new Station("A"))) {
				assertTrue(successeur.isIncident());
			}
		}
	}

	@DisplayName("Suppression d'un incident sur une station du réseau")
	@Test
	void test16() {
		this.reseau.suppressionIncidentStation((new Station("A")));
		assertFalse(this.reseau.stationAvecIncident(new Station("A")));
		List<Station> successeursDeB = this.reseau.getSuccesseursStation(new Station("B"));
		for (Station successeur : successeursDeB) {
			if (successeur.equals(new Station("A"))) {
				assertFalse(successeur.isIncident());
			}
		}
	}

	@DisplayName("Existance ligne")
	@Test
	void test17() {
		assertTrue(this.reseau.existeLigne("1"));
	}

	@DisplayName("Non existance ligne")
	@Test
	void test18() {
		assertFalse(this.reseau.existeLigne("2"));
	}

	@DisplayName("Ajout d'une ligne existante")
	@Test
	void test19() {
		assertFalse(this.reseau.ajoutLigne("1"));
	}

	@DisplayName("Ajout d'une ligne non existante")
	@Test
	void test20() {
		assertTrue(this.reseau.ajoutLigne("2"));
	}

	@DisplayName("Suppression d'une ligne existante")
	@Test
	void test21() {
		assertTrue(this.reseau.supprimerLigne("1"));
	}

	@DisplayName("Suppression d'une ligne non existante")
	@Test
	void test22() {
		assertFalse(this.reseau.supprimerLigne("2"));
	}

	@BeforeEach
	void creation() {
		this.reseau = new Reseau();

		reseau.ajoutLigne("1");

		try {
			reseau.ajoutStation(new Station("A"));
			reseau.ajoutStation(new Station("B"));
			reseau.ajoutStation(new Station("C"));
			reseau.ajoutStation(new Station("D"));
			reseau.ajoutStation(new Station("E"));
			reseau.ajoutStation(new Station("F"));
		} catch (StationDejaExistante e) {
			e.printStackTrace();
		}

		try {
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("B"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("C"), "1"), 4);
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("E"), "1"), 5);

			reseau.ajoutTroncon(new TronconLigne(new Station("B"), new Station("A"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("B"), new Station("D"), "1"), 6);

			reseau.ajoutTroncon(new TronconLigne(new Station("C"), new Station("A"), "1"), 4);
			reseau.ajoutTroncon(new TronconLigne(new Station("C"), new Station("F"), "1"), 3);

			reseau.ajoutTroncon(new TronconLigne(new Station("D"), new Station("B"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("D"), new Station("F"), "1"), 1);

			reseau.ajoutTroncon(new TronconLigne(new Station("E"), new Station("A"), "1"), 5);
			reseau.ajoutTroncon(new TronconLigne(new Station("E"), new Station("F"), "1"), 3);

			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("E"), "1"), 3);
			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("C"), "1"), 3);
			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("D"), "1"), 1);
		} catch (SuccesseurNoeudDejaExistant e) {
			e.printStackTrace();
		}

	}

}
