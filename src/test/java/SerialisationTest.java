import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import donnees.Coordonnees;
import donnees.Horaire;
import donnees.Reseau;
import donnees.Serialisation;
import donnees.Station;
import donnees.TronconLigne;
import exceptions.StationDejaExistante;
import exceptions.SuccesseurNoeudDejaExistant;

class SerialisationTest {

	private final static String CHEMIN_FICHIER_TEST = "src/test/resources/initialisationTest";
	private static Serialisation serial;

	@BeforeAll
	static void initialisation() {
		serial = Serialisation.getInstance();
	}

	@Test
	void testSimpleSans() {

		Reseau attendu = null;
		try {
			attendu = genererUneLigneSansIncident();
		} catch (StationDejaExistante | SuccesseurNoeudDejaExistant e) {
			e.printStackTrace();
		}

		serial.serialize(attendu, CHEMIN_FICHIER_TEST);
		Reseau obtenu = serial.deserialize(CHEMIN_FICHIER_TEST);

		assertTrue(attendu.equals(obtenu));
	}

	@Test
	void testMultiSans() {

		Reseau attendu = null;
		try {
			attendu = genererMultiLignesSansIncident();
		} catch (StationDejaExistante | SuccesseurNoeudDejaExistant e) {
			e.printStackTrace();
		}

		serial.serialize(attendu, CHEMIN_FICHIER_TEST);
		Reseau obtenu = serial.deserialize(CHEMIN_FICHIER_TEST);

		assertTrue(attendu.equals(obtenu));
	}

	@Test
	void testSimpleAvec() {

		Reseau attendu = null;
		try {
			attendu = genererUneLigneAvecIncident();
		} catch (StationDejaExistante | SuccesseurNoeudDejaExistant e) {
			e.printStackTrace();
		}

		serial.serialize(attendu, CHEMIN_FICHIER_TEST);
		Reseau obtenu = serial.deserialize(CHEMIN_FICHIER_TEST);

		assertTrue(attendu.equals(obtenu));
	}

	@Test
	void testMultiAvec() {

		Reseau attendu = null;
		try {
			attendu = genererMultiLignesAvecIncident();
		} catch (StationDejaExistante | SuccesseurNoeudDejaExistant e) {
			e.printStackTrace();
		}

		serial.serialize(attendu, CHEMIN_FICHIER_TEST);
		Reseau obtenu = serial.deserialize(CHEMIN_FICHIER_TEST);

		assertTrue(attendu.equals(obtenu));
	}

	private Reseau genererUneLigneSansIncident() throws StationDejaExistante, SuccesseurNoeudDejaExistant {
		Reseau reseau = new Reseau();

		reseau.ajoutLigne("ligne1");
		// Ligne 1
		// Creation des stations
		Station laDefense = new Station("LA DEFENSE", false, 1, new Coordonnees(48.8918614341, 2.23647911585));
		Station esplanadeDeLaDefense = new Station("ESPLANADE DE LA DEFENSE", false, 1,
				new Coordonnees(48.8883580602, 2.24993721286));
		Station pontdeNeuilly = new Station("PONT DE NEUILLY", false, 1, new Coordonnees(48.8855061094, 2.2585274702));
		Station lesSablons = new Station("LES SABLONS", false, 1, new Coordonnees(48.8635862714, 2.46708540814));
		Station porteMaillot = new Station("PORTE MAILLOT", false, 1, new Coordonnees(48.8780061763, 2.28246564));
		Station argentine = new Station("ARGENTINE", false, 1, new Coordonnees(48.875672486, 2.28944416448));
		Station charlesDeGaulleEtoile = new Station("CHARLES DE GAULLE - ETOILE", false, 1,
				new Coordonnees(48.8744527757, 2.2963258095));
		Station georgeV = new Station("GEORGE V", false, 1, new Coordonnees(48.8720456194, 2.30076918564));
		Station franklinDRoosevelt = new Station("FRANKLIN ROOSVELT", false, 1,
				new Coordonnees(48.8688126319, 2.30992632447));
		Station champsElyseesClemenceau = new Station("CHAMPS-ELYSEES-CLEMENCEAU", false, 1,
				new Coordonnees(48.8678427062, 2.31320999172));
		Station concorde = new Station("CONCORDE", false, 1, new Coordonnees(48.8654893909, 2.32141178921));
		Station tuileries = new Station("TUILERIES", false, 1, new Coordonnees(48.8647801625, 2.32909495443));
		Station palaisRoyalMuseeDuLouvre = new Station("PALAIS ROYAL - MUSEE DU LOUVRE", false, 1,
				new Coordonnees(48.8625785271, 2.3360287312));
		Station louvreRivoli = new Station("LOUVRE - RIVOLI", false, 1, new Coordonnees(48.8656520589, 2.34338479681));
		Station chatelet = new Station("CHATELET", false, 1, new Coordonnees(48.8583899635, 2.34748372999));
		Station hotelDeVille = new Station("HOTEL DE VILLE", false, 1, new Coordonnees(48.8560619441, 2.35030260237));
		Station saintPaul = new Station("SAINT-PAUL", false, 1, new Coordonnees(48.8549637098, 2.36156579418));
		Station bastille = new Station("BASTILLE", false, 1, new Coordonnees(48.8529756747, 2.36921882444));
		Station gareDeLyon = new Station("GARE DE LYON", false, 1, new Coordonnees(48.8436904326, 2.37328450012));
		Station reuillyDiderot = new Station("REUILLY - DIDEROT", false, 1,
				new Coordonnees(48.8475229557, 2.38749044347));
		Station nation = new Station("NATION", false, 1, new Coordonnees(48.8491278557, 2.39593524577));
		Station porteDeVincennes = new Station("PORTE DE VINCENNES", false, 1,
				new Coordonnees(48.8470078912, 2.41023131382));
		Station saintMande = new Station("SAINT-MANDE", false, 1, new Coordonnees(48.8462123404, 2.41746102327));
		Station berault = new Station("BERAULT", false, 1, new Coordonnees(48.8456929219, 2.42742804347));
		Station chateauDeVincennes = new Station("CHATEAU DE VINCENNES", false, 1,
				new Coordonnees(48.844089776, 2.44248546714));

		// Ajout des stations dans le graphe
		reseau.ajoutStation(laDefense);
		reseau.ajoutStation(esplanadeDeLaDefense);
		reseau.ajoutStation(pontdeNeuilly);
		reseau.ajoutStation(lesSablons);
		reseau.ajoutStation(porteMaillot);
		reseau.ajoutStation(argentine);
		reseau.ajoutStation(charlesDeGaulleEtoile);
		reseau.ajoutStation(georgeV);
		reseau.ajoutStation(franklinDRoosevelt);
		reseau.ajoutStation(champsElyseesClemenceau);
		reseau.ajoutStation(concorde);
		reseau.ajoutStation(tuileries);
		reseau.ajoutStation(palaisRoyalMuseeDuLouvre);
		reseau.ajoutStation(louvreRivoli);
		reseau.ajoutStation(chatelet);
		reseau.ajoutStation(hotelDeVille);
		reseau.ajoutStation(saintPaul);
		reseau.ajoutStation(bastille);
		reseau.ajoutStation(gareDeLyon);
		reseau.ajoutStation(reuillyDiderot);
		reseau.ajoutStation(nation);
		reseau.ajoutStation(porteDeVincennes);
		reseau.ajoutStation(saintMande);
		reseau.ajoutStation(berault);
		reseau.ajoutStation(chateauDeVincennes);

		// Creation des troncons de lignes
		TronconLigne l1_a1 = new TronconLigne(laDefense, esplanadeDeLaDefense, false, "ligne1");
		TronconLigne l1_a2 = new TronconLigne(esplanadeDeLaDefense, laDefense, false, "ligne1");
		TronconLigne l1_b1 = new TronconLigne(esplanadeDeLaDefense, pontdeNeuilly, false, "ligne1");
		TronconLigne l1_b2 = new TronconLigne(pontdeNeuilly, esplanadeDeLaDefense, false, "ligne1");
		TronconLigne l1_c1 = new TronconLigne(pontdeNeuilly, lesSablons, false, "ligne1");
		TronconLigne l1_c2 = new TronconLigne(lesSablons, pontdeNeuilly, false, "ligne1");
		TronconLigne l1_d1 = new TronconLigne(lesSablons, porteMaillot, false, "ligne1");
		TronconLigne l1_d2 = new TronconLigne(porteMaillot, lesSablons, false, "ligne1");
		TronconLigne l1_e1 = new TronconLigne(porteMaillot, argentine, false, "ligne1");
		TronconLigne l1_e2 = new TronconLigne(argentine, porteMaillot, false, "ligne1");
		TronconLigne l1_f1 = new TronconLigne(argentine, charlesDeGaulleEtoile, false, "ligne1");
		TronconLigne l1_f2 = new TronconLigne(charlesDeGaulleEtoile, argentine, false, "ligne1");
		TronconLigne l1_g1 = new TronconLigne(charlesDeGaulleEtoile, georgeV, false, "ligne1");
		TronconLigne l1_g2 = new TronconLigne(georgeV, charlesDeGaulleEtoile, false, "ligne1");
		TronconLigne l1_h1 = new TronconLigne(georgeV, franklinDRoosevelt, false, "ligne1");
		TronconLigne l1_h2 = new TronconLigne(franklinDRoosevelt, georgeV, false, "ligne1");
		TronconLigne l1_i1 = new TronconLigne(franklinDRoosevelt, champsElyseesClemenceau, false, "ligne1");
		TronconLigne l1_i2 = new TronconLigne(champsElyseesClemenceau, franklinDRoosevelt, false, "ligne1");
		TronconLigne l1_j1 = new TronconLigne(champsElyseesClemenceau, concorde, false, "ligne1");
		TronconLigne l1_j2 = new TronconLigne(concorde, champsElyseesClemenceau, false, "ligne1");
		TronconLigne l1_k1 = new TronconLigne(concorde, tuileries, false, "ligne1");
		TronconLigne l1_k2 = new TronconLigne(tuileries, concorde, false, "ligne1");
		TronconLigne l1_l1 = new TronconLigne(tuileries, palaisRoyalMuseeDuLouvre, false, "ligne1");
		TronconLigne l1_l2 = new TronconLigne(palaisRoyalMuseeDuLouvre, tuileries, false, "ligne1");
		TronconLigne l1_m1 = new TronconLigne(palaisRoyalMuseeDuLouvre, louvreRivoli, false, "ligne1");
		TronconLigne l1_m2 = new TronconLigne(louvreRivoli, palaisRoyalMuseeDuLouvre, false, "ligne1");
		TronconLigne l1_n1 = new TronconLigne(louvreRivoli, chatelet, false, "ligne1");
		TronconLigne l1_n2 = new TronconLigne(chatelet, louvreRivoli, false, "ligne1");
		TronconLigne l1_o1 = new TronconLigne(chatelet, hotelDeVille, false, "ligne1");
		TronconLigne l1_o2 = new TronconLigne(hotelDeVille, chatelet, false, "ligne1");
		TronconLigne l1_p1 = new TronconLigne(hotelDeVille, saintPaul, false, "ligne1");
		TronconLigne l1_p2 = new TronconLigne(saintPaul, hotelDeVille, false, "ligne1");
		TronconLigne l1_q1 = new TronconLigne(saintPaul, bastille, false, "ligne1");
		TronconLigne l1_q2 = new TronconLigne(bastille, saintPaul, false, "ligne1");
		TronconLigne l1_r1 = new TronconLigne(bastille, gareDeLyon, false, "ligne1");
		TronconLigne l1_r2 = new TronconLigne(gareDeLyon, bastille, false, "ligne1");
		TronconLigne l1_s1 = new TronconLigne(gareDeLyon, reuillyDiderot, false, "ligne1");
		TronconLigne l1_s2 = new TronconLigne(reuillyDiderot, gareDeLyon, false, "ligne1");
		TronconLigne l1_t1 = new TronconLigne(reuillyDiderot, nation, false, "ligne1");
		TronconLigne l1_t2 = new TronconLigne(nation, reuillyDiderot, false, "ligne1");
		TronconLigne l1_u1 = new TronconLigne(nation, porteDeVincennes, false, "ligne1");
		TronconLigne l1_u2 = new TronconLigne(porteDeVincennes, nation, false, "ligne1");
		TronconLigne l1_v1 = new TronconLigne(porteDeVincennes, saintMande, false, "ligne1");
		TronconLigne l1_v2 = new TronconLigne(saintMande, porteDeVincennes, false, "ligne1");
		TronconLigne l1_w1 = new TronconLigne(saintMande, berault, false, "ligne1");
		TronconLigne l1_w2 = new TronconLigne(berault, saintMande, false, "ligne1");
		TronconLigne l1_x1 = new TronconLigne(berault, chateauDeVincennes, false, "ligne1");
		TronconLigne l1_x2 = new TronconLigne(chateauDeVincennes, berault, false, "ligne1");

		// Ajout des troncons de ligne dans le graphe
		reseau.ajoutTroncon(l1_a1, 1);
		reseau.ajoutTroncon(l1_a2, 2);
		reseau.ajoutTroncon(l1_b1, 3);
		reseau.ajoutTroncon(l1_b2, 4);
		reseau.ajoutTroncon(l1_c1, 1);
		reseau.ajoutTroncon(l1_c2, 2);
		reseau.ajoutTroncon(l1_d1, 3);
		reseau.ajoutTroncon(l1_d2, 4);
		reseau.ajoutTroncon(l1_e1, 1);
		reseau.ajoutTroncon(l1_e2, 2);
		reseau.ajoutTroncon(l1_f1, 3);
		reseau.ajoutTroncon(l1_f2, 4);
		reseau.ajoutTroncon(l1_g1, 1);
		reseau.ajoutTroncon(l1_g2, 2);
		reseau.ajoutTroncon(l1_h1, 3);
		reseau.ajoutTroncon(l1_h2, 4);
		reseau.ajoutTroncon(l1_i1, 1);
		reseau.ajoutTroncon(l1_i2, 2);
		reseau.ajoutTroncon(l1_j1, 3);
		reseau.ajoutTroncon(l1_j2, 4);
		reseau.ajoutTroncon(l1_k1, 1);
		reseau.ajoutTroncon(l1_k2, 2);
		reseau.ajoutTroncon(l1_l1, 3);
		reseau.ajoutTroncon(l1_l2, 4);
		reseau.ajoutTroncon(l1_m1, 1);
		reseau.ajoutTroncon(l1_m2, 2);
		reseau.ajoutTroncon(l1_n1, 3);
		reseau.ajoutTroncon(l1_n2, 4);
		reseau.ajoutTroncon(l1_o1, 1);
		reseau.ajoutTroncon(l1_o2, 2);
		reseau.ajoutTroncon(l1_p1, 3);
		reseau.ajoutTroncon(l1_p2, 4);
		reseau.ajoutTroncon(l1_q1, 1);
		reseau.ajoutTroncon(l1_q2, 2);
		reseau.ajoutTroncon(l1_r1, 3);
		reseau.ajoutTroncon(l1_r2, 4);
		reseau.ajoutTroncon(l1_s1, 1);
		reseau.ajoutTroncon(l1_s2, 2);
		reseau.ajoutTroncon(l1_t1, 3);
		reseau.ajoutTroncon(l1_t2, 4);
		reseau.ajoutTroncon(l1_u1, 1);
		reseau.ajoutTroncon(l1_u2, 2);
		reseau.ajoutTroncon(l1_v1, 3);
		reseau.ajoutTroncon(l1_v2, 4);
		reseau.ajoutTroncon(l1_w1, 1);
		reseau.ajoutTroncon(l1_w2, 2);
		reseau.ajoutTroncon(l1_x1, 3);
		reseau.ajoutTroncon(l1_x2, 4);

		// HORAIRES LIGNE 1
		// Dans le 1er sens
		reseau.ajouterHoraires(laDefense.getNom(), esplanadeDeLaDefense.getNom(), "ligne1", new Horaire(5, 0),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(esplanadeDeLaDefense.getNom(), pontdeNeuilly.getNom(), "ligne1", new Horaire(5, 2),
				new Horaire(23, 2), 360);
		reseau.ajouterHoraires(pontdeNeuilly.getNom(), lesSablons.getNom(), "ligne1", new Horaire(5, 6),
				new Horaire(23, 6), 360);
		reseau.ajouterHoraires(lesSablons.getNom(), porteMaillot.getNom(), "ligne1", new Horaire(5, 8),
				new Horaire(23, 8), 360);
		reseau.ajouterHoraires(porteMaillot.getNom(), argentine.getNom(), "ligne1", new Horaire(5, 12),
				new Horaire(23, 12), 360);
		reseau.ajouterHoraires(argentine.getNom(), charlesDeGaulleEtoile.getNom(), "ligne1", new Horaire(5, 14),
				new Horaire(23, 14), 360);
		reseau.ajouterHoraires(charlesDeGaulleEtoile.getNom(), georgeV.getNom(), "ligne1", new Horaire(5, 18),
				new Horaire(23, 18), 360);
		reseau.ajouterHoraires(georgeV.getNom(), franklinDRoosevelt.getNom(), "ligne1", new Horaire(5, 20),
				new Horaire(23, 20), 360);
		reseau.ajouterHoraires(franklinDRoosevelt.getNom(), champsElyseesClemenceau.getNom(), "ligne1",
				new Horaire(5, 24), new Horaire(23, 24), 360);
		reseau.ajouterHoraires(champsElyseesClemenceau.getNom(), concorde.getNom(), "ligne1", new Horaire(5, 26),
				new Horaire(23, 26), 360);
		reseau.ajouterHoraires(concorde.getNom(), tuileries.getNom(), "ligne1", new Horaire(5, 30), new Horaire(23, 30),
				360);
		reseau.ajouterHoraires(tuileries.getNom(), palaisRoyalMuseeDuLouvre.getNom(), "ligne1", new Horaire(5, 32),
				new Horaire(23, 32), 360);
		reseau.ajouterHoraires(palaisRoyalMuseeDuLouvre.getNom(), louvreRivoli.getNom(), "ligne1", new Horaire(5, 36),
				new Horaire(23, 36), 360);
		reseau.ajouterHoraires(louvreRivoli.getNom(), chatelet.getNom(), "ligne1", new Horaire(5, 38),
				new Horaire(23, 38), 360);
		reseau.ajouterHoraires(chatelet.getNom(), hotelDeVille.getNom(), "ligne1", new Horaire(5, 42),
				new Horaire(23, 42), 360);
		reseau.ajouterHoraires(hotelDeVille.getNom(), saintPaul.getNom(), "ligne1", new Horaire(5, 44),
				new Horaire(23, 44), 360);
		reseau.ajouterHoraires(saintPaul.getNom(), bastille.getNom(), "ligne1", new Horaire(5, 48), new Horaire(23, 48),
				360);
		reseau.ajouterHoraires(bastille.getNom(), gareDeLyon.getNom(), "ligne1", new Horaire(5, 50),
				new Horaire(23, 50), 360);
		reseau.ajouterHoraires(gareDeLyon.getNom(), reuillyDiderot.getNom(), "ligne1", new Horaire(5, 54),
				new Horaire(23, 54), 360);
		reseau.ajouterHoraires(reuillyDiderot.getNom(), nation.getNom(), "ligne1", new Horaire(5, 56),
				new Horaire(23, 56), 360);
		reseau.ajouterHoraires(nation.getNom(), porteDeVincennes.getNom(), "ligne1", new Horaire(6, 0),
				new Horaire(23, 56), 360);
		reseau.ajouterHoraires(porteDeVincennes.getNom(), saintMande.getNom(), "ligne1", new Horaire(6, 2),
				new Horaire(23, 56), 360);
		reseau.ajouterHoraires(saintMande.getNom(), chateauDeVincennes.getNom(), "ligne1", new Horaire(6, 6),
				new Horaire(23, 56), 360);

		// Dans le 2nd sens

		reseau.ajouterHoraires(esplanadeDeLaDefense.getNom(), laDefense.getNom(), "ligne1", new Horaire(6, 31),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(pontdeNeuilly.getNom(), esplanadeDeLaDefense.getNom(), "ligne1", new Horaire(6, 26),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(lesSablons.getNom(), pontdeNeuilly.getNom(), "ligne1", new Horaire(6, 23),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(porteMaillot.getNom(), lesSablons.getNom(), "ligne1", new Horaire(6, 18),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(argentine.getNom(), porteMaillot.getNom(), "ligne1", new Horaire(6, 15),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(charlesDeGaulleEtoile.getNom(), argentine.getNom(), "ligne1", new Horaire(6, 10),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(georgeV.getNom(), charlesDeGaulleEtoile.getNom(), "ligne1", new Horaire(6, 7),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(franklinDRoosevelt.getNom(), georgeV.getNom(), "ligne1", new Horaire(6, 2),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(champsElyseesClemenceau.getNom(), franklinDRoosevelt.getNom(), "ligne1",
				new Horaire(5, 59), new Horaire(23, 59), 360);
		reseau.ajouterHoraires(concorde.getNom(), champsElyseesClemenceau.getNom(), "ligne1", new Horaire(5, 56),
				new Horaire(23, 56), 360);
		reseau.ajouterHoraires(tuileries.getNom(), concorde.getNom(), "ligne1", new Horaire(5, 51), new Horaire(23, 51),
				360);
		reseau.ajouterHoraires(palaisRoyalMuseeDuLouvre.getNom(), tuileries.getNom(), "ligne1", new Horaire(5, 48),
				new Horaire(23, 48), 360);
		reseau.ajouterHoraires(louvreRivoli.getNom(), palaisRoyalMuseeDuLouvre.getNom(), "ligne1", new Horaire(5, 43),
				new Horaire(23, 43), 360);
		reseau.ajouterHoraires(chatelet.getNom(), louvreRivoli.getNom(), "ligne1", new Horaire(5, 40),
				new Horaire(23, 40), 360);
		reseau.ajouterHoraires(hotelDeVille.getNom(), chatelet.getNom(), "ligne1", new Horaire(5, 37),
				new Horaire(23, 37), 360);
		reseau.ajouterHoraires(saintPaul.getNom(), hotelDeVille.getNom(), "ligne1", new Horaire(5, 32),
				new Horaire(23, 32), 360);
		reseau.ajouterHoraires(bastille.getNom(), saintPaul.getNom(), "ligne1", new Horaire(5, 29), new Horaire(23, 29),
				360);
		reseau.ajouterHoraires(gareDeLyon.getNom(), bastille.getNom(), "ligne1", new Horaire(5, 24),
				new Horaire(23, 24), 360);
		reseau.ajouterHoraires(reuillyDiderot.getNom(), gareDeLyon.getNom(), "ligne1", new Horaire(5, 21),
				new Horaire(23, 21), 360);
		reseau.ajouterHoraires(nation.getNom(), reuillyDiderot.getNom(), "ligne1", new Horaire(5, 16),
				new Horaire(23, 16), 360);
		reseau.ajouterHoraires(porteDeVincennes.getNom(), nation.getNom(), "ligne1", new Horaire(5, 13),
				new Horaire(23, 13), 360);
		reseau.ajouterHoraires(saintMande.getNom(), porteDeVincennes.getNom(), "ligne1", new Horaire(5, 8),
				new Horaire(23, 8), 360);
		reseau.ajouterHoraires(berault.getNom(), saintMande.getNom(), "ligne1", new Horaire(5, 5), new Horaire(23, 5),
				360);
		reseau.ajouterHoraires(chateauDeVincennes.getNom(), berault.getNom(), "ligne1", new Horaire(5, 0),
				new Horaire(23, 0), 360);
		return reseau;
	}

	private Reseau genererMultiLignesSansIncident() throws StationDejaExistante, SuccesseurNoeudDejaExistant {
		Reseau reseau = new Reseau();

		reseau.ajoutLigne("ligne1");
		// Ligne 1
		// Creation des stations
		Station laDefense = new Station("LA DEFENSE", false, 1, new Coordonnees(48.8918614341, 2.23647911585));
		Station esplanadeDeLaDefense = new Station("ESPLANADE DE LA DEFENSE", false, 1,
				new Coordonnees(48.8883580602, 2.24993721286));
		Station pontdeNeuilly = new Station("PONT DE NEUILLY", false, 1, new Coordonnees(48.8855061094, 2.2585274702));
		Station lesSablons = new Station("LES SABLONS", false, 1, new Coordonnees(48.8635862714, 2.46708540814));
		Station porteMaillot = new Station("PORTE MAILLOT", false, 1, new Coordonnees(48.8780061763, 2.28246564));
		Station argentine = new Station("ARGENTINE", false, 1, new Coordonnees(48.875672486, 2.28944416448));
		Station charlesDeGaulleEtoile = new Station("CHARLES DE GAULLE - ETOILE", false, 1,
				new Coordonnees(48.8744527757, 2.2963258095));
		Station georgeV = new Station("GEORGE V", false, 1, new Coordonnees(48.8720456194, 2.30076918564));
		Station franklinDRoosevelt = new Station("FRANKLIN ROOSVELT", false, 1,
				new Coordonnees(48.8688126319, 2.30992632447));
		Station champsElyseesClemenceau = new Station("CHAMPS-ELYSEES-CLEMENCEAU", false, 1,
				new Coordonnees(48.8678427062, 2.31320999172));
		Station concorde = new Station("CONCORDE", false, 1, new Coordonnees(48.8654893909, 2.32141178921));
		Station tuileries = new Station("TUILERIES", false, 1, new Coordonnees(48.8647801625, 2.32909495443));
		Station palaisRoyalMuseeDuLouvre = new Station("PALAIS ROYAL - MUSEE DU LOUVRE", false, 1,
				new Coordonnees(48.8625785271, 2.3360287312));
		Station louvreRivoli = new Station("LOUVRE - RIVOLI", false, 1, new Coordonnees(48.8656520589, 2.34338479681));
		Station chatelet = new Station("CHATELET", false, 1, new Coordonnees(48.8583899635, 2.34748372999));
		Station hotelDeVille = new Station("HOTEL DE VILLE", false, 1, new Coordonnees(48.8560619441, 2.35030260237));
		Station saintPaul = new Station("SAINT-PAUL", false, 1, new Coordonnees(48.8549637098, 2.36156579418));
		Station bastille = new Station("BASTILLE", false, 1, new Coordonnees(48.8529756747, 2.36921882444));
		Station gareDeLyon = new Station("GARE DE LYON", false, 1, new Coordonnees(48.8436904326, 2.37328450012));
		Station reuillyDiderot = new Station("REUILLY - DIDEROT", false, 1,
				new Coordonnees(48.8475229557, 2.38749044347));
		Station nation = new Station("NATION", false, 1, new Coordonnees(48.8491278557, 2.39593524577));
		Station porteDeVincennes = new Station("PORTE DE VINCENNES", false, 1,
				new Coordonnees(48.8470078912, 2.41023131382));
		Station saintMande = new Station("SAINT-MANDE", false, 1, new Coordonnees(48.8462123404, 2.41746102327));
		Station berault = new Station("BERAULT", false, 1, new Coordonnees(48.8456929219, 2.42742804347));
		Station chateauDeVincennes = new Station("CHATEAU DE VINCENNES", false, 1,
				new Coordonnees(48.844089776, 2.44248546714));

		// Ajout des stations dans le graphe
		reseau.ajoutStation(laDefense);
		reseau.ajoutStation(esplanadeDeLaDefense);
		reseau.ajoutStation(pontdeNeuilly);
		reseau.ajoutStation(lesSablons);
		reseau.ajoutStation(porteMaillot);
		reseau.ajoutStation(argentine);
		reseau.ajoutStation(charlesDeGaulleEtoile);
		reseau.ajoutStation(georgeV);
		reseau.ajoutStation(franklinDRoosevelt);
		reseau.ajoutStation(champsElyseesClemenceau);
		reseau.ajoutStation(concorde);
		reseau.ajoutStation(tuileries);
		reseau.ajoutStation(palaisRoyalMuseeDuLouvre);
		reseau.ajoutStation(louvreRivoli);
		reseau.ajoutStation(chatelet);
		reseau.ajoutStation(hotelDeVille);
		reseau.ajoutStation(saintPaul);
		reseau.ajoutStation(bastille);
		reseau.ajoutStation(gareDeLyon);
		reseau.ajoutStation(reuillyDiderot);
		reseau.ajoutStation(nation);
		reseau.ajoutStation(porteDeVincennes);
		reseau.ajoutStation(saintMande);
		reseau.ajoutStation(berault);
		reseau.ajoutStation(chateauDeVincennes);

		// Creation des troncons de lignes
		TronconLigne l1_a1 = new TronconLigne(laDefense, esplanadeDeLaDefense, false, "ligne1");
		TronconLigne l1_a2 = new TronconLigne(esplanadeDeLaDefense, laDefense, false, "ligne1");
		TronconLigne l1_b1 = new TronconLigne(esplanadeDeLaDefense, pontdeNeuilly, false, "ligne1");
		TronconLigne l1_b2 = new TronconLigne(pontdeNeuilly, esplanadeDeLaDefense, false, "ligne1");
		TronconLigne l1_c1 = new TronconLigne(pontdeNeuilly, lesSablons, false, "ligne1");
		TronconLigne l1_c2 = new TronconLigne(lesSablons, pontdeNeuilly, false, "ligne1");
		TronconLigne l1_d1 = new TronconLigne(lesSablons, porteMaillot, false, "ligne1");
		TronconLigne l1_d2 = new TronconLigne(porteMaillot, lesSablons, false, "ligne1");
		TronconLigne l1_e1 = new TronconLigne(porteMaillot, argentine, false, "ligne1");
		TronconLigne l1_e2 = new TronconLigne(argentine, porteMaillot, false, "ligne1");
		TronconLigne l1_f1 = new TronconLigne(argentine, charlesDeGaulleEtoile, false, "ligne1");
		TronconLigne l1_f2 = new TronconLigne(charlesDeGaulleEtoile, argentine, false, "ligne1");
		TronconLigne l1_g1 = new TronconLigne(charlesDeGaulleEtoile, georgeV, false, "ligne1");
		TronconLigne l1_g2 = new TronconLigne(georgeV, charlesDeGaulleEtoile, false, "ligne1");
		TronconLigne l1_h1 = new TronconLigne(georgeV, franklinDRoosevelt, false, "ligne1");
		TronconLigne l1_h2 = new TronconLigne(franklinDRoosevelt, georgeV, false, "ligne1");
		TronconLigne l1_i1 = new TronconLigne(franklinDRoosevelt, champsElyseesClemenceau, false, "ligne1");
		TronconLigne l1_i2 = new TronconLigne(champsElyseesClemenceau, franklinDRoosevelt, false, "ligne1");
		TronconLigne l1_j1 = new TronconLigne(champsElyseesClemenceau, concorde, false, "ligne1");
		TronconLigne l1_j2 = new TronconLigne(concorde, champsElyseesClemenceau, false, "ligne1");
		TronconLigne l1_k1 = new TronconLigne(concorde, tuileries, false, "ligne1");
		TronconLigne l1_k2 = new TronconLigne(tuileries, concorde, false, "ligne1");
		TronconLigne l1_l1 = new TronconLigne(tuileries, palaisRoyalMuseeDuLouvre, false, "ligne1");
		TronconLigne l1_l2 = new TronconLigne(palaisRoyalMuseeDuLouvre, tuileries, false, "ligne1");
		TronconLigne l1_m1 = new TronconLigne(palaisRoyalMuseeDuLouvre, louvreRivoli, false, "ligne1");
		TronconLigne l1_m2 = new TronconLigne(louvreRivoli, palaisRoyalMuseeDuLouvre, false, "ligne1");
		TronconLigne l1_n1 = new TronconLigne(louvreRivoli, chatelet, false, "ligne1");
		TronconLigne l1_n2 = new TronconLigne(chatelet, louvreRivoli, false, "ligne1");
		TronconLigne l1_o1 = new TronconLigne(chatelet, hotelDeVille, false, "ligne1");
		TronconLigne l1_o2 = new TronconLigne(hotelDeVille, chatelet, false, "ligne1");
		TronconLigne l1_p1 = new TronconLigne(hotelDeVille, saintPaul, false, "ligne1");
		TronconLigne l1_p2 = new TronconLigne(saintPaul, hotelDeVille, false, "ligne1");
		TronconLigne l1_q1 = new TronconLigne(saintPaul, bastille, false, "ligne1");
		TronconLigne l1_q2 = new TronconLigne(bastille, saintPaul, false, "ligne1");
		TronconLigne l1_r1 = new TronconLigne(bastille, gareDeLyon, false, "ligne1");
		TronconLigne l1_r2 = new TronconLigne(gareDeLyon, bastille, false, "ligne1");
		TronconLigne l1_s1 = new TronconLigne(gareDeLyon, reuillyDiderot, false, "ligne1");
		TronconLigne l1_s2 = new TronconLigne(reuillyDiderot, gareDeLyon, false, "ligne1");
		TronconLigne l1_t1 = new TronconLigne(reuillyDiderot, nation, false, "ligne1");
		TronconLigne l1_t2 = new TronconLigne(nation, reuillyDiderot, false, "ligne1");
		TronconLigne l1_u1 = new TronconLigne(nation, porteDeVincennes, false, "ligne1");
		TronconLigne l1_u2 = new TronconLigne(porteDeVincennes, nation, false, "ligne1");
		TronconLigne l1_v1 = new TronconLigne(porteDeVincennes, saintMande, false, "ligne1");
		TronconLigne l1_v2 = new TronconLigne(saintMande, porteDeVincennes, false, "ligne1");
		TronconLigne l1_w1 = new TronconLigne(saintMande, berault, false, "ligne1");
		TronconLigne l1_w2 = new TronconLigne(berault, saintMande, false, "ligne1");
		TronconLigne l1_x1 = new TronconLigne(berault, chateauDeVincennes, false, "ligne1");
		TronconLigne l1_x2 = new TronconLigne(chateauDeVincennes, berault, false, "ligne1");

		// Ajout des troncons de ligne dans le graphe
		reseau.ajoutTroncon(l1_a1, 1);
		reseau.ajoutTroncon(l1_a2, 2);
		reseau.ajoutTroncon(l1_b1, 3);
		reseau.ajoutTroncon(l1_b2, 4);
		reseau.ajoutTroncon(l1_c1, 1);
		reseau.ajoutTroncon(l1_c2, 2);
		reseau.ajoutTroncon(l1_d1, 3);
		reseau.ajoutTroncon(l1_d2, 4);
		reseau.ajoutTroncon(l1_e1, 1);
		reseau.ajoutTroncon(l1_e2, 2);
		reseau.ajoutTroncon(l1_f1, 3);
		reseau.ajoutTroncon(l1_f2, 4);
		reseau.ajoutTroncon(l1_g1, 1);
		reseau.ajoutTroncon(l1_g2, 2);
		reseau.ajoutTroncon(l1_h1, 3);
		reseau.ajoutTroncon(l1_h2, 4);
		reseau.ajoutTroncon(l1_i1, 1);
		reseau.ajoutTroncon(l1_i2, 2);
		reseau.ajoutTroncon(l1_j1, 3);
		reseau.ajoutTroncon(l1_j2, 4);
		reseau.ajoutTroncon(l1_k1, 1);
		reseau.ajoutTroncon(l1_k2, 2);
		reseau.ajoutTroncon(l1_l1, 3);
		reseau.ajoutTroncon(l1_l2, 4);
		reseau.ajoutTroncon(l1_m1, 1);
		reseau.ajoutTroncon(l1_m2, 2);
		reseau.ajoutTroncon(l1_n1, 3);
		reseau.ajoutTroncon(l1_n2, 4);
		reseau.ajoutTroncon(l1_o1, 1);
		reseau.ajoutTroncon(l1_o2, 2);
		reseau.ajoutTroncon(l1_p1, 3);
		reseau.ajoutTroncon(l1_p2, 4);
		reseau.ajoutTroncon(l1_q1, 1);
		reseau.ajoutTroncon(l1_q2, 2);
		reseau.ajoutTroncon(l1_r1, 3);
		reseau.ajoutTroncon(l1_r2, 4);
		reseau.ajoutTroncon(l1_s1, 1);
		reseau.ajoutTroncon(l1_s2, 2);
		reseau.ajoutTroncon(l1_t1, 3);
		reseau.ajoutTroncon(l1_t2, 4);
		reseau.ajoutTroncon(l1_u1, 1);
		reseau.ajoutTroncon(l1_u2, 2);
		reseau.ajoutTroncon(l1_v1, 3);
		reseau.ajoutTroncon(l1_v2, 4);
		reseau.ajoutTroncon(l1_w1, 1);
		reseau.ajoutTroncon(l1_w2, 2);
		reseau.ajoutTroncon(l1_x1, 3);
		reseau.ajoutTroncon(l1_x2, 4);

		reseau.ajoutLigne("ligne14");
		// ligne14
		// Creation des stations
		Station saintLazare = new Station("SAINT-LAZARE", false, 1, new Coordonnees(48.8754209778, 2.32669527207));
		Station madeleine = new Station("MADELEINE", false, 1, new Coordonnees(48.8697947152, 2.32461201202));
		Station pyramides = new Station("PYRAMIDES", false, 1, new Coordonnees(48.8669912798, 2.33367187742));
		Station bercy = new Station("BERCY", false, 1, new Coordonnees(48.840542783, 2.37940946312));
		Station courSaintEmilion = new Station("COUR SAINT-EMILION", false, 1,
				new Coordonnees(48.8333137156, 2.38729970438));
		Station bibliothqueFrancoisMitterand = new Station("BIBLIOTHEQUE FRANCOIS MITTERRAND", false, 1,
				new Coordonnees(48.8303876267, 2.37703251238));
		Station olympiades = new Station("OLYMPIADES", false, 1, new Coordonnees(48.827029092, 2.3673515636));

		// Ajout des stations dans le graphe
		reseau.ajoutStation(saintLazare);
		reseau.ajoutStation(madeleine);
		reseau.ajoutStation(pyramides);
		reseau.ajoutStation(bercy);
		reseau.ajoutStation(courSaintEmilion);
		reseau.ajoutStation(bibliothqueFrancoisMitterand);
		reseau.ajoutStation(olympiades);

		// Creation des troncons de lignes
		TronconLigne l14_a1 = new TronconLigne(saintLazare, madeleine, false, "ligne14");
		TronconLigne l14_a2 = new TronconLigne(madeleine, saintLazare, false, "ligne14");
		TronconLigne l14_b1 = new TronconLigne(madeleine, pyramides, false, "ligne14");
		TronconLigne l14_b2 = new TronconLigne(pyramides, madeleine, false, "ligne14");
		TronconLigne l14_c1 = new TronconLigne(pyramides, chatelet, false, "ligne14");
		TronconLigne l14_c2 = new TronconLigne(chatelet, pyramides, false, "ligne14");
		TronconLigne l14_d1 = new TronconLigne(chatelet, gareDeLyon, false, "ligne14");
		TronconLigne l14_d2 = new TronconLigne(gareDeLyon, chatelet, false, "ligne14");
		TronconLigne l14_e1 = new TronconLigne(gareDeLyon, bercy, false, "ligne14");
		TronconLigne l14_e2 = new TronconLigne(bercy, gareDeLyon, false, "ligne14");
		TronconLigne l14_f1 = new TronconLigne(gareDeLyon, bercy, false, "ligne14");
		TronconLigne l14_f2 = new TronconLigne(bercy, gareDeLyon, false, "ligne14");
		TronconLigne l14_g1 = new TronconLigne(bercy, courSaintEmilion, false, "ligne14");
		TronconLigne l14_g2 = new TronconLigne(courSaintEmilion, bercy, false, "ligne14");
		TronconLigne l14_h1 = new TronconLigne(courSaintEmilion, bibliothqueFrancoisMitterand, false, "ligne14");
		TronconLigne l14_h2 = new TronconLigne(bibliothqueFrancoisMitterand, courSaintEmilion, false, "ligne14");
		TronconLigne l14_i1 = new TronconLigne(bibliothqueFrancoisMitterand, olympiades, false, "ligne14");
		TronconLigne l14_i2 = new TronconLigne(olympiades, bibliothqueFrancoisMitterand, false, "ligne14");

		// Ajout des troncons de ligne dans le graphe
		reseau.ajoutTroncon(l14_a1, 2);
		reseau.ajoutTroncon(l14_a2, 3);
		reseau.ajoutTroncon(l14_b1, 4);
		reseau.ajoutTroncon(l14_b2, 1);
		reseau.ajoutTroncon(l14_c1, 2);
		reseau.ajoutTroncon(l14_c2, 3);
		reseau.ajoutTroncon(l14_d1, 4);
		reseau.ajoutTroncon(l14_d2, 1);
		reseau.ajoutTroncon(l14_e1, 2);
		reseau.ajoutTroncon(l14_e2, 3);
		reseau.ajoutTroncon(l14_f1, 4);
		reseau.ajoutTroncon(l14_f2, 1);
		reseau.ajoutTroncon(l14_g1, 2);
		reseau.ajoutTroncon(l14_g2, 3);
		reseau.ajoutTroncon(l14_h1, 4);
		reseau.ajoutTroncon(l14_h2, 1);
		reseau.ajoutTroncon(l14_i1, 2);
		reseau.ajoutTroncon(l14_i2, 3);

		reseau.ajoutLigne("ligne2");
		// ligne2
		// Creation des stations
		Station porteDauphine = new Station("PORTE DAUPHINE", false, 1, new Coordonnees(48.8708037888, 2.27521174633));
		Station victorHugo = new Station("VICTOR HUGO", false, 1, new Coordonnees(48.8063562651, 2.23868613272));
		Station ternes = new Station("TERNES", false, 1, new Coordonnees(48.8782014404, 2.2992387664));
		Station courcelles = new Station("COURCELLES", false, 1, new Coordonnees(48.8793981198, 2.30381625565));
		Station monceau = new Station("MONCEAU", false, 1, new Coordonnees(48.8805948493, 2.30934780449));
		Station villiers = new Station("VILLIERS", false, 1, new Coordonnees(48.8814051004, 2.31614705091));
		Station rome = new Station("ROME", false, 1, new Coordonnees(48.8820708339, 2.32039833931));
		Station placeDeClichy = new Station("PLACE DE CLICHY", false, 1, new Coordonnees(48.882763602, 2.32699356999));
		Station blanche = new Station("BLANCHE", false, 1, new Coordonnees(48.8832313018, 2.3329348921));
		Station pigalle = new Station("PIGALLE", false, 1, new Coordonnees(48.8820270896, 2.33721380175));
		Station anvers = new Station("ANVERS", false, 1, new Coordonnees(48.8828716902, 2.3441635728));
		Station barbesRochechouart = new Station("BARBES-ROCHECHOUART", false, 1,
				new Coordonnees(48.8839317699, 2.3493557086));
		Station laChapelle = new Station("LA CHAPELLE", false, 1, new Coordonnees(48.8847387691, 2.36149799466));
		Station stalingrad = new Station("STALINGRAD", false, 1, new Coordonnees(48.8723760535, 2.4281147908));
		Station jaures = new Station("JAURES", false, 1, new Coordonnees(48.8834060066, 2.37218105282));
		Station colonelFabien = new Station("COLONEL FABIEN", false, 1, new Coordonnees(48.8120908164, 2.2805483798));
		Station belleville = new Station("BELLEVILLE", false, 1, new Coordonnees(48.8723589592, 2.37751431368));
		Station couronnes = new Station("COURONNES", false, 1, new Coordonnees(48.868861951, 2.38019539306));
		Station menilmontant = new Station("MENILMONTANT", false, 1, new Coordonnees(48.8707141755, 2.39879342936));
		Station pereLachaise = new Station("PERE LACHAISE", false, 1, new Coordonnees(48.8631612051, 2.38726040018));
		Station philippeAuguste = new Station("PHILIPPE AUGUSTE", false, 1,
				new Coordonnees(48.8583788415, 2.38973473274));
		Station alexandreDumas = new Station("ALEXANDRE DUMAS", false, 1,
				new Coordonnees(48.8068704634, 2.51233678718));
		Station avron = new Station("AVRON", false, 1, new Coordonnees(48.8500655011, 2.49939528589));

		// Ajout des stations dans le graphe
		reseau.ajoutStation(porteDauphine);
		reseau.ajoutStation(victorHugo);
		reseau.ajoutStation(ternes);
		reseau.ajoutStation(courcelles);
		reseau.ajoutStation(monceau);
		reseau.ajoutStation(villiers);
		reseau.ajoutStation(rome);
		reseau.ajoutStation(placeDeClichy);
		reseau.ajoutStation(blanche);
		reseau.ajoutStation(pigalle);
		reseau.ajoutStation(anvers);
		reseau.ajoutStation(barbesRochechouart);
		reseau.ajoutStation(laChapelle);
		reseau.ajoutStation(stalingrad);
		reseau.ajoutStation(jaures);
		reseau.ajoutStation(colonelFabien);
		reseau.ajoutStation(belleville);
		reseau.ajoutStation(couronnes);
		reseau.ajoutStation(menilmontant);
		reseau.ajoutStation(pereLachaise);
		reseau.ajoutStation(philippeAuguste);
		reseau.ajoutStation(alexandreDumas);
		reseau.ajoutStation(avron);

		// Creation des troncons de lignes
		TronconLigne l2_a1 = new TronconLigne(porteDauphine, victorHugo, false, "ligne2");
		TronconLigne l2_a2 = new TronconLigne(victorHugo, porteDauphine, false, "ligne2");
		TronconLigne l2_b1 = new TronconLigne(victorHugo, charlesDeGaulleEtoile, false, "ligne2");
		TronconLigne l2_b2 = new TronconLigne(charlesDeGaulleEtoile, victorHugo, false, "ligne2");
		TronconLigne l2_c1 = new TronconLigne(charlesDeGaulleEtoile, ternes, false, "ligne2");
		TronconLigne l2_c2 = new TronconLigne(ternes, charlesDeGaulleEtoile, false, "ligne2");
		TronconLigne l2_d1 = new TronconLigne(ternes, courcelles, false, "ligne2");
		TronconLigne l2_d2 = new TronconLigne(courcelles, ternes, false, "ligne2");
		TronconLigne l2_e1 = new TronconLigne(courcelles, monceau, false, "ligne2");
		TronconLigne l2_e2 = new TronconLigne(monceau, courcelles, false, "ligne2");
		TronconLigne l2_f1 = new TronconLigne(monceau, villiers, false, "ligne2");
		TronconLigne l2_f2 = new TronconLigne(villiers, monceau, false, "ligne2");
		TronconLigne l2_g1 = new TronconLigne(villiers, rome, false, "ligne2");
		TronconLigne l2_g2 = new TronconLigne(rome, villiers, false, "ligne2");
		TronconLigne l2_h1 = new TronconLigne(rome, placeDeClichy, false, "ligne2");
		TronconLigne l2_h2 = new TronconLigne(placeDeClichy, rome, false, "ligne2");
		TronconLigne l2_i1 = new TronconLigne(placeDeClichy, blanche, false, "ligne2");
		TronconLigne l2_i2 = new TronconLigne(blanche, placeDeClichy, false, "ligne2");
		TronconLigne l2_j1 = new TronconLigne(blanche, pigalle, false, "ligne2");
		TronconLigne l2_j2 = new TronconLigne(pigalle, blanche, false, "ligne2");
		TronconLigne l2_k1 = new TronconLigne(pigalle, anvers, false, "ligne2");
		TronconLigne l2_k2 = new TronconLigne(anvers, pigalle, false, "ligne2");
		TronconLigne l2_l1 = new TronconLigne(anvers, barbesRochechouart, false, "ligne2");
		TronconLigne l2_l2 = new TronconLigne(barbesRochechouart, anvers, false, "ligne2");
		TronconLigne l2_m1 = new TronconLigne(barbesRochechouart, laChapelle, false, "ligne2");
		TronconLigne l2_m2 = new TronconLigne(laChapelle, barbesRochechouart, false, "ligne2");
		TronconLigne l2_n1 = new TronconLigne(laChapelle, stalingrad, false, "ligne2");
		TronconLigne l2_n2 = new TronconLigne(stalingrad, laChapelle, false, "ligne2");
		TronconLigne l2_o1 = new TronconLigne(stalingrad, jaures, false, "ligne2");
		TronconLigne l2_o2 = new TronconLigne(jaures, stalingrad, false, "ligne2");
		TronconLigne l2_p1 = new TronconLigne(jaures, colonelFabien, false, "ligne2");
		TronconLigne l2_p2 = new TronconLigne(colonelFabien, jaures, false, "ligne2");
		TronconLigne l2_q1 = new TronconLigne(colonelFabien, belleville, false, "ligne2");
		TronconLigne l2_q2 = new TronconLigne(belleville, colonelFabien, false, "ligne2");
		TronconLigne l2_r1 = new TronconLigne(belleville, couronnes, false, "ligne2");
		TronconLigne l2_r2 = new TronconLigne(couronnes, belleville, false, "ligne2");
		TronconLigne l2_s1 = new TronconLigne(couronnes, menilmontant, false, "ligne2");
		TronconLigne l2_s2 = new TronconLigne(menilmontant, couronnes, false, "ligne2");
		TronconLigne l2_t1 = new TronconLigne(menilmontant, pereLachaise, false, "ligne2");
		TronconLigne l2_t2 = new TronconLigne(pereLachaise, menilmontant, false, "ligne2");
		TronconLigne l2_u1 = new TronconLigne(pereLachaise, philippeAuguste, false, "ligne2");
		TronconLigne l2_u2 = new TronconLigne(philippeAuguste, pereLachaise, false, "ligne2");
		TronconLigne l2_v1 = new TronconLigne(philippeAuguste, alexandreDumas, false, "ligne2");
		TronconLigne l2_v2 = new TronconLigne(alexandreDumas, philippeAuguste, false, "ligne2");
		TronconLigne l2_w1 = new TronconLigne(alexandreDumas, avron, false, "ligne2");
		TronconLigne l2_w2 = new TronconLigne(avron, alexandreDumas, false, "ligne2");
		TronconLigne l2_x1 = new TronconLigne(avron, nation, false, "ligne2");
		TronconLigne l2_x2 = new TronconLigne(nation, avron, false, "ligne2");

		// Ajout des troncons de ligne dans le graphe
		reseau.ajoutTroncon(l2_a1, 4);
		reseau.ajoutTroncon(l2_a2, 3);
		reseau.ajoutTroncon(l2_b1, 2);
		reseau.ajoutTroncon(l2_b2, 1);
		reseau.ajoutTroncon(l2_c1, 4);
		reseau.ajoutTroncon(l2_c2, 3);
		reseau.ajoutTroncon(l2_d1, 2);
		reseau.ajoutTroncon(l2_d2, 1);
		reseau.ajoutTroncon(l2_e1, 4);
		reseau.ajoutTroncon(l2_e2, 3);
		reseau.ajoutTroncon(l2_f1, 2);
		reseau.ajoutTroncon(l2_f2, 1);
		reseau.ajoutTroncon(l2_g1, 4);
		reseau.ajoutTroncon(l2_g2, 3);
		reseau.ajoutTroncon(l2_h1, 2);
		reseau.ajoutTroncon(l2_h2, 1);
		reseau.ajoutTroncon(l2_i1, 4);
		reseau.ajoutTroncon(l2_i2, 3);
		reseau.ajoutTroncon(l2_j1, 2);
		reseau.ajoutTroncon(l2_j2, 1);
		reseau.ajoutTroncon(l2_k1, 4);
		reseau.ajoutTroncon(l2_k2, 3);
		reseau.ajoutTroncon(l2_l1, 2);
		reseau.ajoutTroncon(l2_l2, 1);
		reseau.ajoutTroncon(l2_m1, 4);
		reseau.ajoutTroncon(l2_m2, 3);
		reseau.ajoutTroncon(l2_n1, 2);
		reseau.ajoutTroncon(l2_n2, 1);
		reseau.ajoutTroncon(l2_o1, 4);
		reseau.ajoutTroncon(l2_o2, 3);
		reseau.ajoutTroncon(l2_p1, 2);
		reseau.ajoutTroncon(l2_p2, 1);
		reseau.ajoutTroncon(l2_q1, 4);
		reseau.ajoutTroncon(l2_q2, 3);
		reseau.ajoutTroncon(l2_r1, 2);
		reseau.ajoutTroncon(l2_r2, 1);
		reseau.ajoutTroncon(l2_s1, 4);
		reseau.ajoutTroncon(l2_s2, 3);
		reseau.ajoutTroncon(l2_t1, 2);
		reseau.ajoutTroncon(l2_t2, 1);
		reseau.ajoutTroncon(l2_u1, 4);
		reseau.ajoutTroncon(l2_u2, 3);
		reseau.ajoutTroncon(l2_v1, 2);
		reseau.ajoutTroncon(l2_v2, 1);
		reseau.ajoutTroncon(l2_w1, 4);
		reseau.ajoutTroncon(l2_w2, 3);
		reseau.ajoutTroncon(l2_x1, 2);
		reseau.ajoutTroncon(l2_x2, 1);

		reseau.ajoutLigne("ligne11");
		// ligne11
		// Creation des stations

		Station rambuteau = new Station("RAMBUTEAU", false, 1, new Coordonnees(48.8611933956, 2.35328695987));
		Station artsEtMetiers = new Station("ARTS ET METIERS", false, 1, new Coordonnees(48.8658123175, 2.357211674));
		Station republique = new Station("REPUBLIQUE", false, 1, new Coordonnees(48.8692908999, 2.64118063537));
		Station goncourt = new Station("GONCOURT", false, 1, new Coordonnees(48.8698986266, 2.37038726291));
		Station pyrenees = new Station("PYRENEES", false, 1, new Coordonnees(48.8740545016, 2.38573131725));
		Station jourdain = new Station("JOURDAIN", false, 1, new Coordonnees(48.8747722637, 2.38864771213));
		Station placeDesFetes = new Station("PLACE DES FETES", false, 1, new Coordonnees(48.8760461396, 2.39377205486));
		Station telegraphe = new Station("TELEGRAPHE", false, 1, new Coordonnees(48.8755224492, 2.39867651114));
		Station porteDesLilas = new Station("PORTE DES LILAS", false, 1, new Coordonnees(48.8772347167, 2.40652684305));
		Station mairieDesLilas = new Station("MAIRIE DES LILAS", false, 1,
				new Coordonnees(48.8800510618, 2.41566020606));

		// Ajout des stations dans le graphe
		reseau.ajoutStation(rambuteau);
		reseau.ajoutStation(artsEtMetiers);
		reseau.ajoutStation(republique);
		reseau.ajoutStation(goncourt);
		reseau.ajoutStation(pyrenees);
		reseau.ajoutStation(jourdain);
		reseau.ajoutStation(placeDesFetes);
		reseau.ajoutStation(telegraphe);
		reseau.ajoutStation(porteDesLilas);
		reseau.ajoutStation(mairieDesLilas);

		// Creation des troncons de lignes
		TronconLigne l11_a1 = new TronconLigne(chatelet, hotelDeVille, false, "ligne11");
		TronconLigne l11_a2 = new TronconLigne(hotelDeVille, chatelet, false, "ligne11");
		TronconLigne l11_b1 = new TronconLigne(hotelDeVille, rambuteau, false, "ligne11");
		TronconLigne l11_b2 = new TronconLigne(rambuteau, hotelDeVille, false, "ligne11");
		TronconLigne l11_c1 = new TronconLigne(rambuteau, artsEtMetiers, false, "ligne11");
		TronconLigne l11_c2 = new TronconLigne(artsEtMetiers, rambuteau, false, "ligne11");
		TronconLigne l11_d1 = new TronconLigne(artsEtMetiers, republique, false, "ligne11");
		TronconLigne l11_d2 = new TronconLigne(republique, artsEtMetiers, false, "ligne11");
		TronconLigne l11_e1 = new TronconLigne(republique, goncourt, false, "ligne11");
		TronconLigne l11_e2 = new TronconLigne(goncourt, republique, false, "ligne11");
		TronconLigne l11_f1 = new TronconLigne(goncourt, belleville, false, "ligne11");
		TronconLigne l11_f2 = new TronconLigne(belleville, goncourt, false, "ligne11");
		TronconLigne l11_g1 = new TronconLigne(belleville, pyrenees, false, "ligne11");
		TronconLigne l11_g2 = new TronconLigne(pyrenees, belleville, false, "ligne11");
		TronconLigne l11_h1 = new TronconLigne(pyrenees, jourdain, false, "ligne11");
		TronconLigne l11_h2 = new TronconLigne(jourdain, pyrenees, false, "ligne11");
		TronconLigne l11_i1 = new TronconLigne(jourdain, placeDesFetes, false, "ligne11");
		TronconLigne l11_i2 = new TronconLigne(placeDesFetes, jourdain, false, "ligne11");
		TronconLigne l11_j1 = new TronconLigne(placeDesFetes, telegraphe, false, "ligne11");
		TronconLigne l11_j2 = new TronconLigne(telegraphe, placeDesFetes, false, "ligne11");
		TronconLigne l11_k1 = new TronconLigne(telegraphe, porteDesLilas, false, "ligne11");
		TronconLigne l11_k2 = new TronconLigne(porteDesLilas, telegraphe, false, "ligne11");
		TronconLigne l11_l1 = new TronconLigne(porteDesLilas, mairieDesLilas, false, "ligne11");
		TronconLigne l11_l2 = new TronconLigne(mairieDesLilas, porteDesLilas, false, "ligne11");

		// Ajout des troncons de ligne dans le graphe
		reseau.ajoutTroncon(l11_a1, 1);
		reseau.ajoutTroncon(l11_a2, 2);
		reseau.ajoutTroncon(l11_b1, 3);
		reseau.ajoutTroncon(l11_b2, 4);
		reseau.ajoutTroncon(l11_c1, 1);
		reseau.ajoutTroncon(l11_c2, 2);
		reseau.ajoutTroncon(l11_d1, 3);
		reseau.ajoutTroncon(l11_d2, 4);
		reseau.ajoutTroncon(l11_e1, 1);
		reseau.ajoutTroncon(l11_e2, 2);
		reseau.ajoutTroncon(l11_f1, 3);
		reseau.ajoutTroncon(l11_f2, 4);
		reseau.ajoutTroncon(l11_g1, 1);
		reseau.ajoutTroncon(l11_g2, 2);
		reseau.ajoutTroncon(l11_h1, 3);
		reseau.ajoutTroncon(l11_h2, 4);
		reseau.ajoutTroncon(l11_i1, 1);
		reseau.ajoutTroncon(l11_i2, 2);
		reseau.ajoutTroncon(l11_j1, 3);
		reseau.ajoutTroncon(l11_j2, 4);
		reseau.ajoutTroncon(l11_k1, 1);
		reseau.ajoutTroncon(l11_k2, 2);
		reseau.ajoutTroncon(l11_l1, 3);
		reseau.ajoutTroncon(l11_l2, 4);

		// Changement
		reseau.ajouterChangementLigneStation(belleville, "ligne11", "ligne2", 5);
		reseau.ajouterChangementLigneStation(belleville, "ligne2", "ligne11", 5);
		reseau.ajouterChangementLigneStation(chatelet, "ligne1", "ligne11", 5);
		reseau.ajouterChangementLigneStation(chatelet, "ligne11", "ligne1", 5);
		reseau.ajouterChangementLigneStation(chatelet, "ligne14", "ligne11", 5);
		reseau.ajouterChangementLigneStation(chatelet, "ligne11", "ligne14", 5);
		reseau.ajouterChangementLigneStation(chatelet, "ligne14", "ligne1", 5);
		reseau.ajouterChangementLigneStation(chatelet, "ligne1", "ligne14", 5);
		reseau.ajouterChangementLigneStation(nation, "ligne1", "ligne2", 5);
		reseau.ajouterChangementLigneStation(nation, "ligne2", "ligne1", 5);
		reseau.ajouterChangementLigneStation(hotelDeVille, "ligne11", "ligne14", 5);
		reseau.ajouterChangementLigneStation(hotelDeVille, "ligne14", "ligne11", 5);
		reseau.ajouterChangementLigneStation(charlesDeGaulleEtoile, "ligne1", "ligne2", 5);
		reseau.ajouterChangementLigneStation(charlesDeGaulleEtoile, "ligne2", "ligne1", 5);
		reseau.ajouterChangementLigneStation(gareDeLyon, "ligne14", "ligne1", 5);
		reseau.ajouterChangementLigneStation(gareDeLyon, "ligne1", "ligne14", 5);

		// HORAIRES LIGNE 1
		// Dans le 1er sens
		reseau.ajouterHoraires(laDefense.getNom(), esplanadeDeLaDefense.getNom(), "ligne1", new Horaire(5, 0),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(esplanadeDeLaDefense.getNom(), pontdeNeuilly.getNom(), "ligne1", new Horaire(5, 2),
				new Horaire(23, 2), 360);
		reseau.ajouterHoraires(pontdeNeuilly.getNom(), lesSablons.getNom(), "ligne1", new Horaire(5, 6),
				new Horaire(23, 6), 360);
		reseau.ajouterHoraires(lesSablons.getNom(), porteMaillot.getNom(), "ligne1", new Horaire(5, 8),
				new Horaire(23, 8), 360);
		reseau.ajouterHoraires(porteMaillot.getNom(), argentine.getNom(), "ligne1", new Horaire(5, 12),
				new Horaire(23, 12), 360);
		reseau.ajouterHoraires(argentine.getNom(), charlesDeGaulleEtoile.getNom(), "ligne1", new Horaire(5, 14),
				new Horaire(23, 14), 360);
		reseau.ajouterHoraires(charlesDeGaulleEtoile.getNom(), georgeV.getNom(), "ligne1", new Horaire(5, 18),
				new Horaire(23, 18), 360);
		reseau.ajouterHoraires(georgeV.getNom(), franklinDRoosevelt.getNom(), "ligne1", new Horaire(5, 20),
				new Horaire(23, 20), 360);
		reseau.ajouterHoraires(franklinDRoosevelt.getNom(), champsElyseesClemenceau.getNom(), "ligne1",
				new Horaire(5, 24), new Horaire(23, 24), 360);
		reseau.ajouterHoraires(champsElyseesClemenceau.getNom(), concorde.getNom(), "ligne1", new Horaire(5, 26),
				new Horaire(23, 26), 360);
		reseau.ajouterHoraires(concorde.getNom(), tuileries.getNom(), "ligne1", new Horaire(5, 30), new Horaire(23, 30),
				360);
		reseau.ajouterHoraires(tuileries.getNom(), palaisRoyalMuseeDuLouvre.getNom(), "ligne1", new Horaire(5, 32),
				new Horaire(23, 32), 360);
		reseau.ajouterHoraires(palaisRoyalMuseeDuLouvre.getNom(), louvreRivoli.getNom(), "ligne1", new Horaire(5, 36),
				new Horaire(23, 36), 360);
		reseau.ajouterHoraires(louvreRivoli.getNom(), chatelet.getNom(), "ligne1", new Horaire(5, 38),
				new Horaire(23, 38), 360);
		reseau.ajouterHoraires(chatelet.getNom(), hotelDeVille.getNom(), "ligne1", new Horaire(5, 42),
				new Horaire(23, 42), 360);
		reseau.ajouterHoraires(hotelDeVille.getNom(), saintPaul.getNom(), "ligne1", new Horaire(5, 44),
				new Horaire(23, 44), 360);
		reseau.ajouterHoraires(saintPaul.getNom(), bastille.getNom(), "ligne1", new Horaire(5, 48), new Horaire(23, 48),
				360);
		reseau.ajouterHoraires(bastille.getNom(), gareDeLyon.getNom(), "ligne1", new Horaire(5, 50),
				new Horaire(23, 50), 360);
		reseau.ajouterHoraires(gareDeLyon.getNom(), reuillyDiderot.getNom(), "ligne1", new Horaire(5, 54),
				new Horaire(23, 54), 360);
		reseau.ajouterHoraires(reuillyDiderot.getNom(), nation.getNom(), "ligne1", new Horaire(5, 56),
				new Horaire(23, 56), 360);
		reseau.ajouterHoraires(nation.getNom(), porteDeVincennes.getNom(), "ligne1", new Horaire(6, 0),
				new Horaire(23, 56), 360);
		reseau.ajouterHoraires(porteDeVincennes.getNom(), saintMande.getNom(), "ligne1", new Horaire(6, 2),
				new Horaire(23, 56), 360);
		reseau.ajouterHoraires(saintMande.getNom(), chateauDeVincennes.getNom(), "ligne1", new Horaire(6, 6),
				new Horaire(23, 56), 360);

		// Dans le 2nd sens

		reseau.ajouterHoraires(esplanadeDeLaDefense.getNom(), laDefense.getNom(), "ligne1", new Horaire(6, 31),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(pontdeNeuilly.getNom(), esplanadeDeLaDefense.getNom(), "ligne1", new Horaire(6, 26),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(lesSablons.getNom(), pontdeNeuilly.getNom(), "ligne1", new Horaire(6, 23),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(porteMaillot.getNom(), lesSablons.getNom(), "ligne1", new Horaire(6, 18),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(argentine.getNom(), porteMaillot.getNom(), "ligne1", new Horaire(6, 15),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(charlesDeGaulleEtoile.getNom(), argentine.getNom(), "ligne1", new Horaire(6, 10),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(georgeV.getNom(), charlesDeGaulleEtoile.getNom(), "ligne1", new Horaire(6, 7),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(franklinDRoosevelt.getNom(), georgeV.getNom(), "ligne1", new Horaire(6, 2),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(champsElyseesClemenceau.getNom(), franklinDRoosevelt.getNom(), "ligne1",
				new Horaire(5, 59), new Horaire(23, 59), 360);
		reseau.ajouterHoraires(concorde.getNom(), champsElyseesClemenceau.getNom(), "ligne1", new Horaire(5, 56),
				new Horaire(23, 56), 360);
		reseau.ajouterHoraires(tuileries.getNom(), concorde.getNom(), "ligne1", new Horaire(5, 51), new Horaire(23, 51),
				360);
		reseau.ajouterHoraires(palaisRoyalMuseeDuLouvre.getNom(), tuileries.getNom(), "ligne1", new Horaire(5, 48),
				new Horaire(23, 48), 360);
		reseau.ajouterHoraires(louvreRivoli.getNom(), palaisRoyalMuseeDuLouvre.getNom(), "ligne1", new Horaire(5, 43),
				new Horaire(23, 43), 360);
		reseau.ajouterHoraires(chatelet.getNom(), louvreRivoli.getNom(), "ligne1", new Horaire(5, 40),
				new Horaire(23, 40), 360);
		reseau.ajouterHoraires(hotelDeVille.getNom(), chatelet.getNom(), "ligne1", new Horaire(5, 37),
				new Horaire(23, 37), 360);
		reseau.ajouterHoraires(saintPaul.getNom(), hotelDeVille.getNom(), "ligne1", new Horaire(5, 32),
				new Horaire(23, 32), 360);
		reseau.ajouterHoraires(bastille.getNom(), saintPaul.getNom(), "ligne1", new Horaire(5, 29), new Horaire(23, 29),
				360);
		reseau.ajouterHoraires(gareDeLyon.getNom(), bastille.getNom(), "ligne1", new Horaire(5, 24),
				new Horaire(23, 24), 360);
		reseau.ajouterHoraires(reuillyDiderot.getNom(), gareDeLyon.getNom(), "ligne1", new Horaire(5, 21),
				new Horaire(23, 21), 360);
		reseau.ajouterHoraires(nation.getNom(), reuillyDiderot.getNom(), "ligne1", new Horaire(5, 16),
				new Horaire(23, 16), 360);
		reseau.ajouterHoraires(porteDeVincennes.getNom(), nation.getNom(), "ligne1", new Horaire(5, 13),
				new Horaire(23, 13), 360);
		reseau.ajouterHoraires(saintMande.getNom(), porteDeVincennes.getNom(), "ligne1", new Horaire(5, 8),
				new Horaire(23, 8), 360);
		reseau.ajouterHoraires(berault.getNom(), saintMande.getNom(), "ligne1", new Horaire(5, 5), new Horaire(23, 5),
				360);
		reseau.ajouterHoraires(chateauDeVincennes.getNom(), berault.getNom(), "ligne1", new Horaire(5, 0),
				new Horaire(23, 0), 360);

		// HORAIRES Ligne14
		// 1er sens
		reseau.ajouterHoraires(saintLazare.getNom(), madeleine.getNom(), "ligne14", new Horaire(5, 0),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(madeleine.getNom(), pyramides.getNom(), "ligne14", new Horaire(5, 3), new Horaire(23, 3),
				360);
		reseau.ajouterHoraires(pyramides.getNom(), chatelet.getNom(), "ligne14", new Horaire(5, 8), new Horaire(23, 8),
				360);
		reseau.ajouterHoraires(chatelet.getNom(), gareDeLyon.getNom(), "ligne14", new Horaire(5, 12),
				new Horaire(23, 12), 360);
		reseau.ajouterHoraires(gareDeLyon.getNom(), bercy.getNom(), "ligne14", new Horaire(5, 17), new Horaire(23, 17),
				360);
		reseau.ajouterHoraires(bercy.getNom(), courSaintEmilion.getNom(), "ligne14", new Horaire(5, 20),
				new Horaire(23, 20), 360);
		reseau.ajouterHoraires(courSaintEmilion.getNom(), bibliothqueFrancoisMitterand.getNom(), "ligne14",
				new Horaire(5, 25), new Horaire(23, 25), 360);
		reseau.ajouterHoraires(bibliothqueFrancoisMitterand.getNom(), olympiades.getNom(), "ligne14",
				new Horaire(5, 28), new Horaire(23, 28), 360);

		// 2e sens
		reseau.ajouterHoraires(madeleine.getNom(), saintLazare.getNom(), "ligne14", new Horaire(5, 20),
				new Horaire(23, 20), 360);
		reseau.ajouterHoraires(pyramides.getNom(), madeleine.getNom(), "ligne14", new Horaire(5, 18),
				new Horaire(23, 18), 360);
		reseau.ajouterHoraires(chatelet.getNom(), pyramides.getNom(), "ligne14", new Horaire(5, 14),
				new Horaire(23, 14), 360);
		reseau.ajouterHoraires(gareDeLyon.getNom(), chatelet.getNom(), "ligne14", new Horaire(5, 12),
				new Horaire(23, 12), 360);
		reseau.ajouterHoraires(bercy.getNom(), gareDeLyon.getNom(), "ligne14", new Horaire(5, 8), new Horaire(23, 8),
				360);
		reseau.ajouterHoraires(courSaintEmilion.getNom(), bercy.getNom(), "ligne14", new Horaire(5, 6),
				new Horaire(23, 6), 360);
		reseau.ajouterHoraires(bibliothqueFrancoisMitterand.getNom(), courSaintEmilion.getNom(), "ligne14",
				new Horaire(5, 2), new Horaire(23, 2), 360);
		reseau.ajouterHoraires(olympiades.getNom(), bibliothqueFrancoisMitterand.getNom(), "ligne14", new Horaire(5, 0),
				new Horaire(23, 0), 360);

		// Horaire Ligne11
		// 1er sens
		reseau.ajouterHoraires(chatelet.getNom(), hotelDeVille.getNom(), "ligne11", new Horaire(5, 0),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(hotelDeVille.getNom(), rambuteau.getNom(), "ligne11", new Horaire(5, 2),
				new Horaire(23, 2), 360);
		reseau.ajouterHoraires(rambuteau.getNom(), artsEtMetiers.getNom(), "ligne11", new Horaire(5, 6),
				new Horaire(23, 6), 360);
		reseau.ajouterHoraires(artsEtMetiers.getNom(), republique.getNom(), "ligne11", new Horaire(5, 8),
				new Horaire(23, 8), 360);
		reseau.ajouterHoraires(republique.getNom(), goncourt.getNom(), "ligne11", new Horaire(5, 12),
				new Horaire(23, 12), 360);
		reseau.ajouterHoraires(goncourt.getNom(), belleville.getNom(), "ligne11", new Horaire(5, 14),
				new Horaire(23, 14), 360);
		reseau.ajouterHoraires(belleville.getNom(), pyrenees.getNom(), "ligne11", new Horaire(5, 18),
				new Horaire(23, 18), 360);
		reseau.ajouterHoraires(pyrenees.getNom(), jourdain.getNom(), "ligne11", new Horaire(5, 20), new Horaire(23, 20),
				360);
		reseau.ajouterHoraires(jourdain.getNom(), placeDesFetes.getNom(), "ligne11", new Horaire(5, 24),
				new Horaire(23, 24), 360);
		reseau.ajouterHoraires(placeDesFetes.getNom(), telegraphe.getNom(), "ligne11", new Horaire(5, 26),
				new Horaire(23, 26), 360);
		reseau.ajouterHoraires(telegraphe.getNom(), porteDesLilas.getNom(), "ligne11", new Horaire(5, 30),
				new Horaire(23, 30), 360);
		reseau.ajouterHoraires(porteDesLilas.getNom(), mairieDesLilas.getNom(), "ligne11", new Horaire(5, 32),
				new Horaire(23, 32), 360);

		// 2nd sens

		reseau.ajouterHoraires(hotelDeVille.getNom(), chatelet.getNom(), "ligne11", new Horaire(5, 0),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(rambuteau.getNom(), hotelDeVille.getNom(), "ligne11", new Horaire(5, 3),
				new Horaire(23, 3), 360);
		reseau.ajouterHoraires(artsEtMetiers.getNom(), rambuteau.getNom(), "ligne11", new Horaire(5, 8),
				new Horaire(23, 8), 360);
		reseau.ajouterHoraires(republique.getNom(), artsEtMetiers.getNom(), "ligne11", new Horaire(5, 11),
				new Horaire(23, 11), 360);
		reseau.ajouterHoraires(goncourt.getNom(), republique.getNom(), "ligne11", new Horaire(5, 16),
				new Horaire(23, 16), 360);
		reseau.ajouterHoraires(belleville.getNom(), goncourt.getNom(), "ligne11", new Horaire(5, 19),
				new Horaire(23, 19), 360);
		reseau.ajouterHoraires(pyrenees.getNom(), belleville.getNom(), "ligne11", new Horaire(5, 24),
				new Horaire(23, 24), 360);
		reseau.ajouterHoraires(jourdain.getNom(), pyrenees.getNom(), "ligne11", new Horaire(5, 27), new Horaire(23, 27),
				360);
		reseau.ajouterHoraires(placeDesFetes.getNom(), jourdain.getNom(), "ligne11", new Horaire(5, 32),
				new Horaire(23, 32), 360);
		reseau.ajouterHoraires(telegraphe.getNom(), placeDesFetes.getNom(), "ligne11", new Horaire(5, 37),
				new Horaire(23, 37), 360);
		reseau.ajouterHoraires(porteDesLilas.getNom(), telegraphe.getNom(), "ligne11", new Horaire(5, 40),
				new Horaire(23, 40), 360);
		reseau.ajouterHoraires(mairieDesLilas.getNom(), porteDesLilas.getNom(), "ligne11", new Horaire(5, 45),
				new Horaire(23, 45), 360);

		// Horaire Ligne2
		// 1er sens
		reseau.ajouterHoraires(porteDauphine.getNom(), victorHugo.getNom(), "ligne2", new Horaire(5, 0),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(victorHugo.getNom(), charlesDeGaulleEtoile.getNom(), "ligne2", new Horaire(5, 5),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(charlesDeGaulleEtoile.getNom(), ternes.getNom(), "ligne2", new Horaire(5, 8),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(ternes.getNom(), courcelles.getNom(), "ligne2", new Horaire(5, 13), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(courcelles.getNom(), monceau.getNom(), "ligne2", new Horaire(5, 16), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(monceau.getNom(), villiers.getNom(), "ligne2", new Horaire(5, 21), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(villiers.getNom(), rome.getNom(), "ligne2", new Horaire(5, 24), new Horaire(23, 0), 360);
		reseau.ajouterHoraires(rome.getNom(), placeDeClichy.getNom(), "ligne2", new Horaire(5, 29), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(placeDeClichy.getNom(), blanche.getNom(), "ligne2", new Horaire(5, 32),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(blanche.getNom(), pigalle.getNom(), "ligne2", new Horaire(5, 37), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(pigalle.getNom(), anvers.getNom(), "ligne2", new Horaire(5, 40), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(anvers.getNom(), barbesRochechouart.getNom(), "ligne2", new Horaire(5, 45),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(barbesRochechouart.getNom(), laChapelle.getNom(), "ligne2", new Horaire(5, 48),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(laChapelle.getNom(), stalingrad.getNom(), "ligne2", new Horaire(5, 53),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(stalingrad.getNom(), jaures.getNom(), "ligne2", new Horaire(5, 56), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(jaures.getNom(), colonelFabien.getNom(), "ligne2", new Horaire(6, 1), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(colonelFabien.getNom(), belleville.getNom(), "ligne2", new Horaire(6, 6),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(belleville.getNom(), couronnes.getNom(), "ligne2", new Horaire(6, 9), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(couronnes.getNom(), menilmontant.getNom(), "ligne2", new Horaire(6, 14),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(menilmontant.getNom(), pereLachaise.getNom(), "ligne2", new Horaire(6, 17),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(pereLachaise.getNom(), philippeAuguste.getNom(), "ligne2", new Horaire(6, 22),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(philippeAuguste.getNom(), alexandreDumas.getNom(), "ligne2", new Horaire(6, 25),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(alexandreDumas.getNom(), avron.getNom(), "ligne2", new Horaire(6, 30),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(avron.getNom(), nation.getNom(), "ligne2", new Horaire(6, 33), new Horaire(23, 0), 360);

		// 2e sens
		reseau.ajouterHoraires(nation.getNom(), avron.getNom(), "ligne2", new Horaire(6, 12), new Horaire(23, 0), 360);
		reseau.ajouterHoraires(avron.getNom(), alexandreDumas.getNom(), "ligne2", new Horaire(6, 8), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(alexandreDumas.getNom(), philippeAuguste.getNom(), "ligne2", new Horaire(6, 6),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(philippeAuguste.getNom(), pereLachaise.getNom(), "ligne2", new Horaire(6, 2),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(pereLachaise.getNom(), menilmontant.getNom(), "ligne2", new Horaire(5, 58),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(menilmontant.getNom(), couronnes.getNom(), "ligne2", new Horaire(5, 56),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(couronnes.getNom(), belleville.getNom(), "ligne2", new Horaire(5, 52),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(belleville.getNom(), colonelFabien.getNom(), "ligne2", new Horaire(5, 48),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(colonelFabien.getNom(), jaures.getNom(), "ligne2", new Horaire(5, 46),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(jaures.getNom(), stalingrad.getNom(), "ligne2", new Horaire(5, 42), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(stalingrad.getNom(), laChapelle.getNom(), "ligne2", new Horaire(5, 40),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(laChapelle.getNom(), barbesRochechouart.getNom(), "ligne2", new Horaire(5, 36),
				new Horaire(23, 29), 360);
		reseau.ajouterHoraires(barbesRochechouart.getNom(), anvers.getNom(), "ligne2", new Horaire(5, 32),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(anvers.getNom(), pigalle.getNom(), "ligne2", new Horaire(5, 30), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(pigalle.getNom(), blanche.getNom(), "ligne2", new Horaire(5, 26), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(blanche.getNom(), placeDeClichy.getNom(), "ligne2", new Horaire(5, 24),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(placeDeClichy.getNom(), rome.getNom(), "ligne2", new Horaire(5, 20), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(rome.getNom(), villiers.getNom(), "ligne2", new Horaire(5, 18), new Horaire(23, 0), 360);
		reseau.ajouterHoraires(villiers.getNom(), monceau.getNom(), "ligne2", new Horaire(5, 16), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(monceau.getNom(), courcelles.getNom(), "ligne2", new Horaire(5, 12), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(courcelles.getNom(), ternes.getNom(), "ligne2", new Horaire(5, 10), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(ternes.getNom(), charlesDeGaulleEtoile.getNom(), "ligne2", new Horaire(5, 6),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(charlesDeGaulleEtoile.getNom(), victorHugo.getNom(), "ligne2", new Horaire(5, 4),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(victorHugo.getNom(), porteDauphine.getNom(), "ligne2", new Horaire(5, 0),
				new Horaire(23, 0), 360);

		return reseau;
	}

	private Reseau genererUneLigneAvecIncident() throws StationDejaExistante, SuccesseurNoeudDejaExistant {
		Reseau reseau = new Reseau();

		reseau.ajoutLigne("ligne1");
		// Ligne 1
		// Creation des stations
		Station laDefense = new Station("LA DEFENSE", false, 1, new Coordonnees(48.8918614341, 2.23647911585));
		Station esplanadeDeLaDefense = new Station("ESPLANADE DE LA DEFENSE", false, 1,
				new Coordonnees(48.8883580602, 2.24993721286));
		Station pontdeNeuilly = new Station("PONT DE NEUILLY", false, 1, new Coordonnees(48.8855061094, 2.2585274702));
		Station lesSablons = new Station("LES SABLONS", true, 1, new Coordonnees(48.8635862714, 2.46708540814));
		Station porteMaillot = new Station("PORTE MAILLOT", false, 1, new Coordonnees(48.8780061763, 2.28246564));
		Station argentine = new Station("ARGENTINE", false, 1, new Coordonnees(48.875672486, 2.28944416448));
		Station charlesDeGaulleEtoile = new Station("CHARLES DE GAULLE - ETOILE", false, 1,
				new Coordonnees(48.8744527757, 2.2963258095));
		Station georgeV = new Station("GEORGE V", false, 1, new Coordonnees(48.8720456194, 2.30076918564));
		Station franklinDRoosevelt = new Station("FRANKLIN ROOSVELT", false, 1,
				new Coordonnees(48.8688126319, 2.30992632447));
		Station champsElyseesClemenceau = new Station("CHAMPS-ELYSEES-CLEMENCEAU", false, 1,
				new Coordonnees(48.8678427062, 2.31320999172));
		Station concorde = new Station("CONCORDE", true, 1, new Coordonnees(48.8654893909, 2.32141178921));
		Station tuileries = new Station("TUILERIES", false, 1, new Coordonnees(48.8647801625, 2.32909495443));
		Station palaisRoyalMuseeDuLouvre = new Station("PALAIS ROYAL - MUSEE DU LOUVRE", false, 1,
				new Coordonnees(48.8625785271, 2.3360287312));
		Station louvreRivoli = new Station("LOUVRE - RIVOLI", false, 1, new Coordonnees(48.8656520589, 2.34338479681));
		Station chatelet = new Station("CHATELET", false, 1, new Coordonnees(48.8583899635, 2.34748372999));
		Station hotelDeVille = new Station("HOTEL DE VILLE", false, 1, new Coordonnees(48.8560619441, 2.35030260237));
		Station saintPaul = new Station("SAINT-PAUL", false, 1, new Coordonnees(48.8549637098, 2.36156579418));
		Station bastille = new Station("BASTILLE", false, 1, new Coordonnees(48.8529756747, 2.36921882444));
		Station gareDeLyon = new Station("GARE DE LYON", false, 1, new Coordonnees(48.8436904326, 2.37328450012));
		Station reuillyDiderot = new Station("REUILLY - DIDEROT", false, 1,
				new Coordonnees(48.8475229557, 2.38749044347));
		Station nation = new Station("NATION", true, 1, new Coordonnees(48.8491278557, 2.39593524577));
		Station porteDeVincennes = new Station("PORTE DE VINCENNES", false, 1,
				new Coordonnees(48.8470078912, 2.41023131382));
		Station saintMande = new Station("SAINT-MANDE", false, 1, new Coordonnees(48.8462123404, 2.41746102327));
		Station berault = new Station("BERAULT", false, 1, new Coordonnees(48.8456929219, 2.42742804347));
		Station chateauDeVincennes = new Station("CHATEAU DE VINCENNES", false, 1,
				new Coordonnees(48.844089776, 2.44248546714));

		// Ajout des stations dans le graphe
		reseau.ajoutStation(laDefense);
		reseau.ajoutStation(esplanadeDeLaDefense);
		reseau.ajoutStation(pontdeNeuilly);
		reseau.ajoutStation(lesSablons);
		reseau.ajoutStation(porteMaillot);
		reseau.ajoutStation(argentine);
		reseau.ajoutStation(charlesDeGaulleEtoile);
		reseau.ajoutStation(georgeV);
		reseau.ajoutStation(franklinDRoosevelt);
		reseau.ajoutStation(champsElyseesClemenceau);
		reseau.ajoutStation(concorde);
		reseau.ajoutStation(tuileries);
		reseau.ajoutStation(palaisRoyalMuseeDuLouvre);
		reseau.ajoutStation(louvreRivoli);
		reseau.ajoutStation(chatelet);
		reseau.ajoutStation(hotelDeVille);
		reseau.ajoutStation(saintPaul);
		reseau.ajoutStation(bastille);
		reseau.ajoutStation(gareDeLyon);
		reseau.ajoutStation(reuillyDiderot);
		reseau.ajoutStation(nation);
		reseau.ajoutStation(porteDeVincennes);
		reseau.ajoutStation(saintMande);
		reseau.ajoutStation(berault);
		reseau.ajoutStation(chateauDeVincennes);

		// Creation des troncons de lignes
		TronconLigne l1_a1 = new TronconLigne(laDefense, esplanadeDeLaDefense, false, "ligne1");
		TronconLigne l1_a2 = new TronconLigne(esplanadeDeLaDefense, laDefense, false, "ligne1");
		TronconLigne l1_b1 = new TronconLigne(esplanadeDeLaDefense, pontdeNeuilly, false, "ligne1");
		TronconLigne l1_b2 = new TronconLigne(pontdeNeuilly, esplanadeDeLaDefense, false, "ligne1");
		TronconLigne l1_c1 = new TronconLigne(pontdeNeuilly, lesSablons, false, "ligne1");
		TronconLigne l1_c2 = new TronconLigne(lesSablons, pontdeNeuilly, false, "ligne1");
		TronconLigne l1_d1 = new TronconLigne(lesSablons, porteMaillot, false, "ligne1");
		TronconLigne l1_d2 = new TronconLigne(porteMaillot, lesSablons, false, "ligne1");
		TronconLigne l1_e1 = new TronconLigne(porteMaillot, argentine, false, "ligne1");
		TronconLigne l1_e2 = new TronconLigne(argentine, porteMaillot, false, "ligne1");
		TronconLigne l1_f1 = new TronconLigne(argentine, charlesDeGaulleEtoile, false, "ligne1");
		TronconLigne l1_f2 = new TronconLigne(charlesDeGaulleEtoile, argentine, false, "ligne1");
		TronconLigne l1_g1 = new TronconLigne(charlesDeGaulleEtoile, georgeV, false, "ligne1");
		TronconLigne l1_g2 = new TronconLigne(georgeV, charlesDeGaulleEtoile, false, "ligne1");
		TronconLigne l1_h1 = new TronconLigne(georgeV, franklinDRoosevelt, false, "ligne1");
		TronconLigne l1_h2 = new TronconLigne(franklinDRoosevelt, georgeV, false, "ligne1");
		TronconLigne l1_i1 = new TronconLigne(franklinDRoosevelt, champsElyseesClemenceau, false, "ligne1");
		TronconLigne l1_i2 = new TronconLigne(champsElyseesClemenceau, franklinDRoosevelt, false, "ligne1");
		TronconLigne l1_j1 = new TronconLigne(champsElyseesClemenceau, concorde, false, "ligne1");
		TronconLigne l1_j2 = new TronconLigne(concorde, champsElyseesClemenceau, false, "ligne1");
		TronconLigne l1_k1 = new TronconLigne(concorde, tuileries, false, "ligne1");
		TronconLigne l1_k2 = new TronconLigne(tuileries, concorde, false, "ligne1");
		TronconLigne l1_l1 = new TronconLigne(tuileries, palaisRoyalMuseeDuLouvre, false, "ligne1");
		TronconLigne l1_l2 = new TronconLigne(palaisRoyalMuseeDuLouvre, tuileries, false, "ligne1");
		TronconLigne l1_m1 = new TronconLigne(palaisRoyalMuseeDuLouvre, louvreRivoli, false, "ligne1");
		TronconLigne l1_m2 = new TronconLigne(louvreRivoli, palaisRoyalMuseeDuLouvre, false, "ligne1");
		TronconLigne l1_n1 = new TronconLigne(louvreRivoli, chatelet, false, "ligne1");
		TronconLigne l1_n2 = new TronconLigne(chatelet, louvreRivoli, false, "ligne1");
		TronconLigne l1_o1 = new TronconLigne(chatelet, hotelDeVille, false, "ligne1");
		TronconLigne l1_o2 = new TronconLigne(hotelDeVille, chatelet, false, "ligne1");
		TronconLigne l1_p1 = new TronconLigne(hotelDeVille, saintPaul, false, "ligne1");
		TronconLigne l1_p2 = new TronconLigne(saintPaul, hotelDeVille, false, "ligne1");
		TronconLigne l1_q1 = new TronconLigne(saintPaul, bastille, true, "ligne1");
		TronconLigne l1_q2 = new TronconLigne(bastille, saintPaul, true, "ligne1");
		TronconLigne l1_r1 = new TronconLigne(bastille, gareDeLyon, false, "ligne1");
		TronconLigne l1_r2 = new TronconLigne(gareDeLyon, bastille, false, "ligne1");
		TronconLigne l1_s1 = new TronconLigne(gareDeLyon, reuillyDiderot, false, "ligne1");
		TronconLigne l1_s2 = new TronconLigne(reuillyDiderot, gareDeLyon, false, "ligne1");
		TronconLigne l1_t1 = new TronconLigne(reuillyDiderot, nation, false, "ligne1");
		TronconLigne l1_t2 = new TronconLigne(nation, reuillyDiderot, false, "ligne1");
		TronconLigne l1_u1 = new TronconLigne(nation, porteDeVincennes, false, "ligne1");
		TronconLigne l1_u2 = new TronconLigne(porteDeVincennes, nation, false, "ligne1");
		TronconLigne l1_v1 = new TronconLigne(porteDeVincennes, saintMande, false, "ligne1");
		TronconLigne l1_v2 = new TronconLigne(saintMande, porteDeVincennes, false, "ligne1");
		TronconLigne l1_w1 = new TronconLigne(saintMande, berault, false, "ligne1");
		TronconLigne l1_w2 = new TronconLigne(berault, saintMande, false, "ligne1");
		TronconLigne l1_x1 = new TronconLigne(berault, chateauDeVincennes, false, "ligne1");
		TronconLigne l1_x2 = new TronconLigne(chateauDeVincennes, berault, false, "ligne1");

		// Ajout des troncons de ligne dans le graphe
		reseau.ajoutTroncon(l1_a1, 1);
		reseau.ajoutTroncon(l1_a2, 2);
		reseau.ajoutTroncon(l1_b1, 3);
		reseau.ajoutTroncon(l1_b2, 4);
		reseau.ajoutTroncon(l1_c1, 1);
		reseau.ajoutTroncon(l1_c2, 2);
		reseau.ajoutTroncon(l1_d1, 3);
		reseau.ajoutTroncon(l1_d2, 4);
		reseau.ajoutTroncon(l1_e1, 1);
		reseau.ajoutTroncon(l1_e2, 2);
		reseau.ajoutTroncon(l1_f1, 3);
		reseau.ajoutTroncon(l1_f2, 4);
		reseau.ajoutTroncon(l1_g1, 1);
		reseau.ajoutTroncon(l1_g2, 2);
		reseau.ajoutTroncon(l1_h1, 3);
		reseau.ajoutTroncon(l1_h2, 4);
		reseau.ajoutTroncon(l1_i1, 1);
		reseau.ajoutTroncon(l1_i2, 2);
		reseau.ajoutTroncon(l1_j1, 3);
		reseau.ajoutTroncon(l1_j2, 4);
		reseau.ajoutTroncon(l1_k1, 1);
		reseau.ajoutTroncon(l1_k2, 2);
		reseau.ajoutTroncon(l1_l1, 3);
		reseau.ajoutTroncon(l1_l2, 4);
		reseau.ajoutTroncon(l1_m1, 1);
		reseau.ajoutTroncon(l1_m2, 2);
		reseau.ajoutTroncon(l1_n1, 3);
		reseau.ajoutTroncon(l1_n2, 4);
		reseau.ajoutTroncon(l1_o1, 1);
		reseau.ajoutTroncon(l1_o2, 2);
		reseau.ajoutTroncon(l1_p1, 3);
		reseau.ajoutTroncon(l1_p2, 4);
		reseau.ajoutTroncon(l1_q1, 1);
		reseau.ajoutTroncon(l1_q2, 2);
		reseau.ajoutTroncon(l1_r1, 3);
		reseau.ajoutTroncon(l1_r2, 4);
		reseau.ajoutTroncon(l1_s1, 1);
		reseau.ajoutTroncon(l1_s2, 2);
		reseau.ajoutTroncon(l1_t1, 3);
		reseau.ajoutTroncon(l1_t2, 4);
		reseau.ajoutTroncon(l1_u1, 1);
		reseau.ajoutTroncon(l1_u2, 2);
		reseau.ajoutTroncon(l1_v1, 3);
		reseau.ajoutTroncon(l1_v2, 4);
		reseau.ajoutTroncon(l1_w1, 1);
		reseau.ajoutTroncon(l1_w2, 2);
		reseau.ajoutTroncon(l1_x1, 3);
		reseau.ajoutTroncon(l1_x2, 4);

		/*
		 * graphe.ajoutIncidentStation(lesSablons);
		 * graphe.ajoutIncidentStation(concorde); graphe.ajoutIncidentStation(nation);
		 * ajoutIncidentLigne(l1_q1); ajoutIncidentLigne(l1_q2);
		 */

		// HORAIRES LIGNE 1
		// Dans le 1er sens
		reseau.ajouterHoraires(laDefense.getNom(), esplanadeDeLaDefense.getNom(), "ligne1", new Horaire(5, 0),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(esplanadeDeLaDefense.getNom(), pontdeNeuilly.getNom(), "ligne1", new Horaire(5, 2),
				new Horaire(23, 2), 360);
		reseau.ajouterHoraires(pontdeNeuilly.getNom(), lesSablons.getNom(), "ligne1", new Horaire(5, 6),
				new Horaire(23, 6), 360);
		reseau.ajouterHoraires(lesSablons.getNom(), porteMaillot.getNom(), "ligne1", new Horaire(5, 8),
				new Horaire(23, 8), 360);
		reseau.ajouterHoraires(porteMaillot.getNom(), argentine.getNom(), "ligne1", new Horaire(5, 12),
				new Horaire(23, 12), 360);
		reseau.ajouterHoraires(argentine.getNom(), charlesDeGaulleEtoile.getNom(), "ligne1", new Horaire(5, 14),
				new Horaire(23, 14), 360);
		reseau.ajouterHoraires(charlesDeGaulleEtoile.getNom(), georgeV.getNom(), "ligne1", new Horaire(5, 18),
				new Horaire(23, 18), 360);
		reseau.ajouterHoraires(georgeV.getNom(), franklinDRoosevelt.getNom(), "ligne1", new Horaire(5, 20),
				new Horaire(23, 20), 360);
		reseau.ajouterHoraires(franklinDRoosevelt.getNom(), champsElyseesClemenceau.getNom(), "ligne1",
				new Horaire(5, 24), new Horaire(23, 24), 360);
		reseau.ajouterHoraires(champsElyseesClemenceau.getNom(), concorde.getNom(), "ligne1", new Horaire(5, 26),
				new Horaire(23, 26), 360);
		reseau.ajouterHoraires(concorde.getNom(), tuileries.getNom(), "ligne1", new Horaire(5, 30), new Horaire(23, 30),
				360);
		reseau.ajouterHoraires(tuileries.getNom(), palaisRoyalMuseeDuLouvre.getNom(), "ligne1", new Horaire(5, 32),
				new Horaire(23, 32), 360);
		reseau.ajouterHoraires(palaisRoyalMuseeDuLouvre.getNom(), louvreRivoli.getNom(), "ligne1", new Horaire(5, 36),
				new Horaire(23, 36), 360);
		reseau.ajouterHoraires(louvreRivoli.getNom(), chatelet.getNom(), "ligne1", new Horaire(5, 38),
				new Horaire(23, 38), 360);
		reseau.ajouterHoraires(chatelet.getNom(), hotelDeVille.getNom(), "ligne1", new Horaire(5, 42),
				new Horaire(23, 42), 360);
		reseau.ajouterHoraires(hotelDeVille.getNom(), saintPaul.getNom(), "ligne1", new Horaire(5, 44),
				new Horaire(23, 44), 360);
		reseau.ajouterHoraires(saintPaul.getNom(), bastille.getNom(), "ligne1", new Horaire(5, 48), new Horaire(23, 48),
				360);
		reseau.ajouterHoraires(bastille.getNom(), gareDeLyon.getNom(), "ligne1", new Horaire(5, 50),
				new Horaire(23, 50), 360);
		reseau.ajouterHoraires(gareDeLyon.getNom(), reuillyDiderot.getNom(), "ligne1", new Horaire(5, 54),
				new Horaire(23, 54), 360);
		reseau.ajouterHoraires(reuillyDiderot.getNom(), nation.getNom(), "ligne1", new Horaire(5, 56),
				new Horaire(23, 56), 360);
		reseau.ajouterHoraires(nation.getNom(), porteDeVincennes.getNom(), "ligne1", new Horaire(6, 0),
				new Horaire(23, 56), 360);
		reseau.ajouterHoraires(porteDeVincennes.getNom(), saintMande.getNom(), "ligne1", new Horaire(6, 2),
				new Horaire(23, 56), 360);
		reseau.ajouterHoraires(saintMande.getNom(), chateauDeVincennes.getNom(), "ligne1", new Horaire(6, 6),
				new Horaire(23, 56), 360);

		// Dans le 2nd sens

		reseau.ajouterHoraires(esplanadeDeLaDefense.getNom(), laDefense.getNom(), "ligne1", new Horaire(6, 31),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(pontdeNeuilly.getNom(), esplanadeDeLaDefense.getNom(), "ligne1", new Horaire(6, 26),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(lesSablons.getNom(), pontdeNeuilly.getNom(), "ligne1", new Horaire(6, 23),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(porteMaillot.getNom(), lesSablons.getNom(), "ligne1", new Horaire(6, 18),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(argentine.getNom(), porteMaillot.getNom(), "ligne1", new Horaire(6, 15),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(charlesDeGaulleEtoile.getNom(), argentine.getNom(), "ligne1", new Horaire(6, 10),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(georgeV.getNom(), charlesDeGaulleEtoile.getNom(), "ligne1", new Horaire(6, 7),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(franklinDRoosevelt.getNom(), georgeV.getNom(), "ligne1", new Horaire(6, 2),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(champsElyseesClemenceau.getNom(), franklinDRoosevelt.getNom(), "ligne1",
				new Horaire(5, 59), new Horaire(23, 59), 360);
		reseau.ajouterHoraires(concorde.getNom(), champsElyseesClemenceau.getNom(), "ligne1", new Horaire(5, 56),
				new Horaire(23, 56), 360);
		reseau.ajouterHoraires(tuileries.getNom(), concorde.getNom(), "ligne1", new Horaire(5, 51), new Horaire(23, 51),
				360);
		reseau.ajouterHoraires(palaisRoyalMuseeDuLouvre.getNom(), tuileries.getNom(), "ligne1", new Horaire(5, 48),
				new Horaire(23, 48), 360);
		reseau.ajouterHoraires(louvreRivoli.getNom(), palaisRoyalMuseeDuLouvre.getNom(), "ligne1", new Horaire(5, 43),
				new Horaire(23, 43), 360);
		reseau.ajouterHoraires(chatelet.getNom(), louvreRivoli.getNom(), "ligne1", new Horaire(5, 40),
				new Horaire(23, 40), 360);
		reseau.ajouterHoraires(hotelDeVille.getNom(), chatelet.getNom(), "ligne1", new Horaire(5, 37),
				new Horaire(23, 37), 360);
		reseau.ajouterHoraires(saintPaul.getNom(), hotelDeVille.getNom(), "ligne1", new Horaire(5, 32),
				new Horaire(23, 32), 360);
		reseau.ajouterHoraires(bastille.getNom(), saintPaul.getNom(), "ligne1", new Horaire(5, 29), new Horaire(23, 29),
				360);
		reseau.ajouterHoraires(gareDeLyon.getNom(), bastille.getNom(), "ligne1", new Horaire(5, 24),
				new Horaire(23, 24), 360);
		reseau.ajouterHoraires(reuillyDiderot.getNom(), gareDeLyon.getNom(), "ligne1", new Horaire(5, 21),
				new Horaire(23, 21), 360);
		reseau.ajouterHoraires(nation.getNom(), reuillyDiderot.getNom(), "ligne1", new Horaire(5, 16),
				new Horaire(23, 16), 360);
		reseau.ajouterHoraires(porteDeVincennes.getNom(), nation.getNom(), "ligne1", new Horaire(5, 13),
				new Horaire(23, 13), 360);
		reseau.ajouterHoraires(saintMande.getNom(), porteDeVincennes.getNom(), "ligne1", new Horaire(5, 8),
				new Horaire(23, 8), 360);
		reseau.ajouterHoraires(berault.getNom(), saintMande.getNom(), "ligne1", new Horaire(5, 5), new Horaire(23, 5),
				360);
		reseau.ajouterHoraires(chateauDeVincennes.getNom(), berault.getNom(), "ligne1", new Horaire(5, 0),
				new Horaire(23, 0), 360);
		return reseau;
	}

	private Reseau genererMultiLignesAvecIncident() throws StationDejaExistante, SuccesseurNoeudDejaExistant {
		Reseau reseau = new Reseau();

		reseau.ajoutLigne("ligne1");
		// Ligne 1
		// Creation des stations
		Station laDefense = new Station("LA DEFENSE", false, 1, new Coordonnees(48.8918614341, 2.23647911585));
		Station esplanadeDeLaDefense = new Station("ESPLANADE DE LA DEFENSE", false, 1,
				new Coordonnees(48.8883580602, 2.24993721286));
		Station pontdeNeuilly = new Station("PONT DE NEUILLY", false, 1, new Coordonnees(48.8855061094, 2.2585274702));
		Station lesSablons = new Station("LES SABLONS", true, 1, new Coordonnees(48.8635862714, 2.46708540814));
		Station porteMaillot = new Station("PORTE MAILLOT", false, 1, new Coordonnees(48.8780061763, 2.28246564));
		Station argentine = new Station("ARGENTINE", false, 1, new Coordonnees(48.875672486, 2.28944416448));
		Station charlesDeGaulleEtoile = new Station("CHARLES DE GAULLE - ETOILE", false, 1,
				new Coordonnees(48.8744527757, 2.2963258095));
		Station georgeV = new Station("GEORGE V", false, 1, new Coordonnees(48.8720456194, 2.30076918564));
		Station franklinDRoosevelt = new Station("FRANKLIN ROOSVELT", false, 1,
				new Coordonnees(48.8688126319, 2.30992632447));
		Station champsElyseesClemenceau = new Station("CHAMPS-ELYSEES-CLEMENCEAU", false, 1,
				new Coordonnees(48.8678427062, 2.31320999172));
		Station concorde = new Station("CONCORDE", true, 1, new Coordonnees(48.8654893909, 2.32141178921));
		Station tuileries = new Station("TUILERIES", false, 1, new Coordonnees(48.8647801625, 2.32909495443));
		Station palaisRoyalMuseeDuLouvre = new Station("PALAIS ROYAL - MUSEE DU LOUVRE", false, 1,
				new Coordonnees(48.8625785271, 2.3360287312));
		Station louvreRivoli = new Station("LOUVRE - RIVOLI", false, 1, new Coordonnees(48.8656520589, 2.34338479681));
		Station chatelet = new Station("CHATELET", false, 1, new Coordonnees(48.8583899635, 2.34748372999));
		Station hotelDeVille = new Station("HOTEL DE VILLE", false, 1, new Coordonnees(48.8560619441, 2.35030260237));
		Station saintPaul = new Station("SAINT-PAUL", false, 1, new Coordonnees(48.8549637098, 2.36156579418));
		Station bastille = new Station("BASTILLE", false, 1, new Coordonnees(48.8529756747, 2.36921882444));
		Station gareDeLyon = new Station("GARE DE LYON", true, 1, new Coordonnees(48.8436904326, 2.37328450012));
		Station reuillyDiderot = new Station("REUILLY - DIDEROT", false, 1,
				new Coordonnees(48.8475229557, 2.38749044347));
		Station nation = new Station("NATION", true, 1, new Coordonnees(48.8491278557, 2.39593524577));
		Station porteDeVincennes = new Station("PORTE DE VINCENNES", false, 1,
				new Coordonnees(48.8470078912, 2.41023131382));
		Station saintMande = new Station("SAINT-MANDE", false, 1, new Coordonnees(48.8462123404, 2.41746102327));
		Station berault = new Station("BERAULT", false, 1, new Coordonnees(48.8456929219, 2.42742804347));
		Station chateauDeVincennes = new Station("CHATEAU DE VINCENNES", false, 1,
				new Coordonnees(48.844089776, 2.44248546714));

		// Ajout des stations dans le graphe
		reseau.ajoutStation(laDefense);
		reseau.ajoutStation(esplanadeDeLaDefense);
		reseau.ajoutStation(pontdeNeuilly);
		reseau.ajoutStation(lesSablons);
		reseau.ajoutStation(porteMaillot);
		reseau.ajoutStation(argentine);
		reseau.ajoutStation(charlesDeGaulleEtoile);
		reseau.ajoutStation(georgeV);
		reseau.ajoutStation(franklinDRoosevelt);
		reseau.ajoutStation(champsElyseesClemenceau);
		reseau.ajoutStation(concorde);
		reseau.ajoutStation(tuileries);
		reseau.ajoutStation(palaisRoyalMuseeDuLouvre);
		reseau.ajoutStation(louvreRivoli);
		reseau.ajoutStation(chatelet);
		reseau.ajoutStation(hotelDeVille);
		reseau.ajoutStation(saintPaul);
		reseau.ajoutStation(bastille);
		reseau.ajoutStation(gareDeLyon);
		reseau.ajoutStation(reuillyDiderot);
		reseau.ajoutStation(nation);
		reseau.ajoutStation(porteDeVincennes);
		reseau.ajoutStation(saintMande);
		reseau.ajoutStation(berault);
		reseau.ajoutStation(chateauDeVincennes);

		// Creation des troncons de lignes
		TronconLigne l1_a1 = new TronconLigne(laDefense, esplanadeDeLaDefense, false, "ligne1");
		TronconLigne l1_a2 = new TronconLigne(esplanadeDeLaDefense, laDefense, false, "ligne1");
		TronconLigne l1_b1 = new TronconLigne(esplanadeDeLaDefense, pontdeNeuilly, false, "ligne1");
		TronconLigne l1_b2 = new TronconLigne(pontdeNeuilly, esplanadeDeLaDefense, false, "ligne1");
		TronconLigne l1_c1 = new TronconLigne(pontdeNeuilly, lesSablons, false, "ligne1");
		TronconLigne l1_c2 = new TronconLigne(lesSablons, pontdeNeuilly, false, "ligne1");
		TronconLigne l1_d1 = new TronconLigne(lesSablons, porteMaillot, false, "ligne1");
		TronconLigne l1_d2 = new TronconLigne(porteMaillot, lesSablons, false, "ligne1");
		TronconLigne l1_e1 = new TronconLigne(porteMaillot, argentine, false, "ligne1");
		TronconLigne l1_e2 = new TronconLigne(argentine, porteMaillot, false, "ligne1");
		TronconLigne l1_f1 = new TronconLigne(argentine, charlesDeGaulleEtoile, false, "ligne1");
		TronconLigne l1_f2 = new TronconLigne(charlesDeGaulleEtoile, argentine, false, "ligne1");
		TronconLigne l1_g1 = new TronconLigne(charlesDeGaulleEtoile, georgeV, false, "ligne1");
		TronconLigne l1_g2 = new TronconLigne(georgeV, charlesDeGaulleEtoile, false, "ligne1");
		TronconLigne l1_h1 = new TronconLigne(georgeV, franklinDRoosevelt, false, "ligne1");
		TronconLigne l1_h2 = new TronconLigne(franklinDRoosevelt, georgeV, false, "ligne1");
		TronconLigne l1_i1 = new TronconLigne(franklinDRoosevelt, champsElyseesClemenceau, false, "ligne1");
		TronconLigne l1_i2 = new TronconLigne(champsElyseesClemenceau, franklinDRoosevelt, false, "ligne1");
		TronconLigne l1_j1 = new TronconLigne(champsElyseesClemenceau, concorde, false, "ligne1");
		TronconLigne l1_j2 = new TronconLigne(concorde, champsElyseesClemenceau, false, "ligne1");
		TronconLigne l1_k1 = new TronconLigne(concorde, tuileries, false, "ligne1");
		TronconLigne l1_k2 = new TronconLigne(tuileries, concorde, false, "ligne1");
		TronconLigne l1_l1 = new TronconLigne(tuileries, palaisRoyalMuseeDuLouvre, false, "ligne1");
		TronconLigne l1_l2 = new TronconLigne(palaisRoyalMuseeDuLouvre, tuileries, false, "ligne1");
		TronconLigne l1_m1 = new TronconLigne(palaisRoyalMuseeDuLouvre, louvreRivoli, false, "ligne1");
		TronconLigne l1_m2 = new TronconLigne(louvreRivoli, palaisRoyalMuseeDuLouvre, false, "ligne1");
		TronconLigne l1_n1 = new TronconLigne(louvreRivoli, chatelet, false, "ligne1");
		TronconLigne l1_n2 = new TronconLigne(chatelet, louvreRivoli, false, "ligne1");
		TronconLigne l1_o1 = new TronconLigne(chatelet, hotelDeVille, false, "ligne1");
		TronconLigne l1_o2 = new TronconLigne(hotelDeVille, chatelet, false, "ligne1");
		TronconLigne l1_p1 = new TronconLigne(hotelDeVille, saintPaul, false, "ligne1");
		TronconLigne l1_p2 = new TronconLigne(saintPaul, hotelDeVille, false, "ligne1");
		TronconLigne l1_q1 = new TronconLigne(saintPaul, bastille, false, "ligne1");
		TronconLigne l1_q2 = new TronconLigne(bastille, saintPaul, false, "ligne1");
		TronconLigne l1_r1 = new TronconLigne(bastille, gareDeLyon, false, "ligne1");
		TronconLigne l1_r2 = new TronconLigne(gareDeLyon, bastille, false, "ligne1");
		TronconLigne l1_s1 = new TronconLigne(gareDeLyon, reuillyDiderot, false, "ligne1");
		TronconLigne l1_s2 = new TronconLigne(reuillyDiderot, gareDeLyon, false, "ligne1");
		TronconLigne l1_t1 = new TronconLigne(reuillyDiderot, nation, false, "ligne1");
		TronconLigne l1_t2 = new TronconLigne(nation, reuillyDiderot, false, "ligne1");
		TronconLigne l1_u1 = new TronconLigne(nation, porteDeVincennes, false, "ligne1");
		TronconLigne l1_u2 = new TronconLigne(porteDeVincennes, nation, false, "ligne1");
		TronconLigne l1_v1 = new TronconLigne(porteDeVincennes, saintMande, false, "ligne1");
		TronconLigne l1_v2 = new TronconLigne(saintMande, porteDeVincennes, false, "ligne1");
		TronconLigne l1_w1 = new TronconLigne(saintMande, berault, false, "ligne1");
		TronconLigne l1_w2 = new TronconLigne(berault, saintMande, false, "ligne1");
		TronconLigne l1_x1 = new TronconLigne(berault, chateauDeVincennes, false, "ligne1");
		TronconLigne l1_x2 = new TronconLigne(chateauDeVincennes, berault, false, "ligne1");

		// Ajout des troncons de ligne dans le graphe
		reseau.ajoutTroncon(l1_a1, 1);
		reseau.ajoutTroncon(l1_a2, 2);
		reseau.ajoutTroncon(l1_b1, 3);
		reseau.ajoutTroncon(l1_b2, 4);
		reseau.ajoutTroncon(l1_c1, 1);
		reseau.ajoutTroncon(l1_c2, 2);
		reseau.ajoutTroncon(l1_d1, 3);
		reseau.ajoutTroncon(l1_d2, 4);
		reseau.ajoutTroncon(l1_e1, 1);
		reseau.ajoutTroncon(l1_e2, 2);
		reseau.ajoutTroncon(l1_f1, 3);
		reseau.ajoutTroncon(l1_f2, 4);
		reseau.ajoutTroncon(l1_g1, 1);
		reseau.ajoutTroncon(l1_g2, 2);
		reseau.ajoutTroncon(l1_h1, 3);
		reseau.ajoutTroncon(l1_h2, 4);
		reseau.ajoutTroncon(l1_i1, 1);
		reseau.ajoutTroncon(l1_i2, 2);
		reseau.ajoutTroncon(l1_j1, 3);
		reseau.ajoutTroncon(l1_j2, 4);
		reseau.ajoutTroncon(l1_k1, 1);
		reseau.ajoutTroncon(l1_k2, 2);
		reseau.ajoutTroncon(l1_l1, 3);
		reseau.ajoutTroncon(l1_l2, 4);
		reseau.ajoutTroncon(l1_m1, 1);
		reseau.ajoutTroncon(l1_m2, 2);
		reseau.ajoutTroncon(l1_n1, 3);
		reseau.ajoutTroncon(l1_n2, 4);
		reseau.ajoutTroncon(l1_o1, 1);
		reseau.ajoutTroncon(l1_o2, 2);
		reseau.ajoutTroncon(l1_p1, 3);
		reseau.ajoutTroncon(l1_p2, 4);
		reseau.ajoutTroncon(l1_q1, 1);
		reseau.ajoutTroncon(l1_q2, 2);
		reseau.ajoutTroncon(l1_r1, 3);
		reseau.ajoutTroncon(l1_r2, 4);
		reseau.ajoutTroncon(l1_s1, 1);
		reseau.ajoutTroncon(l1_s2, 2);
		reseau.ajoutTroncon(l1_t1, 3);
		reseau.ajoutTroncon(l1_t2, 4);
		reseau.ajoutTroncon(l1_u1, 1);
		reseau.ajoutTroncon(l1_u2, 2);
		reseau.ajoutTroncon(l1_v1, 3);
		reseau.ajoutTroncon(l1_v2, 4);
		reseau.ajoutTroncon(l1_w1, 1);
		reseau.ajoutTroncon(l1_w2, 2);
		reseau.ajoutTroncon(l1_x1, 3);
		reseau.ajoutTroncon(l1_x2, 4);

		reseau.ajoutLigne("ligne14");
		// ligne14
		// Creation des stations
		Station saintLazare = new Station("SAINT-LAZARE", false, 1, new Coordonnees(48.8754209778, 2.32669527207));
		Station madeleine = new Station("MADELEINE", false, 1, new Coordonnees(48.8697947152, 2.32461201202));
		Station pyramides = new Station("PYRAMIDES", false, 1, new Coordonnees(48.8669912798, 2.33367187742));
		Station bercy = new Station("BERCY", false, 1, new Coordonnees(48.840542783, 2.37940946312));
		Station courSaintEmilion = new Station("COUR SAINT-EMILION", false, 1,
				new Coordonnees(48.8333137156, 2.38729970438));
		Station bibliothqueFrancoisMitterand = new Station("BIBLIOTHEQUE FRANCOIS MITTERRAND", false, 1,
				new Coordonnees(48.8303876267, 2.37703251238));
		Station olympiades = new Station("OLYMPIADES", false, 1, new Coordonnees(48.827029092, 2.3673515636));

		// Ajout des stations dans le graphe
		reseau.ajoutStation(saintLazare);
		reseau.ajoutStation(madeleine);
		reseau.ajoutStation(pyramides);
		reseau.ajoutStation(bercy);
		reseau.ajoutStation(courSaintEmilion);
		reseau.ajoutStation(bibliothqueFrancoisMitterand);
		reseau.ajoutStation(olympiades);

		// Creation des troncons de lignes
		TronconLigne l14_a1 = new TronconLigne(saintLazare, madeleine, false, "ligne14");
		TronconLigne l14_a2 = new TronconLigne(madeleine, saintLazare, false, "ligne14");
		TronconLigne l14_b1 = new TronconLigne(madeleine, pyramides, false, "ligne14");
		TronconLigne l14_b2 = new TronconLigne(pyramides, madeleine, false, "ligne14");
		TronconLigne l14_c1 = new TronconLigne(pyramides, chatelet, false, "ligne14");
		TronconLigne l14_c2 = new TronconLigne(chatelet, pyramides, false, "ligne14");
		TronconLigne l14_d1 = new TronconLigne(chatelet, gareDeLyon, false, "ligne14");
		TronconLigne l14_d2 = new TronconLigne(gareDeLyon, chatelet, false, "ligne14");
		TronconLigne l14_e1 = new TronconLigne(gareDeLyon, bercy, false, "ligne14");
		TronconLigne l14_e2 = new TronconLigne(bercy, gareDeLyon, false, "ligne14");
		TronconLigne l14_f1 = new TronconLigne(gareDeLyon, bercy, false, "ligne14");
		TronconLigne l14_f2 = new TronconLigne(bercy, gareDeLyon, false, "ligne14");
		TronconLigne l14_g1 = new TronconLigne(bercy, courSaintEmilion, false, "ligne14");
		TronconLigne l14_g2 = new TronconLigne(courSaintEmilion, bercy, false, "ligne14");
		TronconLigne l14_h1 = new TronconLigne(courSaintEmilion, bibliothqueFrancoisMitterand, false, "ligne14");
		TronconLigne l14_h2 = new TronconLigne(bibliothqueFrancoisMitterand, courSaintEmilion, false, "ligne14");
		TronconLigne l14_i1 = new TronconLigne(bibliothqueFrancoisMitterand, olympiades, false, "ligne14");
		TronconLigne l14_i2 = new TronconLigne(olympiades, bibliothqueFrancoisMitterand, false, "ligne14");

		// Ajout des troncons de ligne dans le graphe
		reseau.ajoutTroncon(l14_a1, 2);
		reseau.ajoutTroncon(l14_a2, 3);
		reseau.ajoutTroncon(l14_b1, 4);
		reseau.ajoutTroncon(l14_b2, 1);
		reseau.ajoutTroncon(l14_c1, 2);
		reseau.ajoutTroncon(l14_c2, 3);
		reseau.ajoutTroncon(l14_d1, 4);
		reseau.ajoutTroncon(l14_d2, 1);
		reseau.ajoutTroncon(l14_e1, 2);
		reseau.ajoutTroncon(l14_e2, 3);
		reseau.ajoutTroncon(l14_f1, 4);
		reseau.ajoutTroncon(l14_f2, 1);
		reseau.ajoutTroncon(l14_g1, 2);
		reseau.ajoutTroncon(l14_g2, 3);
		reseau.ajoutTroncon(l14_h1, 4);
		reseau.ajoutTroncon(l14_h2, 1);
		reseau.ajoutTroncon(l14_i1, 2);
		reseau.ajoutTroncon(l14_i2, 3);

		reseau.ajoutLigne("ligne2");
		// ligne2
		// Creation des stations
		Station porteDauphine = new Station("PORTE DAUPHINE", false, 1, new Coordonnees(48.8708037888, 2.27521174633));
		Station victorHugo = new Station("VICTOR HUGO", false, 1, new Coordonnees(48.8063562651, 2.23868613272));
		Station ternes = new Station("TERNES", false, 1, new Coordonnees(48.8782014404, 2.2992387664));
		Station courcelles = new Station("COURCELLES", false, 1, new Coordonnees(48.8793981198, 2.30381625565));
		Station monceau = new Station("MONCEAU", false, 1, new Coordonnees(48.8805948493, 2.30934780449));
		Station villiers = new Station("VILLIERS", false, 1, new Coordonnees(48.8814051004, 2.31614705091));
		Station rome = new Station("ROME", true, 1, new Coordonnees(48.8820708339, 2.32039833931));
		Station placeDeClichy = new Station("PLACE DE CLICHY", false, 1, new Coordonnees(48.882763602, 2.32699356999));
		Station blanche = new Station("BLANCHE", false, 1, new Coordonnees(48.8832313018, 2.3329348921));
		Station pigalle = new Station("PIGALLE", false, 1, new Coordonnees(48.8820270896, 2.33721380175));
		Station anvers = new Station("ANVERS", false, 1, new Coordonnees(48.8828716902, 2.3441635728));
		Station barbesRochechouart = new Station("BARBES-ROCHECHOUART", false, 1,
				new Coordonnees(48.8839317699, 2.3493557086));
		Station laChapelle = new Station("LA CHAPELLE", false, 1, new Coordonnees(48.8847387691, 2.36149799466));
		Station stalingrad = new Station("STALINGRAD", false, 1, new Coordonnees(48.8723760535, 2.4281147908));
		Station jaures = new Station("JAURES", false, 1, new Coordonnees(48.8834060066, 2.37218105282));
		Station colonelFabien = new Station("COLONEL FABIEN", false, 1, new Coordonnees(48.8120908164, 2.2805483798));
		Station belleville = new Station("BELLEVILLE", false, 1, new Coordonnees(48.8723589592, 2.37751431368));
		Station couronnes = new Station("COURONNES", false, 1, new Coordonnees(48.868861951, 2.38019539306));
		Station menilmontant = new Station("MENILMONTANT", false, 1, new Coordonnees(48.8707141755, 2.39879342936));
		Station pereLachaise = new Station("PERE LACHAISE", false, 1, new Coordonnees(48.8631612051, 2.38726040018));
		Station philippeAuguste = new Station("PHILIPPE AUGUSTE", false, 1,
				new Coordonnees(48.8583788415, 2.38973473274));
		Station alexandreDumas = new Station("ALEXANDRE DUMAS", false, 1,
				new Coordonnees(48.8068704634, 2.51233678718));
		Station avron = new Station("AVRON", false, 1, new Coordonnees(48.8500655011, 2.49939528589));

		// Ajout des stations dans le graphe
		reseau.ajoutStation(porteDauphine);
		reseau.ajoutStation(victorHugo);
		reseau.ajoutStation(ternes);
		reseau.ajoutStation(courcelles);
		reseau.ajoutStation(monceau);
		reseau.ajoutStation(villiers);
		reseau.ajoutStation(rome);
		reseau.ajoutStation(placeDeClichy);
		reseau.ajoutStation(blanche);
		reseau.ajoutStation(pigalle);
		reseau.ajoutStation(anvers);
		reseau.ajoutStation(barbesRochechouart);
		reseau.ajoutStation(laChapelle);
		reseau.ajoutStation(stalingrad);
		reseau.ajoutStation(jaures);
		reseau.ajoutStation(colonelFabien);
		reseau.ajoutStation(belleville);
		reseau.ajoutStation(couronnes);
		reseau.ajoutStation(menilmontant);
		reseau.ajoutStation(pereLachaise);
		reseau.ajoutStation(philippeAuguste);
		reseau.ajoutStation(alexandreDumas);
		reseau.ajoutStation(avron);

		// Creation des troncons de lignes
		TronconLigne l2_a1 = new TronconLigne(porteDauphine, victorHugo, false, "ligne2");
		TronconLigne l2_a2 = new TronconLigne(victorHugo, porteDauphine, false, "ligne2");
		TronconLigne l2_b1 = new TronconLigne(victorHugo, charlesDeGaulleEtoile, false, "ligne2");
		TronconLigne l2_b2 = new TronconLigne(charlesDeGaulleEtoile, victorHugo, false, "ligne2");
		TronconLigne l2_c1 = new TronconLigne(charlesDeGaulleEtoile, ternes, false, "ligne2");
		TronconLigne l2_c2 = new TronconLigne(ternes, charlesDeGaulleEtoile, false, "ligne2");
		TronconLigne l2_d1 = new TronconLigne(ternes, courcelles, false, "ligne2");
		TronconLigne l2_d2 = new TronconLigne(courcelles, ternes, false, "ligne2");
		TronconLigne l2_e1 = new TronconLigne(courcelles, monceau, false, "ligne2");
		TronconLigne l2_e2 = new TronconLigne(monceau, courcelles, false, "ligne2");
		TronconLigne l2_f1 = new TronconLigne(monceau, villiers, false, "ligne2");
		TronconLigne l2_f2 = new TronconLigne(villiers, monceau, false, "ligne2");
		TronconLigne l2_g1 = new TronconLigne(villiers, rome, false, "ligne2");
		TronconLigne l2_g2 = new TronconLigne(rome, villiers, false, "ligne2");
		TronconLigne l2_h1 = new TronconLigne(rome, placeDeClichy, false, "ligne2");
		TronconLigne l2_h2 = new TronconLigne(placeDeClichy, rome, false, "ligne2");
		TronconLigne l2_i1 = new TronconLigne(placeDeClichy, blanche, false, "ligne2");
		TronconLigne l2_i2 = new TronconLigne(blanche, placeDeClichy, false, "ligne2");
		TronconLigne l2_j1 = new TronconLigne(blanche, pigalle, false, "ligne2");
		TronconLigne l2_j2 = new TronconLigne(pigalle, blanche, false, "ligne2");
		TronconLigne l2_k1 = new TronconLigne(pigalle, anvers, false, "ligne2");
		TronconLigne l2_k2 = new TronconLigne(anvers, pigalle, false, "ligne2");
		TronconLigne l2_l1 = new TronconLigne(anvers, barbesRochechouart, false, "ligne2");
		TronconLigne l2_l2 = new TronconLigne(barbesRochechouart, anvers, false, "ligne2");
		TronconLigne l2_m1 = new TronconLigne(barbesRochechouart, laChapelle, false, "ligne2");
		TronconLigne l2_m2 = new TronconLigne(laChapelle, barbesRochechouart, false, "ligne2");
		TronconLigne l2_n1 = new TronconLigne(laChapelle, stalingrad, false, "ligne2");
		TronconLigne l2_n2 = new TronconLigne(stalingrad, laChapelle, false, "ligne2");
		TronconLigne l2_o1 = new TronconLigne(stalingrad, jaures, false, "ligne2");
		TronconLigne l2_o2 = new TronconLigne(jaures, stalingrad, false, "ligne2");
		TronconLigne l2_p1 = new TronconLigne(jaures, colonelFabien, false, "ligne2");
		TronconLigne l2_p2 = new TronconLigne(colonelFabien, jaures, false, "ligne2");
		TronconLigne l2_q1 = new TronconLigne(colonelFabien, belleville, false, "ligne2");
		TronconLigne l2_q2 = new TronconLigne(belleville, colonelFabien, false, "ligne2");
		TronconLigne l2_r1 = new TronconLigne(belleville, couronnes, false, "ligne2");
		TronconLigne l2_r2 = new TronconLigne(couronnes, belleville, false, "ligne2");
		TronconLigne l2_s1 = new TronconLigne(couronnes, menilmontant, false, "ligne2");
		TronconLigne l2_s2 = new TronconLigne(menilmontant, couronnes, false, "ligne2");
		TronconLigne l2_t1 = new TronconLigne(menilmontant, pereLachaise, false, "ligne2");
		TronconLigne l2_t2 = new TronconLigne(pereLachaise, menilmontant, false, "ligne2");
		TronconLigne l2_u1 = new TronconLigne(pereLachaise, philippeAuguste, false, "ligne2");
		TronconLigne l2_u2 = new TronconLigne(philippeAuguste, pereLachaise, false, "ligne2");
		TronconLigne l2_v1 = new TronconLigne(philippeAuguste, alexandreDumas, false, "ligne2");
		TronconLigne l2_v2 = new TronconLigne(alexandreDumas, philippeAuguste, false, "ligne2");
		TronconLigne l2_w1 = new TronconLigne(alexandreDumas, avron, false, "ligne2");
		TronconLigne l2_w2 = new TronconLigne(avron, alexandreDumas, false, "ligne2");
		TronconLigne l2_x1 = new TronconLigne(avron, nation, false, "ligne2");
		TronconLigne l2_x2 = new TronconLigne(nation, avron, false, "ligne2");

		// Ajout des troncons de ligne dans le graphe
		reseau.ajoutTroncon(l2_a1, 4);
		reseau.ajoutTroncon(l2_a2, 3);
		reseau.ajoutTroncon(l2_b1, 2);
		reseau.ajoutTroncon(l2_b2, 1);
		reseau.ajoutTroncon(l2_c1, 4);
		reseau.ajoutTroncon(l2_c2, 3);
		reseau.ajoutTroncon(l2_d1, 2);
		reseau.ajoutTroncon(l2_d2, 1);
		reseau.ajoutTroncon(l2_e1, 4);
		reseau.ajoutTroncon(l2_e2, 3);
		reseau.ajoutTroncon(l2_f1, 2);
		reseau.ajoutTroncon(l2_f2, 1);
		reseau.ajoutTroncon(l2_g1, 4);
		reseau.ajoutTroncon(l2_g2, 3);
		reseau.ajoutTroncon(l2_h1, 2);
		reseau.ajoutTroncon(l2_h2, 1);
		reseau.ajoutTroncon(l2_i1, 4);
		reseau.ajoutTroncon(l2_i2, 3);
		reseau.ajoutTroncon(l2_j1, 2);
		reseau.ajoutTroncon(l2_j2, 1);
		reseau.ajoutTroncon(l2_k1, 4);
		reseau.ajoutTroncon(l2_k2, 3);
		reseau.ajoutTroncon(l2_l1, 2);
		reseau.ajoutTroncon(l2_l2, 1);
		reseau.ajoutTroncon(l2_m1, 4);
		reseau.ajoutTroncon(l2_m2, 3);
		reseau.ajoutTroncon(l2_n1, 2);
		reseau.ajoutTroncon(l2_n2, 1);
		reseau.ajoutTroncon(l2_o1, 4);
		reseau.ajoutTroncon(l2_o2, 3);
		reseau.ajoutTroncon(l2_p1, 2);
		reseau.ajoutTroncon(l2_p2, 1);
		reseau.ajoutTroncon(l2_q1, 4);
		reseau.ajoutTroncon(l2_q2, 3);
		reseau.ajoutTroncon(l2_r1, 2);
		reseau.ajoutTroncon(l2_r2, 1);
		reseau.ajoutTroncon(l2_s1, 4);
		reseau.ajoutTroncon(l2_s2, 3);
		reseau.ajoutTroncon(l2_t1, 2);
		reseau.ajoutTroncon(l2_t2, 1);
		reseau.ajoutTroncon(l2_u1, 4);
		reseau.ajoutTroncon(l2_u2, 3);
		reseau.ajoutTroncon(l2_v1, 2);
		reseau.ajoutTroncon(l2_v2, 1);
		reseau.ajoutTroncon(l2_w1, 4);
		reseau.ajoutTroncon(l2_w2, 3);
		reseau.ajoutTroncon(l2_x1, 2);
		reseau.ajoutTroncon(l2_x2, 1);

		reseau.ajoutLigne("ligne11");
		// ligne11
		// Creation des stations
		Station rambuteau = new Station("RAMBUTEAU", false, 1, new Coordonnees(48.8611933956, 2.35328695987));
		Station artsEtMetiers = new Station("ARTS ET METIERS", false, 1, new Coordonnees(48.8658123175, 2.357211674));
		Station republique = new Station("REPUBLIQUE", false, 1, new Coordonnees(48.8692908999, 2.64118063537));
		Station goncourt = new Station("GONCOURT", false, 1, new Coordonnees(48.8698986266, 2.37038726291));
		Station pyrenees = new Station("PYRENEES", false, 1, new Coordonnees(48.8740545016, 2.38573131725));
		Station jourdain = new Station("JOURDAIN", true, 1, new Coordonnees(48.8747722637, 2.38864771213));
		Station placeDesFetes = new Station("PLACE DES FETES", false, 1, new Coordonnees(48.8760461396, 2.39377205486));
		Station telegraphe = new Station("TELEGRAPHE", true, 1, new Coordonnees(48.8755224492, 2.39867651114));
		Station porteDesLilas = new Station("PORTE DES LILAS", false, 1, new Coordonnees(48.8772347167, 2.40652684305));
		Station mairieDesLilas = new Station("MAIRIE DES LILAS", false, 1,
				new Coordonnees(48.8800510618, 2.41566020606));

		// Ajout des stations dans le graphe
		reseau.ajoutStation(rambuteau);
		reseau.ajoutStation(artsEtMetiers);
		reseau.ajoutStation(republique);
		reseau.ajoutStation(goncourt);
		reseau.ajoutStation(pyrenees);
		reseau.ajoutStation(jourdain);
		reseau.ajoutStation(placeDesFetes);
		reseau.ajoutStation(telegraphe);
		reseau.ajoutStation(porteDesLilas);
		reseau.ajoutStation(mairieDesLilas);

		// Creation des troncons de lignes
		TronconLigne l11_a1 = new TronconLigne(chatelet, hotelDeVille, false, "ligne11");
		TronconLigne l11_a2 = new TronconLigne(hotelDeVille, chatelet, false, "ligne11");
		TronconLigne l11_b1 = new TronconLigne(hotelDeVille, rambuteau, false, "ligne11");
		TronconLigne l11_b2 = new TronconLigne(rambuteau, hotelDeVille, false, "ligne11");
		TronconLigne l11_c1 = new TronconLigne(rambuteau, artsEtMetiers, false, "ligne11");
		TronconLigne l11_c2 = new TronconLigne(artsEtMetiers, rambuteau, false, "ligne11");
		TronconLigne l11_d1 = new TronconLigne(artsEtMetiers, republique, false, "ligne11");
		TronconLigne l11_d2 = new TronconLigne(republique, artsEtMetiers, false, "ligne11");
		TronconLigne l11_e1 = new TronconLigne(republique, goncourt, false, "ligne11");
		TronconLigne l11_e2 = new TronconLigne(goncourt, republique, false, "ligne11");
		TronconLigne l11_f1 = new TronconLigne(goncourt, belleville, false, "ligne11");
		TronconLigne l11_f2 = new TronconLigne(belleville, goncourt, false, "ligne11");
		TronconLigne l11_g1 = new TronconLigne(belleville, pyrenees, false, "ligne11");
		TronconLigne l11_g2 = new TronconLigne(pyrenees, belleville, false, "ligne11");
		TronconLigne l11_h1 = new TronconLigne(pyrenees, jourdain, false, "ligne11");
		TronconLigne l11_h2 = new TronconLigne(jourdain, pyrenees, false, "ligne11");
		TronconLigne l11_i1 = new TronconLigne(jourdain, placeDesFetes, false, "ligne11");
		TronconLigne l11_i2 = new TronconLigne(placeDesFetes, jourdain, false, "ligne11");
		TronconLigne l11_j1 = new TronconLigne(placeDesFetes, telegraphe, false, "ligne11");
		TronconLigne l11_j2 = new TronconLigne(telegraphe, placeDesFetes, false, "ligne11");
		TronconLigne l11_k1 = new TronconLigne(telegraphe, porteDesLilas, false, "ligne11");
		TronconLigne l11_k2 = new TronconLigne(porteDesLilas, telegraphe, false, "ligne11");
		TronconLigne l11_l1 = new TronconLigne(porteDesLilas, mairieDesLilas, false, "ligne11");
		TronconLigne l11_l2 = new TronconLigne(mairieDesLilas, porteDesLilas, false, "ligne11");

		// Ajout des troncons de ligne dans le graphe
		reseau.ajoutTroncon(l11_a1, 1);
		reseau.ajoutTroncon(l11_a2, 2);
		reseau.ajoutTroncon(l11_b1, 3);
		reseau.ajoutTroncon(l11_b2, 4);
		reseau.ajoutTroncon(l11_c1, 1);
		reseau.ajoutTroncon(l11_c2, 2);
		reseau.ajoutTroncon(l11_d1, 3);
		reseau.ajoutTroncon(l11_d2, 4);
		reseau.ajoutTroncon(l11_e1, 1);
		reseau.ajoutTroncon(l11_e2, 2);
		reseau.ajoutTroncon(l11_f1, 3);
		reseau.ajoutTroncon(l11_f2, 4);
		reseau.ajoutTroncon(l11_g1, 1);
		reseau.ajoutTroncon(l11_g2, 2);
		reseau.ajoutTroncon(l11_h1, 3);
		reseau.ajoutTroncon(l11_h2, 4);
		reseau.ajoutTroncon(l11_i1, 1);
		reseau.ajoutTroncon(l11_i2, 2);
		reseau.ajoutTroncon(l11_j1, 3);
		reseau.ajoutTroncon(l11_j2, 4);
		reseau.ajoutTroncon(l11_k1, 1);
		reseau.ajoutTroncon(l11_k2, 2);
		reseau.ajoutTroncon(l11_l1, 3);
		reseau.ajoutTroncon(l11_l2, 4);

		// Changement
		reseau.ajouterChangementLigneStation(belleville, "ligne11", "ligne2", 5);
		reseau.ajouterChangementLigneStation(belleville, "ligne2", "ligne11", 5);
		reseau.ajouterChangementLigneStation(chatelet, "ligne1", "ligne11", 5);
		reseau.ajouterChangementLigneStation(chatelet, "ligne11", "ligne1", 5);
		reseau.ajouterChangementLigneStation(chatelet, "ligne14", "ligne11", 5);
		reseau.ajouterChangementLigneStation(chatelet, "ligne11", "ligne14", 5);
		reseau.ajouterChangementLigneStation(chatelet, "ligne14", "ligne1", 5);
		reseau.ajouterChangementLigneStation(chatelet, "ligne1", "ligne14", 5);
		reseau.ajouterChangementLigneStation(nation, "ligne1", "ligne2", 5);
		reseau.ajouterChangementLigneStation(nation, "ligne2", "ligne1", 5);
		reseau.ajouterChangementLigneStation(hotelDeVille, "ligne11", "ligne14", 5);
		reseau.ajouterChangementLigneStation(hotelDeVille, "ligne14", "ligne11", 5);
		reseau.ajouterChangementLigneStation(charlesDeGaulleEtoile, "ligne1", "ligne2", 5);
		reseau.ajouterChangementLigneStation(charlesDeGaulleEtoile, "ligne2", "ligne1", 5);
		reseau.ajouterChangementLigneStation(gareDeLyon, "ligne14", "ligne1", 5);
		reseau.ajouterChangementLigneStation(gareDeLyon, "ligne1", "ligne14", 5);

		// Ajout des incidents
		/*
		 * graphe.ajoutIncidentStation(lesSablons);
		 * graphe.ajoutIncidentStation(concorde); graphe.ajoutIncidentStation(nation);
		 * graphe.ajoutIncidentStation(rome); graphe.ajoutIncidentStation(jourdain);
		 * graphe.ajoutIncidentStation(gareDeLyon);
		 * graphe.ajoutIncidentStation(telegraphe); ajoutIncidentLigne(l1_q1);
		 * ajoutIncidentLigne(l1_q2); ajoutIncidentLigne(l11_c1);
		 * ajoutIncidentLigne(l1_c2);
		 */

		// HORAIRES LIGNE 1
		// Dans le 1er sens
		reseau.ajouterHoraires(laDefense.getNom(), esplanadeDeLaDefense.getNom(), "ligne1", new Horaire(5, 0),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(esplanadeDeLaDefense.getNom(), pontdeNeuilly.getNom(), "ligne1", new Horaire(5, 2),
				new Horaire(23, 2), 360);
		reseau.ajouterHoraires(pontdeNeuilly.getNom(), lesSablons.getNom(), "ligne1", new Horaire(5, 6),
				new Horaire(23, 6), 360);
		reseau.ajouterHoraires(lesSablons.getNom(), porteMaillot.getNom(), "ligne1", new Horaire(5, 8),
				new Horaire(23, 8), 360);
		reseau.ajouterHoraires(porteMaillot.getNom(), argentine.getNom(), "ligne1", new Horaire(5, 12),
				new Horaire(23, 12), 360);
		reseau.ajouterHoraires(argentine.getNom(), charlesDeGaulleEtoile.getNom(), "ligne1", new Horaire(5, 14),
				new Horaire(23, 14), 360);
		reseau.ajouterHoraires(charlesDeGaulleEtoile.getNom(), georgeV.getNom(), "ligne1", new Horaire(5, 18),
				new Horaire(23, 18), 360);
		reseau.ajouterHoraires(georgeV.getNom(), franklinDRoosevelt.getNom(), "ligne1", new Horaire(5, 20),
				new Horaire(23, 20), 360);
		reseau.ajouterHoraires(franklinDRoosevelt.getNom(), champsElyseesClemenceau.getNom(), "ligne1",
				new Horaire(5, 24), new Horaire(23, 24), 360);
		reseau.ajouterHoraires(champsElyseesClemenceau.getNom(), concorde.getNom(), "ligne1", new Horaire(5, 26),
				new Horaire(23, 26), 360);
		reseau.ajouterHoraires(concorde.getNom(), tuileries.getNom(), "ligne1", new Horaire(5, 30), new Horaire(23, 30),
				360);
		reseau.ajouterHoraires(tuileries.getNom(), palaisRoyalMuseeDuLouvre.getNom(), "ligne1", new Horaire(5, 32),
				new Horaire(23, 32), 360);
		reseau.ajouterHoraires(palaisRoyalMuseeDuLouvre.getNom(), louvreRivoli.getNom(), "ligne1", new Horaire(5, 36),
				new Horaire(23, 36), 360);
		reseau.ajouterHoraires(louvreRivoli.getNom(), chatelet.getNom(), "ligne1", new Horaire(5, 38),
				new Horaire(23, 38), 360);
		reseau.ajouterHoraires(chatelet.getNom(), hotelDeVille.getNom(), "ligne1", new Horaire(5, 42),
				new Horaire(23, 42), 360);
		reseau.ajouterHoraires(hotelDeVille.getNom(), saintPaul.getNom(), "ligne1", new Horaire(5, 44),
				new Horaire(23, 44), 360);
		reseau.ajouterHoraires(saintPaul.getNom(), bastille.getNom(), "ligne1", new Horaire(5, 48), new Horaire(23, 48),
				360);
		reseau.ajouterHoraires(bastille.getNom(), gareDeLyon.getNom(), "ligne1", new Horaire(5, 50),
				new Horaire(23, 50), 360);
		reseau.ajouterHoraires(gareDeLyon.getNom(), reuillyDiderot.getNom(), "ligne1", new Horaire(5, 54),
				new Horaire(23, 54), 360);
		reseau.ajouterHoraires(reuillyDiderot.getNom(), nation.getNom(), "ligne1", new Horaire(5, 56),
				new Horaire(23, 56), 360);
		reseau.ajouterHoraires(nation.getNom(), porteDeVincennes.getNom(), "ligne1", new Horaire(6, 0),
				new Horaire(23, 56), 360);
		reseau.ajouterHoraires(porteDeVincennes.getNom(), saintMande.getNom(), "ligne1", new Horaire(6, 2),
				new Horaire(23, 56), 360);
		reseau.ajouterHoraires(saintMande.getNom(), chateauDeVincennes.getNom(), "ligne1", new Horaire(6, 6),
				new Horaire(23, 56), 360);

		// Dans le 2nd sens

		reseau.ajouterHoraires(esplanadeDeLaDefense.getNom(), laDefense.getNom(), "ligne1", new Horaire(6, 31),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(pontdeNeuilly.getNom(), esplanadeDeLaDefense.getNom(), "ligne1", new Horaire(6, 26),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(lesSablons.getNom(), pontdeNeuilly.getNom(), "ligne1", new Horaire(6, 23),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(porteMaillot.getNom(), lesSablons.getNom(), "ligne1", new Horaire(6, 18),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(argentine.getNom(), porteMaillot.getNom(), "ligne1", new Horaire(6, 15),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(charlesDeGaulleEtoile.getNom(), argentine.getNom(), "ligne1", new Horaire(6, 10),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(georgeV.getNom(), charlesDeGaulleEtoile.getNom(), "ligne1", new Horaire(6, 7),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(franklinDRoosevelt.getNom(), georgeV.getNom(), "ligne1", new Horaire(6, 2),
				new Horaire(23, 59), 360);
		reseau.ajouterHoraires(champsElyseesClemenceau.getNom(), franklinDRoosevelt.getNom(), "ligne1",
				new Horaire(5, 59), new Horaire(23, 59), 360);
		reseau.ajouterHoraires(concorde.getNom(), champsElyseesClemenceau.getNom(), "ligne1", new Horaire(5, 56),
				new Horaire(23, 56), 360);
		reseau.ajouterHoraires(tuileries.getNom(), concorde.getNom(), "ligne1", new Horaire(5, 51), new Horaire(23, 51),
				360);
		reseau.ajouterHoraires(palaisRoyalMuseeDuLouvre.getNom(), tuileries.getNom(), "ligne1", new Horaire(5, 48),
				new Horaire(23, 48), 360);
		reseau.ajouterHoraires(louvreRivoli.getNom(), palaisRoyalMuseeDuLouvre.getNom(), "ligne1", new Horaire(5, 43),
				new Horaire(23, 43), 360);
		reseau.ajouterHoraires(chatelet.getNom(), louvreRivoli.getNom(), "ligne1", new Horaire(5, 40),
				new Horaire(23, 40), 360);
		reseau.ajouterHoraires(hotelDeVille.getNom(), chatelet.getNom(), "ligne1", new Horaire(5, 37),
				new Horaire(23, 37), 360);
		reseau.ajouterHoraires(saintPaul.getNom(), hotelDeVille.getNom(), "ligne1", new Horaire(5, 32),
				new Horaire(23, 32), 360);
		reseau.ajouterHoraires(bastille.getNom(), saintPaul.getNom(), "ligne1", new Horaire(5, 29), new Horaire(23, 29),
				360);
		reseau.ajouterHoraires(gareDeLyon.getNom(), bastille.getNom(), "ligne1", new Horaire(5, 24),
				new Horaire(23, 24), 360);
		reseau.ajouterHoraires(reuillyDiderot.getNom(), gareDeLyon.getNom(), "ligne1", new Horaire(5, 21),
				new Horaire(23, 21), 360);
		reseau.ajouterHoraires(nation.getNom(), reuillyDiderot.getNom(), "ligne1", new Horaire(5, 16),
				new Horaire(23, 16), 360);
		reseau.ajouterHoraires(porteDeVincennes.getNom(), nation.getNom(), "ligne1", new Horaire(5, 13),
				new Horaire(23, 13), 360);
		reseau.ajouterHoraires(saintMande.getNom(), porteDeVincennes.getNom(), "ligne1", new Horaire(5, 8),
				new Horaire(23, 8), 360);
		reseau.ajouterHoraires(berault.getNom(), saintMande.getNom(), "ligne1", new Horaire(5, 5), new Horaire(23, 5),
				360);
		reseau.ajouterHoraires(chateauDeVincennes.getNom(), berault.getNom(), "ligne1", new Horaire(5, 0),
				new Horaire(23, 0), 360);

		// HORAIRES Ligne14
		// 1er sens
		reseau.ajouterHoraires(saintLazare.getNom(), madeleine.getNom(), "ligne14", new Horaire(5, 0),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(madeleine.getNom(), pyramides.getNom(), "ligne14", new Horaire(5, 3), new Horaire(23, 3),
				360);
		reseau.ajouterHoraires(pyramides.getNom(), chatelet.getNom(), "ligne14", new Horaire(5, 8), new Horaire(23, 8),
				360);
		reseau.ajouterHoraires(chatelet.getNom(), gareDeLyon.getNom(), "ligne14", new Horaire(5, 12),
				new Horaire(23, 12), 360);
		reseau.ajouterHoraires(gareDeLyon.getNom(), bercy.getNom(), "ligne14", new Horaire(5, 17), new Horaire(23, 17),
				360);
		reseau.ajouterHoraires(bercy.getNom(), courSaintEmilion.getNom(), "ligne14", new Horaire(5, 20),
				new Horaire(23, 20), 360);
		reseau.ajouterHoraires(courSaintEmilion.getNom(), bibliothqueFrancoisMitterand.getNom(), "ligne14",
				new Horaire(5, 25), new Horaire(23, 25), 360);
		reseau.ajouterHoraires(bibliothqueFrancoisMitterand.getNom(), olympiades.getNom(), "ligne14",
				new Horaire(5, 28), new Horaire(23, 28), 360);

		// 2e sens
		reseau.ajouterHoraires(madeleine.getNom(), saintLazare.getNom(), "ligne14", new Horaire(5, 20),
				new Horaire(23, 20), 360);
		reseau.ajouterHoraires(pyramides.getNom(), madeleine.getNom(), "ligne14", new Horaire(5, 18),
				new Horaire(23, 18), 360);
		reseau.ajouterHoraires(chatelet.getNom(), pyramides.getNom(), "ligne14", new Horaire(5, 14),
				new Horaire(23, 14), 360);
		reseau.ajouterHoraires(gareDeLyon.getNom(), chatelet.getNom(), "ligne14", new Horaire(5, 12),
				new Horaire(23, 12), 360);
		reseau.ajouterHoraires(bercy.getNom(), gareDeLyon.getNom(), "ligne14", new Horaire(5, 8), new Horaire(23, 8),
				360);
		reseau.ajouterHoraires(courSaintEmilion.getNom(), bercy.getNom(), "ligne14", new Horaire(5, 6),
				new Horaire(23, 6), 360);
		reseau.ajouterHoraires(bibliothqueFrancoisMitterand.getNom(), courSaintEmilion.getNom(), "ligne14",
				new Horaire(5, 2), new Horaire(23, 2), 360);
		reseau.ajouterHoraires(olympiades.getNom(), bibliothqueFrancoisMitterand.getNom(), "ligne14", new Horaire(5, 0),
				new Horaire(23, 0), 360);

		// Horaire Ligne11
		// 1er sens
		reseau.ajouterHoraires(chatelet.getNom(), hotelDeVille.getNom(), "ligne11", new Horaire(5, 0),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(hotelDeVille.getNom(), rambuteau.getNom(), "ligne11", new Horaire(5, 2),
				new Horaire(23, 2), 360);
		reseau.ajouterHoraires(rambuteau.getNom(), artsEtMetiers.getNom(), "ligne11", new Horaire(5, 6),
				new Horaire(23, 6), 360);
		reseau.ajouterHoraires(artsEtMetiers.getNom(), republique.getNom(), "ligne11", new Horaire(5, 8),
				new Horaire(23, 8), 360);
		reseau.ajouterHoraires(republique.getNom(), goncourt.getNom(), "ligne11", new Horaire(5, 12),
				new Horaire(23, 12), 360);
		reseau.ajouterHoraires(goncourt.getNom(), belleville.getNom(), "ligne11", new Horaire(5, 14),
				new Horaire(23, 14), 360);
		reseau.ajouterHoraires(belleville.getNom(), pyrenees.getNom(), "ligne11", new Horaire(5, 18),
				new Horaire(23, 18), 360);
		reseau.ajouterHoraires(pyrenees.getNom(), jourdain.getNom(), "ligne11", new Horaire(5, 20), new Horaire(23, 20),
				360);
		reseau.ajouterHoraires(jourdain.getNom(), placeDesFetes.getNom(), "ligne11", new Horaire(5, 24),
				new Horaire(23, 24), 360);
		reseau.ajouterHoraires(placeDesFetes.getNom(), telegraphe.getNom(), "ligne11", new Horaire(5, 26),
				new Horaire(23, 26), 360);
		reseau.ajouterHoraires(telegraphe.getNom(), porteDesLilas.getNom(), "ligne11", new Horaire(5, 30),
				new Horaire(23, 30), 360);
		reseau.ajouterHoraires(porteDesLilas.getNom(), mairieDesLilas.getNom(), "ligne11", new Horaire(5, 32),
				new Horaire(23, 32), 360);

		// 2nd sens

		reseau.ajouterHoraires(hotelDeVille.getNom(), chatelet.getNom(), "ligne11", new Horaire(5, 0),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(rambuteau.getNom(), hotelDeVille.getNom(), "ligne11", new Horaire(5, 3),
				new Horaire(23, 3), 360);
		reseau.ajouterHoraires(artsEtMetiers.getNom(), rambuteau.getNom(), "ligne11", new Horaire(5, 8),
				new Horaire(23, 8), 360);
		reseau.ajouterHoraires(republique.getNom(), artsEtMetiers.getNom(), "ligne11", new Horaire(5, 11),
				new Horaire(23, 11), 360);
		reseau.ajouterHoraires(goncourt.getNom(), republique.getNom(), "ligne11", new Horaire(5, 16),
				new Horaire(23, 16), 360);
		reseau.ajouterHoraires(belleville.getNom(), goncourt.getNom(), "ligne11", new Horaire(5, 19),
				new Horaire(23, 19), 360);
		reseau.ajouterHoraires(pyrenees.getNom(), belleville.getNom(), "ligne11", new Horaire(5, 24),
				new Horaire(23, 24), 360);
		reseau.ajouterHoraires(jourdain.getNom(), pyrenees.getNom(), "ligne11", new Horaire(5, 27), new Horaire(23, 27),
				360);
		reseau.ajouterHoraires(placeDesFetes.getNom(), jourdain.getNom(), "ligne11", new Horaire(5, 32),
				new Horaire(23, 32), 360);
		reseau.ajouterHoraires(telegraphe.getNom(), placeDesFetes.getNom(), "ligne11", new Horaire(5, 37),
				new Horaire(23, 37), 360);
		reseau.ajouterHoraires(porteDesLilas.getNom(), telegraphe.getNom(), "ligne11", new Horaire(5, 40),
				new Horaire(23, 40), 360);
		reseau.ajouterHoraires(mairieDesLilas.getNom(), porteDesLilas.getNom(), "ligne11", new Horaire(5, 45),
				new Horaire(23, 45), 360);

		// Horaire Ligne2
		// 1er sens
		reseau.ajouterHoraires(porteDauphine.getNom(), victorHugo.getNom(), "ligne2", new Horaire(5, 0),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(victorHugo.getNom(), charlesDeGaulleEtoile.getNom(), "ligne2", new Horaire(5, 5),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(charlesDeGaulleEtoile.getNom(), ternes.getNom(), "ligne2", new Horaire(5, 8),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(ternes.getNom(), courcelles.getNom(), "ligne2", new Horaire(5, 13), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(courcelles.getNom(), monceau.getNom(), "ligne2", new Horaire(5, 16), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(monceau.getNom(), villiers.getNom(), "ligne2", new Horaire(5, 21), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(villiers.getNom(), rome.getNom(), "ligne2", new Horaire(5, 24), new Horaire(23, 0), 360);
		reseau.ajouterHoraires(rome.getNom(), placeDeClichy.getNom(), "ligne2", new Horaire(5, 29), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(placeDeClichy.getNom(), blanche.getNom(), "ligne2", new Horaire(5, 32),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(blanche.getNom(), pigalle.getNom(), "ligne2", new Horaire(5, 37), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(pigalle.getNom(), anvers.getNom(), "ligne2", new Horaire(5, 40), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(anvers.getNom(), barbesRochechouart.getNom(), "ligne2", new Horaire(5, 45),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(barbesRochechouart.getNom(), laChapelle.getNom(), "ligne2", new Horaire(5, 48),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(laChapelle.getNom(), stalingrad.getNom(), "ligne2", new Horaire(5, 53),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(stalingrad.getNom(), jaures.getNom(), "ligne2", new Horaire(5, 56), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(jaures.getNom(), colonelFabien.getNom(), "ligne2", new Horaire(6, 1), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(colonelFabien.getNom(), belleville.getNom(), "ligne2", new Horaire(6, 6),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(belleville.getNom(), couronnes.getNom(), "ligne2", new Horaire(6, 9), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(couronnes.getNom(), menilmontant.getNom(), "ligne2", new Horaire(6, 14),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(menilmontant.getNom(), pereLachaise.getNom(), "ligne2", new Horaire(6, 17),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(pereLachaise.getNom(), philippeAuguste.getNom(), "ligne2", new Horaire(6, 22),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(philippeAuguste.getNom(), alexandreDumas.getNom(), "ligne2", new Horaire(6, 25),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(alexandreDumas.getNom(), avron.getNom(), "ligne2", new Horaire(6, 30),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(avron.getNom(), nation.getNom(), "ligne2", new Horaire(6, 33), new Horaire(23, 0), 360);

		// 2e sens
		reseau.ajouterHoraires(nation.getNom(), avron.getNom(), "ligne2", new Horaire(6, 12), new Horaire(23, 0), 360);
		reseau.ajouterHoraires(avron.getNom(), alexandreDumas.getNom(), "ligne2", new Horaire(6, 8), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(alexandreDumas.getNom(), philippeAuguste.getNom(), "ligne2", new Horaire(6, 6),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(philippeAuguste.getNom(), pereLachaise.getNom(), "ligne2", new Horaire(6, 2),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(pereLachaise.getNom(), menilmontant.getNom(), "ligne2", new Horaire(5, 58),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(menilmontant.getNom(), couronnes.getNom(), "ligne2", new Horaire(5, 56),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(couronnes.getNom(), belleville.getNom(), "ligne2", new Horaire(5, 52),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(belleville.getNom(), colonelFabien.getNom(), "ligne2", new Horaire(5, 48),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(colonelFabien.getNom(), jaures.getNom(), "ligne2", new Horaire(5, 46),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(jaures.getNom(), stalingrad.getNom(), "ligne2", new Horaire(5, 42), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(stalingrad.getNom(), laChapelle.getNom(), "ligne2", new Horaire(5, 40),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(laChapelle.getNom(), barbesRochechouart.getNom(), "ligne2", new Horaire(5, 36),
				new Horaire(23, 29), 360);
		reseau.ajouterHoraires(barbesRochechouart.getNom(), anvers.getNom(), "ligne2", new Horaire(5, 32),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(anvers.getNom(), pigalle.getNom(), "ligne2", new Horaire(5, 30), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(pigalle.getNom(), blanche.getNom(), "ligne2", new Horaire(5, 26), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(blanche.getNom(), placeDeClichy.getNom(), "ligne2", new Horaire(5, 24),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(placeDeClichy.getNom(), rome.getNom(), "ligne2", new Horaire(5, 20), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(rome.getNom(), villiers.getNom(), "ligne2", new Horaire(5, 18), new Horaire(23, 0), 360);
		reseau.ajouterHoraires(villiers.getNom(), monceau.getNom(), "ligne2", new Horaire(5, 16), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(monceau.getNom(), courcelles.getNom(), "ligne2", new Horaire(5, 12), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(courcelles.getNom(), ternes.getNom(), "ligne2", new Horaire(5, 10), new Horaire(23, 0),
				360);
		reseau.ajouterHoraires(ternes.getNom(), charlesDeGaulleEtoile.getNom(), "ligne2", new Horaire(5, 6),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(charlesDeGaulleEtoile.getNom(), victorHugo.getNom(), "ligne2", new Horaire(5, 4),
				new Horaire(23, 0), 360);
		reseau.ajouterHoraires(victorHugo.getNom(), porteDauphine.getNom(), "ligne2", new Horaire(5, 0),
				new Horaire(23, 0), 360);

		return reseau;
	}

}
