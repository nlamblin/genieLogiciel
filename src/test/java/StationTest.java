import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import donnees.Horaire;
import donnees.Station;

class StationTest {

	@DisplayName("Egalite de noeuds")
	@Test
	void test1() {
		Station A = new Station("A");
		Station B = new Station("A", false, 1);
		assertEquals(A, B);
	}

	@DisplayName("Ingalite de noeuds")
	@Test
	void test2() {
		Station A = new Station("A", true, 1);
		Station B = new Station("B");
		assertNotEquals(A, B);
	}

	@DisplayName("Ajout et existance")
	@Test
	void test3() {
		Station A = new Station("A");
		assertFalse(A.contientHoraireLigneDirection("B", "1"));
		A.ajouterHoraire(10, 150, "1", "B", 10);
		assertTrue(A.contientHoraireLigneDirection("B", "1"));
	}

	@DisplayName("Suppression")
	@Test
	void test4() {

		Station A = new Station("A");
		assertFalse(A.contientHoraireLigneDirection("B", "1"));
		A.ajouterHoraire(10, 150, "1", "B", 10);
		assertTrue(A.supprimerHoraire("B", "1"));
		assertFalse(A.contientHoraireLigneDirection("B", "1"));
	}

	@DisplayName("Recuperation si pas d'horaires")
	@Test
	void test5() {
		Station A = new Station("A");
		assertEquals(new ArrayList<Horaire>(), A.recupererHoraire("B", "1"));
	}

}
