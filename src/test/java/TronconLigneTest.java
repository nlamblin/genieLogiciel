import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import donnees.Station;
import donnees.TronconLigne;

class TronconLigneTest {

	@DisplayName("Egalite d'arcs - incident différent")
	@Test
	void test() {
		TronconLigne a = new TronconLigne(new Station("A"), new Station("B"), true, "1");
		TronconLigne b = new TronconLigne(new Station("A"), new Station("B"), false, "1");
		assertEquals(a, b);
	}

	@DisplayName("Egalite d'arcs")
	@Test
	void test1() {
		TronconLigne a = new TronconLigne(new Station("A"), new Station("B"), "1");
		TronconLigne b = new TronconLigne(new Station("A"), new Station("B"), "1");
		assertEquals(a, b);
	}

	@DisplayName("Inegalite d'arcs - Ligne")
	@Test
	void test2() {
		TronconLigne a = new TronconLigne(new Station("A"), new Station("B"), "1");
		TronconLigne b = new TronconLigne(new Station("A"), new Station("B"), "2");
		assertNotEquals(a, b);
	}

	@DisplayName("Inegalite d'arcs - stations")
	@Test
	void test3() {
		TronconLigne a = new TronconLigne(new Station("A"), new Station("B"), "1");
		TronconLigne b = new TronconLigne(new Station("A"), new Station("C"), "1");
		assertNotEquals(a, b);
	}

	@DisplayName("Inegalite d'arcs avec espacement")
	@Test
	void test4() {
		TronconLigne a = new TronconLigne(new Station("A"), new Station("B"), "1");
		TronconLigne b = new TronconLigne(new Station("A"), new Station("B "), "1");
		assertNotEquals(a, b);
	}

	@DisplayName("Arc sans incident")
	@Test
	void test5() {
		TronconLigne a = new TronconLigne(new Station("A"), new Station("B"), "1");
		TronconLigne b = new TronconLigne(new Station("B"), new Station("A"), false, "1");
		assertFalse(a.isIncident());
		assertFalse(b.isIncident());
	}

	@DisplayName("Arc avec incident")
	@Test
	void test6() {
		TronconLigne a = new TronconLigne(new Station("A"), new Station("B"), true, "1");
		assertTrue(a.isIncident());
	}

}
