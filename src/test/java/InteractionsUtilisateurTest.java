import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import donnees.Reseau;
import donnees.Station;
import donnees.TronconLigne;
import exceptions.StationDejaExistante;
import exceptions.SuccesseurNoeudDejaExistant;
import interactions.InteractionsUtilisateur;

class InteractionsUtilisateurTest {

	Reseau reseau;
	InteractionsUtilisateur ie;

	@DisplayName("Verification numéro de station destination existant")
	@Test
	void verifStationDestination1() {
		List<Station> stations = ie.recupererListeStationDestination(new Station("A"));
		assertTrue(ie.verifNumeroStation(4, stations));
	}

	@DisplayName("Verification numéro de station destination non existant")
	@Test
	void verifStationDestination2() {
		List<Station> stations = ie.recupererListeStationDestination(new Station("A"));
		assertFalse(ie.verifNumeroStation(0, stations));
	}

	@DisplayName("Verification de station intermediaire existante")
	@Test
	void verifStationIntermediaire1() {
		List<Station> stationsIntermediaires = new ArrayList<>();
		List<Station> stationsDisponibles = ie.recupererListeStationsIntermediaires(new Station("A"), new Station("B"),
				stationsIntermediaires);
		assertTrue(ie.verifNumeroStation(3, stationsDisponibles));
	}

	@DisplayName("Verification de station intermediaire non existante")
	@Test
	void verifStationIntermediaire2() {
		List<Station> stationsIntermediaires = new ArrayList<>();
		List<Station> stationsDisponibles = ie.recupererListeStationsIntermediaires(new Station("A"), new Station("B"),
				stationsIntermediaires);
		assertFalse(ie.verifNumeroStation(-2, stationsDisponibles));
	}

	@DisplayName("Verification de station intermediaire non présente dans la liste")
	@Test
	void verifStationIntermediaire3() {
		List<Station> stationsIntermediaires = new ArrayList<>();
		stationsIntermediaires.add(new Station("C"));
		assertTrue(ie.recupererListeStationsIntermediaires(new Station("A"), new Station("B"), stationsIntermediaires)
				.contains(new Station("D")));
	}

	@DisplayName("Verification de station intermediaire deja présente dans la liste")
	@Test
	void verifStationIntermediaire4() {
		List<Station> stationsIntermediaires = new ArrayList<>();
		stationsIntermediaires.add(new Station("C"));
		assertFalse(ie.recupererListeStationsIntermediaires(new Station("A"), new Station("B"), stationsIntermediaires)
				.contains(new Station("C")));
	}

	@BeforeEach
	void creation() {
		this.reseau = new Reseau();
		this.ie = new InteractionsUtilisateur(reseau);

		reseau.ajoutLigne("1");

		try {
			reseau.ajoutStation(new Station("A"));
			reseau.ajoutStation(new Station("B"));
			reseau.ajoutStation(new Station("C"));
			reseau.ajoutStation(new Station("D"));
			reseau.ajoutStation(new Station("E"));
			reseau.ajoutStation(new Station("F"));
		} catch (StationDejaExistante e) {
			e.printStackTrace();
		}

		try {
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("B"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("C"), "1"), 4);
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("E"), "1"), 5);

			reseau.ajoutTroncon(new TronconLigne(new Station("B"), new Station("A"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("B"), new Station("D"), "1"), 6);

			reseau.ajoutTroncon(new TronconLigne(new Station("C"), new Station("A"), "1"), 4);
			reseau.ajoutTroncon(new TronconLigne(new Station("C"), new Station("F"), "1"), 3);

			reseau.ajoutTroncon(new TronconLigne(new Station("D"), new Station("B"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("D"), new Station("F"), "1"), 1);

			reseau.ajoutTroncon(new TronconLigne(new Station("E"), new Station("A"), "1"), 5);
			reseau.ajoutTroncon(new TronconLigne(new Station("E"), new Station("F"), "1"), 3);

			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("E"), "1"), 3);
			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("C"), "1"), 3);
			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("D"), "1"), 1);
		} catch (SuccesseurNoeudDejaExistant e) {
			e.printStackTrace();
		}

	}
}
