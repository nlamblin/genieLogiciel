import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import donnees.Reseau;
import donnees.Station;
import donnees.TronconLigne;
import exceptions.StationDejaExistante;
import exceptions.SuccesseurNoeudDejaExistant;
import interactions.Interaction;

class InteractionTest {

	Reseau reseau;
	Interaction interaction;

	@DisplayName("Liste de stations")
	@Test
	void test1() {
		List<Station> liste = new ArrayList<Station>();
		liste.add(new Station("A"));
		liste.add(new Station("B"));
		liste.add(new Station("C"));
		liste.add(new Station("D"));
		liste.add(new Station("E"));
		liste.add(new Station("F"));
		assertEquals(liste, interaction.recupererListeToutesStations());
	}

	@DisplayName("Liste de stations avec incidents")
	@Test
	void test2() {
		List<Station> liste = new ArrayList<Station>();
		liste.add(new Station("A"));
		liste.add(new Station("D"));
		assertEquals(liste, interaction.recupererListeStationAvecIncident());
	}

	@DisplayName("Liste de stations sans incidents")
	@Test
	void test3() {
		List<Station> liste = new ArrayList<Station>();
		liste.add(new Station("B"));
		liste.add(new Station("C"));
		liste.add(new Station("E"));
		liste.add(new Station("F"));
		assertEquals(liste, interaction.recupererListeStationSansIncident());
	}

	@DisplayName("Verification numéro de station source existant")
	@Test
	void verifStation1Source() {
		List<Station> stations = interaction.recupererListeToutesStations();
		assertTrue(interaction.verifNumeroStation(2, stations));
	}

	@DisplayName("Verification numéro de station source non existant")
	@Test
	void verifStationSource2() {
		List<Station> stations = interaction.recupererListeToutesStations();
		assertFalse(interaction.verifNumeroStation(30, stations));
	}

	@DisplayName("Liste troncon avec incident")
	@Test
	void test4() {
		ArrayList<TronconLigne> liste = new ArrayList<TronconLigne>();
		liste.addAll(reseau.getTronconsReseau());
		assertEquals(liste, interaction.recupererListeTousTroncons());
	}

	@DisplayName("Liste troncon sans incident")
	@Test
	void test5() {
		ArrayList<TronconLigne> liste = new ArrayList<TronconLigne>();
		liste.addAll(reseau.getTronconsReseau());
		assertEquals(liste, interaction.recupererListeTousTroncons());
	}

	@DisplayName("Liste troncons")
	@Test
	void test6() {
		ArrayList<TronconLigne> liste = new ArrayList<TronconLigne>();
		liste.addAll(reseau.getTronconsReseau());
		assertEquals(liste, interaction.recupererListeTousTroncons());
	}

	@BeforeEach
	void creation() {
		this.reseau = new Reseau();
		this.interaction = new Interaction(reseau);

		reseau.ajoutLigne("1");

		try {
			reseau.ajoutStation(new Station("A", true));
			reseau.ajoutStation(new Station("B"));
			reseau.ajoutStation(new Station("C"));
			reseau.ajoutStation(new Station("D", true));
			reseau.ajoutStation(new Station("E"));
			reseau.ajoutStation(new Station("F"));
		} catch (StationDejaExistante e) {
			e.printStackTrace();
		}

		try {
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("B"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("C"), "1"), 4);
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("E"), "1"), 5);

			reseau.ajoutTroncon(new TronconLigne(new Station("B"), new Station("A"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("B"), new Station("D"), "1"), 6);

			reseau.ajoutTroncon(new TronconLigne(new Station("C"), new Station("A"), "1"), 4);
			reseau.ajoutTroncon(new TronconLigne(new Station("C"), new Station("F"), "1"), 3);

			reseau.ajoutTroncon(new TronconLigne(new Station("D"), new Station("B"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("D"), new Station("F"), "1"), 1);

			reseau.ajoutTroncon(new TronconLigne(new Station("E"), new Station("A"), "1"), 5);
			reseau.ajoutTroncon(new TronconLigne(new Station("E"), new Station("F"), "1"), 3);

			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("E"), "1"), 3);
			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("C"), "1"), 3);
			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("D"), "1"), 1);
		} catch (SuccesseurNoeudDejaExistant e) {
			e.printStackTrace();
		}

	}

}
