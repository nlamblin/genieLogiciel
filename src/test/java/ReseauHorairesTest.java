import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import donnees.Horaire;
import donnees.Reseau;
import donnees.Station;
import donnees.TronconLigne;
import exceptions.StationDejaExistante;
import exceptions.SuccesseurNoeudDejaExistant;

class ReseauHorairesTest {

	Reseau reseau;

	@DisplayName("Ajout et existance")
	@Test
	void test() {
		assertFalse(this.reseau.existanceHoraire("A", "B", "1"));
		this.reseau.ajouterHoraires("A", "B", "1", new Horaire(6, 0), new Horaire(8, 0), 30);
		assertTrue(this.reseau.existanceHoraire("A", "B", "1"));
	}

	@DisplayName("Suppression")
	@Test
	void test2() {
		assertFalse(this.reseau.supprimerHoraire("A", "B", "1"));
		this.reseau.ajouterHoraires("A", "B", "1", new Horaire(6, 0), new Horaire(8, 0), 30);
		assertTrue(this.reseau.supprimerHoraire("A", "B", "1"));
	}

	@DisplayName("Contenu des horaires - horaires de jour")
	@Test
	void test3() {
		this.reseau.ajouterHoraires("A", "B", "1", new Horaire(6, 0), new Horaire(8, 0), 1800);
		ArrayList<Horaire> liste = new ArrayList<Horaire>();
		liste.add(new Horaire(6, 0));
		liste.add(new Horaire(6, 30));
		liste.add(new Horaire(7, 0));
		liste.add(new Horaire(7, 30));
		liste.add(new Horaire(8, 0));
		assertEquals(liste, this.reseau.recupererHoraire("A", "B", "1"));
	}

	@DisplayName("Contenu des horaires - horaires de nuit")
	@Test
	void test4() {
		this.reseau.ajouterHoraires("A", "B", "1", new Horaire(23, 0), new Horaire(1, 0), 1800);
		ArrayList<Horaire> liste = new ArrayList<Horaire>();
		liste.add(new Horaire(23, 0));
		liste.add(new Horaire(23, 30));
		liste.add(new Horaire(0, 0));
		liste.add(new Horaire(0, 30));
		liste.add(new Horaire(1, 0));
		assertEquals(liste, this.reseau.recupererHoraire("A", "B", "1"));
	}

	@BeforeEach
	void creation() {
		this.reseau = new Reseau();

		reseau.ajoutLigne("1");

		try {
			reseau.ajoutStation(new Station("A"));
			reseau.ajoutStation(new Station("B"));
			reseau.ajoutStation(new Station("C"));
			reseau.ajoutStation(new Station("D"));
			reseau.ajoutStation(new Station("E"));
			reseau.ajoutStation(new Station("F"));
		} catch (StationDejaExistante e) {
			e.printStackTrace();
		}

		try {
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("B"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("C"), "1"), 4);
			reseau.ajoutTroncon(new TronconLigne(new Station("A"), new Station("E"), "1"), 5);

			reseau.ajoutTroncon(new TronconLigne(new Station("B"), new Station("A"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("B"), new Station("D"), "1"), 6);

			reseau.ajoutTroncon(new TronconLigne(new Station("C"), new Station("A"), "1"), 4);
			reseau.ajoutTroncon(new TronconLigne(new Station("C"), new Station("F"), "1"), 3);

			reseau.ajoutTroncon(new TronconLigne(new Station("D"), new Station("B"), "1"), 6);
			reseau.ajoutTroncon(new TronconLigne(new Station("D"), new Station("F"), "1"), 1);

			reseau.ajoutTroncon(new TronconLigne(new Station("E"), new Station("A"), "1"), 5);
			reseau.ajoutTroncon(new TronconLigne(new Station("E"), new Station("F"), "1"), 3);

			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("E"), "1"), 3);
			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("C"), "1"), 3);
			reseau.ajoutTroncon(new TronconLigne(new Station("F"), new Station("D"), "1"), 1);
		} catch (SuccesseurNoeudDejaExistant e) {
			e.printStackTrace();
		}

	}

}
