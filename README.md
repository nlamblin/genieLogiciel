
# Génie Logiciel - Application de calcul d'itinéraires

Cette application mobile a pour but de guider son utilisateur dans le métro parisien :
* L’utilisateur aura la possibilité d'utiliser le système de géolocalisation pour trouver la station de départ la plus proche
* Il pourra aussi choisir de partir d'une station particulière pour effectuer son trajet
* L'application lui proposera alors de choisir parmi trois moyens pour planifier son itinéraire :
	- Chemin le plus court
	- Chemin avec le moins de changement de ligne
	- Itinéraire comprenant plusieurs points de passages donnés
* L'application permet aussi de se renseigner sur les horaires de passages pour une ligne / station donnée

## Sommaire

* [Préparation](https://gitlab.com/nlamblin/genieLogiciel#préparation)
	- [Formattage automatique du code](https://gitlab.com/nlamblin/genieLogiciel#formatage-automatique-du-code)
	- [Préparer l'utilisation de Git](https://gitlab.com/nlamblin/genieLogiciel#préparer-lutilisation-de-git)
* [Ressources](https://gitlab.com/nlamblin/genieLogiciel#ressources)
	- [Conventions internes](https://gitlab.com/nlamblin/genieLogiciel#conventions-internes)
	- [Ligne de conduite](https://gitlab.com/nlamblin/genieLogiciel#ligne-de-conduite)
* [Informations supplémentaires](https://gitlab.com/nlamblin/genieLogiciel#informations-supplémentaires)
	- [Historique de versions](https://gitlab.com/nlamblin/genieLogiciel#historique-de-versions)
	- [Contributeurs](https://gitlab.com/nlamblin/genieLogiciel#contributeurs)

---
### Préparation


#### Formatage automatique du code

##### Eclipse JEE Oxygen :

Aller dans `Paramètres` > `Java` > `Editor` > `Save Actions`

Cocher `Perform the selected actions on save` et `Format source code`

##### IntelliJ IDEA :

* Installer le plugin [`Eclipse Code Formatter`](https://plugins.jetbrains.com/plugin/6546-eclipse-code-formatter)

Aller dans `Settings` > `Other settings` > `Eclipse Code Formatter`
Cocher `Use the Eclipse Code Formatter`

* Installer le plugin [`Save Actions Plugin`](https://plugins.jetbrains.com/plugin/7642-save-actions)

Aller dans `Settings` > `Other settings` > `Save Actions`
Dans le champ `Eclipse Java formatter config file` donner le chemin `docs/Java-Formatter-GL.xml` du dépôt

#### Préparer l'utilisation de Git

Changer l'éditeur par défaut
```
git config --global core.editor nano
```

Ajouter les informations pour le dépôt [ou tous les dépôts]
```
git config [--global] user.name <name>
git config [--global] user.email <email>
```

Ajouter les identifiants pour le dépôt [ou tous les dépôts]


Cloner le dépôt Git
```
git clone https://gitlab.com/nlamblin/genieLogiciel
```

---
### Ressources

#### Conventions internes

* **Git** - _Mise en forme de commit, gestion du dépôt_ - [Conventions Git](hhttps://docs.google.com/document/d/16GJfgQDe8OE3UT9tFJh5VnYNXPWYbztkNkPD0ddukU4/edit?usp=sharing)
* **Java** - _Formatage, nommage et  bonnes pratiques_ - [Conventions Java](https://docs.google.com/document/d/13Os7LKiOElF41U9_iORzba81EmFPGyfhLldcfeHlZ2g/edit?usp=sharing)

#### Ligne de conduite

À chaque nouvelle itération une réunion à été organisée pour aborder les points suivants :
* Faire un bilan de l'itération passée : lister les problèmes rencontrés et prendre en note les solutions éventuelles pour les appliquer dans le futur
* Analyser la qualité du code produit à l'aide de SonarQube, modifier le code en conséquence et noter ces remarques pour ne plus les reproduires
* Définir ensemble la direction de l'itération prochaine de manière plus détaillée : Analyser les tâches du backlog Trello associées, et concevoir ensemble une manière de l'implémenter

Nous avons essayer d'utiliser Maven pour lancer les tests avant de faire des commits sur la branche _master_ pour garantir la stabilité du code poussé sur le dépôt

---
### Informations supplémentaires

#### Historique de versions

Vous pouvez retrouver l'historique de version détaillé directement sur le [dépôt gitlab](https://gitlab.com/nlamblin/genieLogiciel/tags)

* **Première itération** _[75094df3](https://gitlab.com/nlamblin/genieLogiciel/commit/75094df3341eed28176d40a641bb978abb963b58)_ (**tag: v1.0**)
	- Initialisation du projet
	- Définition des lignes de conduites du projet (conventions et uniformisation des IDEs)
	- Système de graphe basique (Arcs)
	- Implémentation de l'agorithme Dijkstra (chemin le plus court)
	- Prise en main des outils utilisés (Maven, SonarQube, Git)
	- Interface utilisateur basique en console

* **Deuxième itération** _[75094df3](https://gitlab.com/nlamblin/genieLogiciel/commit/75094df3341eed28176d40a641bb978abb963b58)_ (**tag: v2.0**)
	- Ajout de la prise en charge de Noeuds dans le graphe
	- Adaptation du code existant aux nouvelles fonctionnalités (Graphe, Arcs et Dijkstra)
	- Mise à jour de l'interface utilisateur

* **Troisième itération** _[fb5ff188](https://gitlab.com/nlamblin/genieLogiciel/commit/fb5ff18848ce6255a92383ac88f8819f3d08d805)_ (**tag: v3.0**)
	- Modélisation des temps d'arrêts et incidents aux Stations
	- Ajout d'incidents sur les lignes entre deux Stations
	- Adaptation du code existant aux nouvelles fonctionnalités  (Interface, Graphe, Dijkstra)
_Tâches non terminées et reportées à l'itération suivante :_
	- _Gestion des bugs_
	- _Sérialisation_

* **Quatrième itération** _[df3942bd](https://gitlab.com/nlamblin/genieLogiciel/commit/df3942bdce95c43903db8b3b18b4ad53d63d08d8)_ (**tag: v4.0**)
	- Géolocalisation (Station la plus proche de l'utilisateur)
	- Modélisation des lignes
	- Création d'un jeu de données de tests en utilisant des données de la [RATP](https://data.ratp.fr/explore/)
	- Adaptation du code existant aux nouvelles fonctionnalités  (Interface, Graphe, Dijkstra)
	- Finalisation des tâches non terminés dans l'itération 3
_Tâches non terminées et reportées à l'itération suivante :_
	- _Gestion des bugs_
	- _SonarQube_

* **Cinquième itération** _[ab5d0b82](https://gitlab.com/nlamblin/genieLogiciel/commit/ab5d0b823f804ffb92f08e0107dbe9672d3f13cf)_ (**tag: v5.0**)
	- Ajout des horaires de passages
	- Adaptation du code existant aux nouvelles fonctionnalités  (Interface, Graphe, Dijkstra)
	- Mise à jour de la sérialisation
	- Finalisation des tâches non terminées dans l'itération 4
	- Refactors pour simplifier ou clarifier le code

* **Sixème itération** _(https://gitlab.com/nlamblin/genieLogiciel/commit/432650c82c3b75a49a112030e7ebe7c25efc8152)_ (**tag: v6.0**)
	- Ajout d'un logger simple pour l'affichage des menus
	- Refactors pour simplifier ou clarifier le code
	- Ajout d'un profil "informaticien" qui à plus de droits
	- Tests et corrections de bugs

#### Contributeurs


* [Dolet Maxime](https://gitlab.com/MaxDolet)
* [Lamblin Nicolas](https://gitlab.com/nlamblin)
* [Mansuy Claire](https://gitlab.com/clairemansuy)
* [Thiriot Anaïs](https://gitlab.com/AnaisTh)
